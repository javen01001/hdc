/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * DTSolutionAnalysis.hpp
 *
 *  Created on: Aug 15, 2017
 *      Author: depardo
 */

//THIS SHOULD BE LIVING IN legged_robot_task_dt
// But as there is no kynematics base class
#ifndef DT_SOLUTIONANALYSIS_HPP_
#define DT_SOLUTIONANALYSIS_HPP_

#include <Eigen/Dense>
#include <memory>
#include <dynamical_systems/tools/LeggedRobotTrajectory.hpp>
#include <dynamical_systems/tools/HybridLRTrajectory.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLState.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLProjectedDynamics.hpp>

template <class DIMENSIONS>
class DTSolutionAnalysis {

public:

  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;

  DTSolutionAnalysis(std::shared_ptr<LeggedRobotTrajectory<DIMENSIONS>> lrt, const Eigen::VectorXd & F):
    F_(F) {

    hlrt_ = std::shared_ptr<HybridLRTrajectory<DIMENSIONS>>(new HybridLRTrajectory<DIMENSIONS>(lrt->lrd_));
    hlrt_->setHybridTrajectory(*lrt);


  }
  std::shared_ptr<HybridLRTrajectory<DIMENSIONS>> hlrt_;
  Eigen::VectorXd F_;

  PFLState pflk;
  void analyse();
  state_vector_t defect(const state_vector_t & x1, const control_vector_t & u1,
                        const state_vector_t & x2, const control_vector_t & u2,
                        const state_vector_t & x1d , const state_vector_t & x2d,
                        const double & h, const ContactConfiguration_t & fc);

protected:

};

template <class DIMENSIONS>
void DTSolutionAnalysis<DIMENSIONS>::analyse() {

  //check defects:
  // Computing the actual defect from the solution
  // and comparing against F_

  state_vector_t state_d;
  int nphases = hlrt_->number_of_phases_;
  int index = 1;
  int s_size = state_d.size();
  int total_points = 0;
  for(int p = 0; p <  nphases ; p++ ){
      int t_size = hlrt_->lrp_vector_[p]->trajectory_size;
      int c_code = hlrt_->lrp_vector_[p]->phase_c_code;

      for(int n = 0 ; n < t_size - 1 ; n++) {

          state_vector_t x1 = hlrt_->lrp_vector_[p]->x_t[n];
          state_vector_t x2 = hlrt_->lrp_vector_[p]->x_t[n+1];
          control_vector_t u1 = hlrt_->lrp_vector_[p]->u_t[n];
          control_vector_t u2 = hlrt_->lrp_vector_[p]->u_t[n+1];
          double h =  hlrt_->lrp_vector_[p]->t_[n+1] - hlrt_->lrp_vector_[p]->t_[n];

          state_vector_t x1d = hlrt_->lrp_vector_[p]->xd_t[n];
          state_vector_t x2d = hlrt_->lrp_vector_[p]->xd_t[n+1];

          ContactConfiguration_t fc = DIMENSIONS::get_all_contact_configurations()[c_code];
          state_d = defect(x1,u1,x2,u2,x1d,x2d,h,fc);

          std::cout << "d[" << n+1 << "]:" << (state_d-F_.segment(index,s_size)).transpose() << std::endl;
          if(p==2 && n ==0) {
              std::cout << "F:" << F_.segment(index,s_size).transpose() << std::endl;
              std::cout << "D:" << state_d.transpose() << std::endl;
          }

          index += s_size;
          total_points++;
      }

      std::cout << "------------------------------------" << std::endl;
      std::cout << "------------------------------------" << std::endl;


  }
  total_points +=nphases;
  std::cout << "total points : " << total_points << std::endl;


  GeneralizedCoordinates_t q,qd;
  int c_code = 100;
  for(int p = 1 ; p < nphases ; ++p) {

      state_vector_t x0 = hlrt_->lrp_vector_[p-1]->x_t[0];
      state_vector_t x1 = hlrt_->lrp_vector_[p-1]->x_t.back();
      state_vector_t x2 = hlrt_->lrp_vector_[p]->x_t[0];
      state_vector_t x3 = hlrt_->lrp_vector_[p]->x_t.back();
      state_vector_t diff = x2-x1;

      pflk.GeneralizedCoordinatesFromStateVector(diff,q,qd);

      //check continuity in the phases q1-q2
      if((q.array().abs() > 1e-8).any()) {
          std::cout << "There is a difference in q at switching: " << p << std::endl;
          std::cout << q.transpose() << std::endl;
      }
  }


      //check velocity at the end effector at switching
  for(int p = 0 ; p < nphases ; ++p) {
      std::cout << "------------------------------------" << std::endl;
      std::cout << "------------------------------------" << std::endl;
    c_code = hlrt_->lrp_vector_[p]->phase_c_code;
    pflk.ChangeFeetConfiguration(DIMENSIONS::get_all_contact_configurations()[c_code]);
    for(int k=0 ; k < hlrt_->lrp_vector_[p]->trajectory_size ; ++k) {
        state_vector_t x = hlrt_->lrp_vector_[p]->x_t[k];
        pflk.updateState(x);
        std::cout << "point["<<k<<"] = " << pflk.feet_velocity.transpose() << std::endl;
    }
  }


}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t DTSolutionAnalysis<DIMENSIONS>::defect(const state_vector_t & x1, const control_vector_t & u1,
                                                                  const state_vector_t & x2, const control_vector_t & u2,
                                                                  const state_vector_t & x1d , const state_vector_t & x2d,
                                                                  const double & h, const ContactConfiguration_t & fc) {

  state_vector_t def, y_kmid , ymid_dot, F_kmid;
  control_vector_t u_kmid;
  PFLProjectedDynamics pfldyn(fc);



    // Hermitian interpolation of state at intermediate points
    //y_kmid = 0.5*(this->_y_trajectory[node] + this->_y_trajectory[node+1]) + this->_h_trajectory(inc_index)*0.125*(this->_ydot_trajectory[node] - this->_ydot_trajectory[node+1]);
    y_kmid = 0.5*(x1 + x2) + h*0.125*(x1d - x2d);

    // Midpoint rule for control at intermediate points
    //   u_kmid = 0.5*(this->_u_trajectory[node] + this->_u_trajectory[node+1]);
    u_kmid = 0.5*(u1 + u2);

    // approximated dynamics at intermediate point assuming
    // ymid_dot = (1.5/this->_h_trajectory(inc_index))*(this->_y_trajectory[node+1] - this->_y_trajectory[node]) - 0.25*(this->_ydot_trajectory[node] + this->_ydot_trajectory[node+1]);
    ymid_dot = (1.5/h)*(x2 - x1) - 0.25*(x1d + x2d);

    // dynamics at the intermediate points

    F_kmid = pfldyn.systemDynamics(y_kmid, u_kmid);

    //finally the defect constraints
    def = F_kmid - ymid_dot;

//    std::cout << "fc: " << fc << std::endl;
//    std::cout << "Defect : " << std::endl << def.transpose() << std::endl << std::endl;

  return def;

}


#endif /* DT_SOLUTIONANALYSIS_HPP_ */
