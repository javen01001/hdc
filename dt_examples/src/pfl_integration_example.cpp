/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * hyq_integration_example.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: depardo
 */

#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLProjectedDynamics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLDerivatives.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>
#include <dynamical_systems/tools/SystemIntegrationBase.hpp>
#include <ds_visual_base/RobotConfigVis.hpp>

using rD = robotDimensions;
int main(int argc , char** argv) {

	typedef std::vector<double> time_vector_array_t;

	rD::GeneralizedCoordinates_t q_initial;
	rD::GeneralizedCoordinates_t qd_initial = rD::GeneralizedCoordinates_t::Zero();
	rD::state_vector_t x_initial;

  double simulation_frequency = 100;
  double simulation_time_g = 1;

  // Robot Dynamics: All ee in contact
  rD::ContactConfiguration_t fic(true);
  std::shared_ptr<DynamicsBase<rD>> pfl_dyn(new PFLProjectedDynamics(fic));
  PFLProjectedDynamics aux_dyn(fic);
  aux_dyn.pfl_kin.GetCanonicalPoseState(x_initial);

  // visualization variables
  rD::state_vector_array_t x_solution;
  rD::control_vector_array_t u_solution;
  rD::EELambda_array_t f_sol;
  time_vector_array_t t_solution;

  // forward integration 3
  SystemIntegrationBase<rD> syst_int(pfl_dyn);

  // INTEGRATION
  std::cout << "integrating sys_3..." << std::endl;
  syst_int.Integrate(x_initial,simulation_frequency,simulation_time_g);

  // getting solution and playing back in rviz
  syst_int.GetSolution(x_solution,u_solution,t_solution);
  std::cout << "...end of integration" << std::endl;

  ros::init(argc,argv,"playbackIntegration");

  RobotConfigVis<rD> pfl_vis("pointfootleg");

  for(auto x: x_solution) {
        pfl_vis.visualize_pose(x);
        std::getchar();
    }
  std::cout << "end of example" << std::endl;

  return 0;

}
