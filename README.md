# Hyabrid Direct Collocation Library (HDC) #

The HDC library provides a collection of C++ packages for solving Trajectory Optimization problems via direct transcription (including collocation) and multiple shooting methods.

### How to use this library? ###

Please visit the [documentation page](https://dpardo.bitbucket.io/).


### License ###

The [License File](License.txt) is part of the Hybrid Direct Collocation Library (https://dpardo.bitbucket.io), copyright by ETH Zürich. Licensed under Apache2 license.

### Who do I talk to? ###

Diego Pardo (depardo@ethz.ch)
