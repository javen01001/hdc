/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_LiftLegConstraint.hpp
 *
 *  Created on: April 21, 20167
 *      Author: depardo
 */

#ifndef _LRT_LIFTLEGCONSTRAINT_HPP_
#define _LRT_LIFTLEGCONSTRAINT_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <optimization/constraints/InterPhaseBaseConstraint.hpp>

namespace lrt{

template <class DIMENSIONS>
class LRTLiftLegConstraint: public DirectTrajectoryOptimization::BaseClass::InterPhaseBase<DIMENSIONS>  {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
	typedef typename DIMENSIONS::EELambda_single_component_t EELambda_single_component_t;

  static const int s_size = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE);
  static const int nee = static_cast<int>(DIMENSIONS::DimensionsSize::EE_SIZE);

  LRTLiftLegConstraint(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd,
		  const ContactConfiguration_t & fc_before,const std::vector<int> & leg_id, const double & min_force = 1.0):
		    DirectTrajectoryOptimization::BaseClass::InterPhaseBase<DIMENSIONS>(),
			  lr_dyn(lrd), legs_to_lift(leg_id) {

	  int n_legs = static_cast<int>(legs_to_lift.size());

	  int soc = s_size+n_legs;
	  this->setSize(soc);

	  Eigen::VectorXd low(this->complete_vector_size), up(this->complete_vector_size);
	  low.setZero();
	  up.setZero();
	  low.head(n_legs).setConstant(-infy);
	  up.head(n_legs).setConstant(min_force);
	  this->setBounds(low,up);

    f_value.resize(this->complete_vector_size);

	}

  std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lr_dyn;
  Eigen::VectorXd f_value;
  std::vector<int> legs_to_lift;

  virtual ~LRTLiftLegConstraint(){};

  virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
		  const control_vector_t & u1, const control_vector_t & u2) override;
};

template <class DIMENSIONS>
Eigen::VectorXd LRTLiftLegConstraint<DIMENSIONS>::evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
    const control_vector_t & u1, const control_vector_t & u2) {

  lr_dyn->updateState(x1);
  lr_dyn->updateContactForces(u1); //instead of : lr_dyn.getContactForces(u1);

  EELambda_single_component_t Fz;
  lr_dyn->grf_->getZComponent(Fz);

	for(int i = 0 ; i < legs_to_lift.size() ; i++ ) {
	    f_value(i) = Fz(legs_to_lift[i]);
	}

	f_value.tail(s_size) = x1 - x2;

	return f_value;
}

template <class DIMENSIONS>
const int LRTLiftLegConstraint<DIMENSIONS>::s_size;

template <class DIMENSIONS>
const int LRTLiftLegConstraint<DIMENSIONS>::nee;

} // namespace lrt
#endif /* _LRTLIFTLEGCONSTRAINT_HPP_ */
