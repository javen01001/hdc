/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * hyqLastPhaseConstraint.hpp
 *
 *  Created on: September 11, 2016
 *  Upgrade on : April 27, 2017
 *      Author: depardo
 */

#ifndef _LRT_LASTPHASECONSTRAINT_HPP_
#define _LRT_LASTPHASECONSTRAINT_HPP_
 

#include <Eigen/Dense>
#include <legged_robot_task_dt/constraints/lrt_lastPhaseConstraint.hpp>

namespace lrt {


template <class DIMENSIONS>
class LRTLastPhaseConstraint: public LRTSingleConstraintBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	LRTLastPhaseConstraint(const Eigen::VectorXi & state_index, const Eigen::VectorXd & max_values, const Eigen::VectorXd & min_values):
		LRTSingleConstraintBase<DIMENSIONS>(),_state_index(state_index){


		if(state_index.size() != max_values.size() || state_index.size() != min_values.size()) {
			std::cout << "index and values vector should have the same size" << std::endl;
			std::exit(EXIT_FAILURE);
		}

		if((max_values.array()<min_values.array()).any()) {
	      std::cout << "Inconsistent Max/Min values in LRTSingleConstraintBase" << std::endl;
	      std::exit(EXIT_FAILURE);
		}

		this->SetSizeOfConstraint(state_index.size());
		this->SetBounds(min_values,max_values);

		std::cout << "lower_boud : " << this->lower_bound.transpose() << std::endl;
		std::cout << "upper_boud : " << this->upper_bound.transpose() << std::endl;
	}

	/*virtual void UpdateDynamicPointer(std::shared_ptr<HyQProjectedDynamics> hpd) override ;*/


	void evalLRTConstraint(const state_vector_t & x,
				const control_vector_t & u, Eigen::VectorXd & val);

	~LRTLastPhaseConstraint(){}

private:

	Eigen::VectorXi _state_index;
};

template<class DIMENSIONS>
void LRTLastPhaseConstraint<DIMENSIONS>::evalLRTConstraint(
		const state_vector_t& x, const control_vector_t& u, Eigen::VectorXd & g_val) {

	g_val.resize(this->size_of_constraint);

	for(int i = 0 ; i < g_val.size() ; i++) {
		g_val(i) = x(_state_index(i));
	}
}

} // namespace lrt
#endif /* _LRT_LASTPHASECONSTRAINT_HPP_ */


