/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_singleConstraintBase.hpp
 *
 *  Created on: April 21, 2016
 *      Author: depardo
 */

#ifndef LRTSINGLECONSTRAINTBASE_HPP_
#define LRTSINGLECONSTRAINTBASE_HPP_

#include <dynamical_systems/base/LeggedRobotDynamics.hpp>

namespace lrt {

template <class DIMENSIONS>
class LRTSingleConstraintBase {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	LRTSingleConstraintBase() { }

	virtual ~LRTSingleConstraintBase() {};

	std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > _local_Dynamics;

	virtual void evalLRTConstraint(const state_vector_t & x,
			const control_vector_t & u, Eigen::VectorXd & val) = 0;

  virtual void InitializeParametersFromDynamics(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd){};

	virtual void UpdateDynamicPointer(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd) {
	  _local_Dynamics = lrd;
	}
	void GetSizeOfConstraint(int & soc) {
	  soc = size_of_constraint;
	}
	void SetSizeOfConstraint(const int & soc) {
	  if(soc < 1) {
        std::cout << "LRTSingleConstraint: soc should be greater than 0" << std::endl;
        std::exit(EXIT_FAILURE);
	  }
	  size_of_constraint = soc;
	  size_ok_ = true;
	}
	void GetBounds(Eigen::VectorXd & lb, Eigen::VectorXd & ub){
	  if(!bound_ok_ || !size_ok_) {
	      std::cout << "LRTSingleConstraint is not ready" << std::endl;
	      std::exit(EXIT_FAILURE);
	  }
	    lb = lower_bound; ub = upper_bound;}
	void SetBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub){
	  if(!(lb.size() == ub.size())) {
	      std::cout << "LRTSingleConstraint: bounds are not the same size" << std::endl;
	      std::exit(EXIT_FAILURE);
	  }
	  lower_bound = lb;
	  upper_bound = ub;
	  bound_ok_ = true;
	}

	bool bound_ok_ = false;
	bool size_ok_ = false;

	state_vector_t _x_dot;

protected:

	int size_of_constraint = 0;
	int initial_index = 0;
	Eigen::VectorXd lower_bound;
	Eigen::VectorXd upper_bound;
	Eigen::VectorXd constraint_evaluation;
};

} // namespace lrt
#endif /* LRTSINGLECONSTRAINTBASE_HPP */

