/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_impactDynamicsInterPhaseFreeControl.hpp
 *
 *  Created on: April 21, 2017
 *      Author: depardo
 */

#ifndef _LRT_IMPACTDYNAMICSINTERPHASEFREECONTROL_HPP_
#define _LRT_IMPACTDYNAMICSINTERPHASEFREECONTROL_HPP_

#include <memory>
#include <Eigen/Dense>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <optimization/constraints/InterPhaseBaseConstraint.hpp>

namespace lrt{

template <class DIMENSIONS>
class LRTImpactDynamicsInterPhaseFreeControl : public DirectTrajectoryOptimization::BaseClass::InterPhaseBase<DIMENSIONS>  {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef typename DIMENSIONS::InertiaMatrix_t InertiaMatrix_t;
	typedef typename DIMENSIONS::EEJacobianTranspose_t EEJacobianTranspose_t;
	typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
	typedef typename DIMENSIONS::JointCoordinates_t JointCoordinates_t;
	static const int kTotalDof = DIMENSIONS::kTotalDof;

	GeneralizedCoordinates_t q1,q2,q1d,q2d;

	InertiaMatrix_t M2,P;
	EEJacobianTranspose_t dummyJ;
	Eigen::VectorXd f_value;

	std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lr_dyn;

	LRTImpactDynamicsInterPhaseFreeControl(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd,
	    const ContactConfiguration_t & fc, double impact_vel_dif_square = 5.0):
		DirectTrajectoryOptimization::BaseClass::InterPhaseBase<DIMENSIONS>(),
		lr_dyn(lrd) {

    int soc = kTotalDof + kTotalDof;
    Eigen::VectorXd ub(soc),lb(soc);

    lb.segment(0,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);
    ub.segment(0,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);

    lb.segment(kTotalDof,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);
    ub.segment(kTotalDof,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);

    f_value.resize(soc);

    this->setSize(soc);
    this->setBounds(lb,ub);
	}

	virtual ~LRTImpactDynamicsInterPhaseFreeControl(){ };

	virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
			const control_vector_t & u1, const control_vector_t & u2) override;
};

template <class DIMENSIONS>
Eigen::VectorXd LRTImpactDynamicsInterPhaseFreeControl<DIMENSIONS>::evalConstraintsFct(const state_vector_t & x1, const state_vector_t & x2,
				const control_vector_t & u1, const control_vector_t & u2) {


  lr_dyn->getCoordinatesFromState(x1,q1,q1d);
  lr_dyn->getCoordinatesFromState(x2,q2,q2d);

  //using x2 for computing M and Jc
	lr_dyn->updateState(x2);
	lr_dyn->getInertiaMatrix(q2,M2);
	lr_dyn->getOrthogonalProjection(P,dummyJ);

	// positions dont change
	f_value.segment(0,kTotalDof) = q1-q2;

	// impulsive constraint
	InertiaMatrix_t PM = P*M2;
	InertiaMatrix_t PMT = PM.transpose();
	f_value.segment(kTotalDof,kTotalDof) = (PM + M2 - PMT)*q2d - PM*q1d;
	return (f_value);
}

template <class DIMENSIONS>
const int LRTImpactDynamicsInterPhaseFreeControl<DIMENSIONS>::kTotalDof;

} // namespace lrt
#endif /* _LRT_IMPACTDYNAMICSINTERPHASEFREECONTROL_HPP_*/
