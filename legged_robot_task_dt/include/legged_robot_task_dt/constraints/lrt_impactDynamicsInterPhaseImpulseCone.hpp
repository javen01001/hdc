/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_impactDynamicsInterPhaseImpulseCone.hpp
 *
 *  Created on: April 21, 2017
 *      Author: depardo
 *
 *
 *      A quick test for adding the friction cone constraint to the impule as well
 *      this is TEMPORARY! as the final solution should be an
 *      INTERPHASE CONTAINER where user might add different constraints
 *
 */

#ifndef _LRT_IMPACTDYNAMICSINTERPHASEIMPULSECONE_HPP_
#define _LRT_IMPACTDYNAMICSINTERPHASEIMPULSECONE_HPP_

#include <memory>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <optimization/constraints/InterPhaseBaseConstraint.hpp>

namespace lrt{

template <class DIMENSIONS>
class LRTImpactDynamicsInterPhaseImpulseCone : public DirectTrajectoryOptimization::BaseClass::InterPhaseBase<DIMENSIONS>  {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef typename DIMENSIONS::InertiaMatrix_t InertiaMatrix_t;
	typedef typename DIMENSIONS::EEJacobianTranspose_t EEJacobianTranspose_t;
	typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
	typedef typename DIMENSIONS::JointCoordinates_t JointCoordinates_t;
	typedef typename DIMENSIONS::EELambda_t EELambda_t;
	static const int kTotalDof = DIMENSIONS::kTotalDof;

	GeneralizedCoordinates_t q1,q2,q1d,q2d;

	InertiaMatrix_t M2,P1;
	EEJacobianTranspose_t J;
	Eigen::VectorXd f_value;

	std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lr_dyn_1;
	std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lr_dyn_2;
	int nlegs_for_contact = 0;
	//int n_imp_components = 0;
	std::vector<int> leg_impulse_id;

	Eigen::VectorXd impulses_vector;
	Eigen::VectorXd fcI_vector, mag_Iz;
	double mu_ = 0.5;
	double max_z_impulse = 1.0;


	LRTImpactDynamicsInterPhaseImpulseCone(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd_1,
	                                       std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd_2,
	                                       double mu = 1 , double mzi = 0.1):lr_dyn_1(lrd_1),
	                                           lr_dyn_2(lrd_2),mu_(mu),max_z_impulse(mzi) {

	  // How many ee make contact in this transition?
	  ContactConfiguration_t dyn1_fic = lr_dyn_1->getContactConfiguration();
	  ContactConfiguration_t dyn2_fic = lr_dyn_2->getContactConfiguration();
	  ContactConfiguration_t fake_fic(false);

	  for(int ee = 0 ; ee < lr_dyn_1->nee ; ee++) {
	      if(dyn1_fic[ee] == false && dyn2_fic[ee] == true) {
	          nlegs_for_contact++;
	          leg_impulse_id.push_back(ee);
	          fake_fic[ee] = true;
	      }
	  }

	  lr_dyn_1->setContactConfiguration(fake_fic);

	  mag_Iz.resize(nlegs_for_contact);
	  fcI_vector.resize(nlegs_for_contact);

	  int soc = kTotalDof + kTotalDof + nlegs_for_contact + nlegs_for_contact;

	  Eigen::VectorXd lb(soc), ub(soc);

		//set bounds
    lb.segment(0,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);
    ub.segment(0,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);

    lb.segment(kTotalDof,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);
    ub.segment(kTotalDof,kTotalDof) = Eigen::VectorXd::Zero(kTotalDof);

    lb.segment(kTotalDof+kTotalDof,nlegs_for_contact) = Eigen::VectorXd::Zero(nlegs_for_contact);
		ub.segment(kTotalDof+kTotalDof,nlegs_for_contact) = Eigen::VectorXd::Constant(nlegs_for_contact,max_z_impulse);

    lb.segment(kTotalDof+kTotalDof+nlegs_for_contact,nlegs_for_contact) = Eigen::VectorXd::Constant(nlegs_for_contact,-1e20);
    ub.segment(kTotalDof+kTotalDof+nlegs_for_contact,nlegs_for_contact) = Eigen::VectorXd::Zero(nlegs_for_contact);

    f_value.resize(soc);
    this->setSize(soc);
		this->setBounds(lb,ub);
	}

	virtual ~LRTImpactDynamicsInterPhaseImpulseCone(){ };

	virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
			const control_vector_t & u1, const control_vector_t & u2) override;
};

template <class DIMENSIONS>
Eigen::VectorXd LRTImpactDynamicsInterPhaseImpulseCone<DIMENSIONS>::evalConstraintsFct(const state_vector_t & x1, const state_vector_t & x2,
				const control_vector_t & u1, const control_vector_t & u2) {

  lr_dyn_2->getCoordinatesFromState(x1,q1,q1d);
  lr_dyn_2->getCoordinatesFromState(x2,q2,q2d);

  //using x2 for computing M and Jc
  lr_dyn_2->updateState(x2);

  lr_dyn_2->getInertiaMatrix(q2,M2);

  lr_dyn_2->getOrthogonalProjection(P1,J);

	// Building the constraint vector
	// 1. Positions don't change
	f_value.segment(0,kTotalDof) = q1-q2;

	// 2. User should be able to decide whether limit changes on u
	//

	// 3. Projected Impact Dynamics constraint

	InertiaMatrix_t PM = P1*M2;
	InertiaMatrix_t PMT = PM.transpose();
	f_value.segment(kTotalDof,kTotalDof) = (PM + M2 - PMT)*q2d - PM*q1d;


	// 4. Impulse friction cone and Z component (ToDo: Move this to an independent class)
	  GeneralizedCoordinates_t d = q2d - q1d;
    impulses_vector = J.transpose()*M2*d;
//
    Eigen::Affine3d A = lr_dyn_2->getWXBTransform();
    for(int leg = 0 ; leg < nlegs_for_contact ; leg++) {
        Eigen::Vector3d legforce = A.linear()*impulses_vector.segment<3>(3*leg_impulse_id[leg]);
        fcI_vector(leg) = legforce.segment<2>(0).norm() - mu_*legforce(2);
        mag_Iz(leg) = legforce(2);
    }

  f_value.segment(kTotalDof+kTotalDof,nlegs_for_contact) = mag_Iz;
  f_value.segment(kTotalDof+kTotalDof+nlegs_for_contact,nlegs_for_contact) = fcI_vector;
  return (f_value);
}

template <class DIMENSIONS>
const int LRTImpactDynamicsInterPhaseImpulseCone<DIMENSIONS>::kTotalDof;

} // namespace lrt
#endif /* LRT_IMPACTDYNAMICSINTERPHASEIMPULSECONE_HPP_*/
