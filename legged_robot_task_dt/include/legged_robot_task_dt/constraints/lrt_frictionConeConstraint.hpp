/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrtFrictionConeConstraint.hpp
 *
 *  Created on: April 21, 2017
 *      Author: depardo
 */

#ifndef LRTFRICTIONCONECONSTRAINT_HPP_
#define LRTFRICTIONCONECONSTRAINT_HPP_
 

#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <legged_robot_task_dt/constraints/lrt_singleConstraintBase.hpp>

namespace lrt {

template <class DIMENSIONS>
class LRTFrictionConeConstraint: public LRTSingleConstraintBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::EELambda_t EELambda_t;
	typedef typename DIMENSIONS::EEFriction_cone_coefficient_t EEFriction_cone_coefficient_t;

	LRTFrictionConeConstraint(const double & mu = 1):
		LRTSingleConstraintBase<DIMENSIONS>()
		,_friction_coefficient(mu) { }

	virtual void InitializeParametersFromDynamics(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd) override;
	void evalLRTConstraint(const state_vector_t & x,
				const control_vector_t & u, Eigen::VectorXd & val);

	virtual ~LRTFrictionConeConstraint(){}

	double _friction_coefficient = 1.0;
	std::vector<int> leg_number;
	int number_of_feet_in_contact = 0;
	int local_size_of_constraint=0;
};

template <class DIMENSIONS>
void LRTFrictionConeConstraint<DIMENSIONS>::InitializeParametersFromDynamics(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd) {

	leg_number.clear();
	number_of_feet_in_contact = this->_local_Dynamics->getNumberPointsContact();
	leg_number = this->_local_Dynamics->getContactPointIDVector();

	local_size_of_constraint = number_of_feet_in_contact;

	Eigen::VectorXd lb(local_size_of_constraint);
	Eigen::VectorXd ub(local_size_of_constraint);

	lb.setConstant(-1e20);
	ub.setConstant(0.0);

	this->SetSizeOfConstraint(local_size_of_constraint);
	this->SetBounds(lb,ub);
}

template <class DIMENSIONS>
void LRTFrictionConeConstraint<DIMENSIONS>::evalLRTConstraint(
		const state_vector_t& x, const control_vector_t& u, Eigen::VectorXd & g_val) {

	g_val.resize(local_size_of_constraint);

	EELambda_t contact_forces;
	EEFriction_cone_coefficient_t gma;

  this->_local_Dynamics->updateContactForcesQuick(this->_local_Dynamics->getTauMinusHminusMqdd());
  this->_local_Dynamics->grf_->getFrictionConeConstraint(_friction_coefficient,gma);

	for (int i = 0 ; i < number_of_feet_in_contact; i++) {
		g_val[i] = gma(leg_number[i]);
	}

}
} // namespace lrt
#endif /* LRTFRICTIONCONECONSTRAINT_HPP_ */
