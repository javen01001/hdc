/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * DTTrajectorySpliner.hpp
 *
 *  Created on: May 5, 2017
 *      Author: depardo
 */

#ifndef _TRAJECTORYSPLINER_HPP_
#define _TRAJECTORYSPLINER_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/tools/HybridLRTrajectory.hpp>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <spline_module/system_trajectory_spline.hpp>


template <class DIMENSIONS>
class DTTrajectorySpliner : public HybridLRTrajectory<DIMENSIONS> {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
  typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;

  DTTrajectorySpliner(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd):
    HybridLRTrajectory<DIMENSIONS>(lrd),real_robot_trajectory_(new LeggedRobotTrajectory<DIMENSIONS>(lrd)){
  }

  // This functionality
  //DTTrajectoryFile<DIMENSIONS> dt_trajectory_file;
  // bool toFile(const std::string & str);

  double frequency_ = 250;
  double dt_ = 1/250;

  void interpolateFromHybird();
  void setInterpolationFrequency(const double & f) { frequency_=f; dt_ = 1/frequency_;}
  bool getInterpolatedTrajectory(state_vector_array_t & x, control_vector_array_t & u ,
                              Eigen::VectorXd & time, std::vector<int> & P_sequence , std::vector<int> & C_sequence);

private:
  std::shared_ptr<LeggedRobotTrajectory<DIMENSIONS>> real_robot_trajectory_;
  std::vector<int> P_sequence_;
  std::vector<int> C_sequence_;
  bool is_splined_ = false;

};

template <class DIMENSIONS>
void DTTrajectorySpliner<DIMENSIONS>::interpolateFromHybird() {

  // this class interpolates all the phases of the Trajectory
  bool hermite_interpolation = true;
  bool verbose_level = false;

  // clear interpolation
  real_robot_trajectory_->clearTrajectory();
  P_sequence_.clear();
  C_sequence_.clear();

  std::vector<systemTrajectorySpline<DIMENSIONS> > phase_spliner_array;


  for(int p = 0; p < this->number_of_phases_ ; ++p) {

      double base_time = this->lrp_vector_[p]->t_(0);
      double p_duration = this->phase_duration_[p];

//      std::cout << "interpolating phase : " << p << std::endl;
//      std::cout << "base_time : " << base_time << std::endl;
//      std::cout << "phase_duration : " << p_duration << std::endl;


      this->lrd_->setContactConfiguration(this->lrp_vector_[p]->phase_c_code);

      systemTrajectorySpline<DIMENSIONS> phase_spliner(this->lrd_,
                                                       this->lrp_vector_[p]->x_t,
                                                       this->lrp_vector_[p]->u_t,
                                                       this->lrp_vector_[p]->t_,
                                                       hermite_interpolation,
                                                       verbose_level);


      //TODO: Fix for connecting the phases better
      double value = p_duration * frequency_;
      int n_points = std::floor(value);
      if(value - (double)n_points > 0.0) {
          n_points++;
      }


//      std:: cout << "interp_points : " << n_points << std::endl;
//      std::getchar();
      for(int k = 0 ; k < n_points - 1 ; ++k) {
          state_vector_t x,xd;
          control_vector_t u;
          double t_sp = base_time + k/frequency_;
          phase_spliner.getTrajectoryPoint(t_sp,x,u,xd);

//          std::cout << "adding : " << t_sp << std::endl;
          real_robot_trajectory_->add_xut_point(x,u,t_sp);

          real_robot_trajectory_->add_xd_point(xd);
          P_sequence_.push_back(p);
          C_sequence_.push_back(this->lrp_vector_[p]->phase_c_code);
      }
//      std::cout << "phase interpolated!" << std::endl;
//      std::getchar();

  }

  real_robot_trajectory_->setDOFDerivatives();
  is_splined_ = true;
}

//temp, use a nice structure for this
template <class DIMENSIONS>
bool DTTrajectorySpliner<DIMENSIONS>::getInterpolatedTrajectory(state_vector_array_t & x, control_vector_array_t & u,
                                                            Eigen::VectorXd & t_traj, std::vector<int> & P_sequence,
                                                            std::vector<int> & C_sequence) {

  if(!is_splined_) {
      return false;
  }

  x = real_robot_trajectory_->x_t;
  u = real_robot_trajectory_->u_t;
  t_traj = real_robot_trajectory_->t_;
  P_sequence = P_sequence_;
  C_sequence = C_sequence_;

  return true;
}


//template <class DIMENSIONS>
//bool DTTrajectorySpliner<DIMENSIONS>::toFile(const std::string & filepath) {
//
//  if(!this->is_splined_){
//      return false;
//  }
//
//  dt_trajectory_file.setTrajectory(real_robot_trajectory_->x_t,
//                                   real_robot_trajectory_->u_t,
//                                   real_robot_trajectory_->t_,
//                                   this->P_sequence_,
//                                   this->C_sequence_);
//
//  dt_trajectory_file.saveTrajectoryToFile(filepath);
//  return true;
//
//}

#endif /* _TRAJECTORYSPLINER_HPP_ */
