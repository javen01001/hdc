/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * InterPhaseBase.hpp
 *
 *  Created on: Sep 3, 2016
 *      Author: depardo
 */

#ifndef _InterPhaseBase_HPP_
#define _InterPhaseBase_HPP_

#include <memory>
#include <optimization/constraints/GenericConstraintsBase.hpp>

namespace DirectTrajectoryOptimization{
namespace BaseClass{

template <class DIMENSIONS>
class InterPhaseBase : public NumDiffDerivativesBase,
					public std::enable_shared_from_this<InterPhaseBase<DIMENSIONS> > {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	static const int s_size = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE);
	static const int c_size = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE);

	int complete_vector_size = 0;
	int size_of_jacobian = 0;
	int total_inputs_jacobian=0;

	std::shared_ptr<InterPhaseBase<DIMENSIONS> > my_pointer;

	InterPhaseBase() { }
	InterPhaseBase(const bool & no_ipc) {

		//Initialization for the default case
		complete_vector_size = s_size;
		ipc_lb = Eigen::VectorXd::Zero(complete_vector_size);
		ipc_ub = Eigen::VectorXd::Zero(complete_vector_size);

		soc_ready_= true;
		bounds_ready_ = true;
	}
	virtual ~InterPhaseBase(){ };
	int GetSize() { return complete_vector_size;}
	int GetNnzJacobian() { return size_of_jacobian;}
	Eigen::VectorXd getLowerBound(){return ipc_lb;}
	Eigen::VectorXd getUpperBound(){return ipc_ub;}

	void setSize(const int & soc);
	void setBounds(const Eigen::VectorXd & lb , const Eigen::VectorXd & ub);

	void initialize();
	void initialize_num_diff();
	void fx(const Eigen::VectorXd & in, Eigen::VectorXd & out);

	// Implement a default constraint where the positions and controls must be the same
	virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
		const control_vector_t & u1, const control_vector_t & u2);

	Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x1, const state_vector_t & x2,
		const control_vector_t & u1, const control_vector_t & u2);

private:
  Eigen::VectorXd ipc_lb;
  Eigen::VectorXd ipc_ub;

  bool soc_ready_ = false;
  bool bounds_ready_ = false;

};

template <class DIMENSIONS>
void InterPhaseBase<DIMENSIONS>::setSize(const int & soc) {
  if(soc < 1) {
      std::cout << "Interphase size must be greater than 0" << std::endl;
      std::exit(EXIT_FAILURE);
  }
  complete_vector_size = soc;
  soc_ready_ = true;
}

template <class DIMENSIONS>
void InterPhaseBase<DIMENSIONS>::setBounds(const Eigen::VectorXd & lb , const Eigen::VectorXd & ub) {
  if(!(lb.size() == complete_vector_size) || !(ub.size() == complete_vector_size)) {
      std::cout << "Interphase bounds size must be equal to SoG" << std::endl;
      std::exit(EXIT_FAILURE);
  }
  ipc_lb = lb;
  ipc_ub = ub;
  bounds_ready_ = true;
}

template <class DIMENSIONS>
void InterPhaseBase<DIMENSIONS>::initialize() {

  if(!soc_ready_ || !bounds_ready_) {
      std::cout << "Interphase is not ready!" << std::endl;
      std::exit(EXIT_FAILURE);
  }

	// this is always like this!
	total_inputs_jacobian = 2*s_size + 2*c_size;
	size_of_jacobian = complete_vector_size*total_inputs_jacobian;
	initialize_num_diff();
}

template <class DIMENSIONS>
Eigen::VectorXd InterPhaseBase<DIMENSIONS>::evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
				const control_vector_t & u1, const control_vector_t & u2) {

	return (x1-x2);
}

template <class DIMENSIONS>
Eigen::VectorXd InterPhaseBase<DIMENSIONS>::evalConstraintsFctDerivatives(
		const state_vector_t & x1, const state_vector_t & x2 ,
		const control_vector_t &u1,const control_vector_t &u2) {

	Eigen::VectorXd in_vector(total_inputs_jacobian);

	in_vector.segment<s_size>(0) = x1;
	in_vector.segment<c_size>(s_size) = u1;
	in_vector.segment<s_size>(s_size+c_size)= x2;
	in_vector.segment<c_size>(s_size+s_size+c_size) = u2;

	this->numDiff->df(in_vector,this->mJacobian);

	//returning ROW-wise the constraint as a vector
	Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> temp(this->mJacobian);
	return (Eigen::Map<Eigen::VectorXd>(temp.data(),size_of_jacobian));
}

template <class DIMENSIONS>
void InterPhaseBase<DIMENSIONS>::fx(const Eigen::VectorXd & inputs, Eigen::VectorXd & outputs) {

	state_vector_t   x1 = inputs.segment<s_size>(0);
	control_vector_t u1 = inputs.segment<c_size>(s_size);
	state_vector_t   x2 = inputs.segment<s_size>(s_size+c_size);
	control_vector_t u2 = inputs.segment<c_size>(s_size+s_size+c_size);

	outputs = evalConstraintsFct(x1,x2,u1,u2);
}

template <class DIMENSIONS>
void InterPhaseBase<DIMENSIONS>::initialize_num_diff() {

	this->my_pointer = std::enable_shared_from_this<InterPhaseBase<DIMENSIONS>>::shared_from_this();
	this->mJacobian.resize(complete_vector_size,total_inputs_jacobian);
	this->numdifoperator = std::shared_ptr<FunctionOperator>(new FunctionOperator(total_inputs_jacobian, complete_vector_size , this->my_pointer));
	this->numDiff = std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> >( new Eigen::NumericalDiff<FunctionOperator>(*this->numdifoperator,std::numeric_limits<double>::epsilon()));

}

} // BaseClass
} // DirectTrajectoryOptimization
#endif /* _InterPhaseBase_HPP_ */
