/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * InterPhaseBaseConstraintFreecontrol.hpp
 *
 *  Created on: Dec 20, 2016
 *      Author: depardo
 */

#ifndef _INTERPHASEBASE_FREECONTROL_HPP_
#define _INTERPHASEBASE_FREECONTROL_HPP_

#include <memory>
#include <optimization/constraints/GenericConstraintsBase.hpp>

namespace DirectTrajectoryOptimization{
namespace BaseClass{

template <class DIMENSIONS>
class InterPhaseBaseFreeControl : public NumDiffDerivativesBase,
					public std::enable_shared_from_this<InterPhaseBaseFreeControl<DIMENSIONS> > {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	int s_size = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE);
	int c_size = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE);

	Eigen::VectorXd ipc_lb;
	Eigen::VectorXd ipc_ub;
	int complete_vector_size = 0;
	int size_of_jacobian = 0;
	int total_inputs_jacobian=0;

	std::shared_ptr<InterPhaseBaseFreeControl<DIMENSIONS> > my_pointer;


	InterPhaseBaseFreeControl(const int & n_constraints , const Eigen::VectorXd & lb , const Eigen::VectorXd & ub):complete_vector_size(n_constraints),ipc_lb(lb),ipc_ub(ub){

		if(ipc_ub.size()!=ipc_lb.size() || ipc_ub.size() != n_constraints) {
			std::cout << "Wrong size of InterPhase constraints! " << std::endl;
			std::exit(EXIT_FAILURE);
		}
		complete_vector_size = n_constraints;
	}

	InterPhaseBaseFreeControl() {

		//Initialization for the default case
		complete_vector_size = s_size;
		ipc_lb = Eigen::VectorXd::Zero(complete_vector_size);
		ipc_ub = Eigen::VectorXd::Zero(complete_vector_size);
	}

	int GetSize() { return complete_vector_size;}
	int GetNnzJacobian() { return size_of_jacobian;}
	Eigen::VectorXd getLowerBound(){return ipc_lb;}
	Eigen::VectorXd getUpperBound(){return ipc_ub;}

	virtual ~InterPhaseBaseFreeControl(){ };

	void initialize();
	void initialize_num_diff();
	void fx(const Eigen::VectorXd & in, Eigen::VectorXd & out);

	// IMPORTANT: u1 and u2 are NOW used in the structure of the jacobian,

		// Implement a default constraint where the positions and controls must be the same
		virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
				const control_vector_t & u1, const control_vector_t & u2);

		Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x1, const state_vector_t & x2,
			const control_vector_t & u1, const control_vector_t & u2);

	//TEMP??
	Eigen::VectorXd temp_jacobian_vector;
};

template <class DIMENSIONS>
void InterPhaseBaseFreeControl<DIMENSIONS>::initialize() {

	// this is always like this!
	total_inputs_jacobian = 2*s_size + 2*c_size;
	size_of_jacobian = complete_vector_size*total_inputs_jacobian;

	temp_jacobian_vector.resize(size_of_jacobian);

	initialize_num_diff();
}

template <class DIMENSIONS>
Eigen::VectorXd InterPhaseBaseFreeControl<DIMENSIONS>::evalConstraintsFct(const state_vector_t& x1, const state_vector_t & x2,
				const control_vector_t & u1, const control_vector_t & u2) {

	// Poistions should be the same
	return (x1-x2);

}

template <class DIMENSIONS>
Eigen::VectorXd InterPhaseBaseFreeControl<DIMENSIONS>::evalConstraintsFctDerivatives(
		const state_vector_t & x1, const state_vector_t & x2 ,
		const control_vector_t &u1,const control_vector_t &u2) {

	Eigen::VectorXd in_vector(total_inputs_jacobian);

	in_vector.segment(0,s_size) = x1;
	in_vector.segment(s_size,c_size) = u1;
	in_vector.segment(s_size+c_size,s_size) = x2;
	in_vector.segment(2*s_size+c_size,c_size) = u2;

	this->numDiff->df(in_vector,this->mJacobian);

	Eigen::MatrixXd temp = this->mJacobian.transpose();

	//returning ROW-wise the constraint as a vector
	return (Eigen::Map<Eigen::VectorXd>(temp.data(),size_of_jacobian));
}

template <class DIMENSIONS>
void InterPhaseBaseFreeControl<DIMENSIONS>::fx(const Eigen::VectorXd & inputs, Eigen::VectorXd & outputs) {

	state_vector_t   x1 = inputs.segment(0,s_size);
	control_vector_t u1 = inputs.segment(s_size,c_size);
	state_vector_t   x2 = inputs.segment(s_size+c_size,s_size);
	control_vector_t u2 = inputs.segment(2*s_size+c_size,c_size);

	outputs = evalConstraintsFct(x1,x2,u1,u2);
}

template <class DIMENSIONS>
void InterPhaseBaseFreeControl<DIMENSIONS>::initialize_num_diff() {

	this->my_pointer = std::enable_shared_from_this<InterPhaseBaseFreeControl<DIMENSIONS>>::shared_from_this();

	mJacobian.resize(complete_vector_size,total_inputs_jacobian);

	this->numdifoperator = std::shared_ptr<FunctionOperator>(new FunctionOperator(total_inputs_jacobian, complete_vector_size , this->my_pointer));

	this->numDiff = std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> >( new Eigen::NumericalDiff<FunctionOperator>(*this->numdifoperator,std::sqrt(std::numeric_limits<double>::epsilon())));

}

} // BaseClass
} // DirectTrajectoryOptimization
#endif /* _INTERPHASEBASE_FREECONTROL_HPP_ */
