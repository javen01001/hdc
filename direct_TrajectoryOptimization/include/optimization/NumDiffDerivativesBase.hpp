/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	NumDiffDerivativesBase.hpp
 *	\brief 	Base class for numerical differentiation of vector functions
 *  \author depardo
 */

#ifndef NUMDIFFDERIVATIVESBASE_HPP_
#define NUMDIFFDERIVATIVESBASE_HPP_

#include <Eigen/Dense>
#include <unsupported/Eigen/NumericalDiff>

namespace DirectTrajectoryOptimization {
namespace BaseClass {

/**
 * @brief This base class provides functionality for numerical differentiation of vector functions
 * @details derived classes need to overload the function that requires to be differentiated
 */
class NumDiffDerivativesBase {

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:
	NumDiffDerivativesBase(){};

public:

	virtual ~NumDiffDerivativesBase(){};

	template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
	struct DynamicSizedFunctor {

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		typedef _Scalar Scalar;
		enum {
			InputsAtCompileTime = NX, ValuesAtCompileTime = NY
		};
		typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
		typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

		int m_inputs, m_values;

		DynamicSizedFunctor() :
				m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {
		}
		DynamicSizedFunctor(int inputs, int values) :
				m_inputs(inputs), m_values(values) {
		}

		int inputs() const {
			return m_inputs;
		}
		int values() const {
			return m_values;
		}

	};

	struct FunctionOperator:DynamicSizedFunctor<double> {

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		//y = f(x);
		FunctionOperator(int in_size, int out_size, std::shared_ptr<NumDiffDerivativesBase> my_owner_pointer) :
				DynamicSizedFunctor<double>(in_size, out_size),owner_pointer(my_owner_pointer)
		{ }

		int operator()(const Eigen::VectorXd &in_vect,
				Eigen::VectorXd &fvec) const {

			fvec.setZero();
			owner_pointer->fx(in_vect, fvec);
			return 0;
		}
	private :
		std::shared_ptr<NumDiffDerivativesBase> owner_pointer;

	};

	std::shared_ptr<FunctionOperator> numdifoperator; /*!< pointer to the derived class functor */
	std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> > numDiff; /*!< pointer to Eigen Based NumDiff constructor */
	Eigen::MatrixXd mJacobian;

	/**
	 * @brief Overload this method in the derived class to obtain numerical approximation of the gradient
	 */
	virtual void fx(const Eigen::VectorXd &, Eigen::VectorXd &) = 0;

	/**
	 * @brief Overload this method to define the local pointers
	 * @details numdifoperator and numDiff need to be defined in this method
	 */
	virtual void initialize_num_diff() = 0;
};
}
}
#endif /* NUMDIFFDERIVATIVESBASE_HPP_ */
