/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 		multiple_shooting.hpp
 *	\brief		Class implementing multiple shooting for direct trajectory optimizaion
 *	\authors  	singh23, depardo
 */

#ifndef INCLUDE_METHOD_INTERFACES_MULTIPLE_SHOOTING_HPP_
#define INCLUDE_METHOD_INTERFACES_MULTIPLE_SHOOTING_HPP_

#define infy 1e20

#include <Eigen/Dense>
#include <boost/numeric/odeint.hpp>
#include <method_interfaces/multiple_shooting/shooting_constraint.hpp>
#include <method_interfaces/base_method_interface.hpp>

namespace DirectTrajectoryOptimization {

template <typename DIMENSIONS>
class DTOMultipleShooting;

/**
 * @brief This class implements Direct Multiple Shooting.
 * @details Implementing interfaces for DTO methods
 */
template<typename DIMENSIONS>
class DTOMultipleShooting: public DTOMethodInterface<DIMENSIONS>, public std::enable_shared_from_this<DTOMultipleShooting<DIMENSIONS>>{

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	DTOMultipleShooting(
			std::vector<std::shared_ptr<DynamicsBase<DIMENSIONS> > > dynamics,
			std::vector<std::shared_ptr<DerivativesBaseDS<DIMENSIONS> > > derivatives,
			std::shared_ptr<BaseClass::CostFunctionBase<DIMENSIONS> > costFunction,
			std::vector<std::shared_ptr<BaseClass::ConstraintsBase<DIMENSIONS> > > constraints,
			Eigen::Vector2d duration, int number_of_nodes, bool methodVerbose) :
				DTOMethodInterface<DIMENSIONS>(dynamics, derivatives, costFunction,
						constraints, duration, number_of_nodes, methodVerbose) { }

	virtual ~DTOMultipleShooting() {}

	virtual void Method_specific_initialization() override {
		_shootingconstraint_ = std::shared_ptr<ShootingConstraint<DIMENSIONS> >(new ShootingConstraint<DIMENSIONS>(this->num_defects,this->myNLP.num_vars,
				std::enable_shared_from_this<DTOMultipleShooting<DIMENSIONS>>::shared_from_this()));
		_shootingconstraint_->initialize();

	}

	// Sparsity structure is the same as the one in direct transcription
	virtual void evalSparseJacobianConstraint(double* val) override;
	virtual void evaluateDefects(Eigen::VectorXd & defect_vector) override;

	std::shared_ptr<ShootingConstraint<DIMENSIONS> > _shootingconstraint_; /*!< Shooting constraint handling the defects */
};

template<class DIMENSIONS>
void DTOMultipleShooting<DIMENSIONS>::evalSparseJacobianConstraint(double* val) {

  Eigen::Map<Eigen::VectorXd> val_vec(val,this->myNLP.lenG);
  val_vec.setZero();
  Eigen::MatrixXd Jacobian;

  Eigen::VectorXd x_local;
  this->SetDecisionVariablesFromStateControlIncrementsTrajectories(x_local,this->_y_trajectory,
                                                                   this->_u_trajectory,
                                                                   this->_h_trajectory);

  _shootingconstraint_->evalDefectGradient(x_local,Jacobian);


	int constraint_row = 0; // row of the constraint
	int G_index = 0;      	// current index of the sparsity structure
	int nnz_per_row;

	/* defect constraints */

	for (int k = 0 ; k < this->n_inc ; k++) {

		int set_of_defects = k * this->_numStates;
		// for each defect
		for (int jj = 0; jj < this->_numStates; jj++) {
			int current_state = set_of_defects + jj;
			// w.r.t. h_k
			val_vec(G_index) = Jacobian(set_of_defects + jj , this->h_inds(k));//Jac_h(jj);
			G_index += 1;

			// w.r.t. y_k
			val_vec.segment(G_index, this->_numStates) = Jacobian.block(current_state , this->y_inds(0,k) , 1 , this->_numStates).transpose();//Jac_yk.row(jj);
			G_index += this->_numStates;

			// w.r.t. y_k+1
			val_vec.segment(G_index, this->_numStates) = Jacobian.block(current_state , this->y_inds(0,k+1) , 1 , this->_numStates).transpose();//Jac_ykp1.row(jj);
			G_index += this->_numStates;

			// w.r.t. u_k
			val_vec.segment(G_index, this->_numControls) = Jacobian.block(current_state , this->u_inds(0,k) , 1 , this->_numControls).transpose();//Jac_uk.row(jj);
			G_index += this->_numControls;

			// w.r.t. u_k+1
			val_vec.segment(G_index, this->_numControls) = Jacobian.block(current_state , this->u_inds(0,k+1) , 1 , this->_numControls).transpose();//row(jj);
			G_index += this->_numControls;

			constraint_row++;
			}
	}


	if (constraint_row != this->num_defects) {
		std::cout << "Error evaluating the Jacobian of constraints (DEFECTS)"
				<< std::endl;
		exit(EXIT_FAILURE);
	}

	// ToDo: The following constraint jacobian is common to direct_transcription, but here is replicated.
	//       This should be centralized in base_method_interface

	/* parametric constraints */
		// Time constraint
		val_vec.segment(G_index, this->n_inc) = Eigen::VectorXd::Ones(this->n_inc);
		G_index += this->n_inc;
		constraint_row++;

		// increments constraint
		nnz_per_row = 2;
		if (this->_evenTimeIncr) {

			for (int k = 0 ; k < this->n_inc -1 ; k++) {
				//iGfun.segment(G_index,nnz_per_row) = Eigen::VectorXi::Constant(nnz_per_row,constraint_row);

				Eigen::VectorXd aux_value(nnz_per_row);
				aux_value << 1 , -1;
				val_vec.segment(G_index,nnz_per_row) = aux_value;

				G_index+=nnz_per_row;
				constraint_row++;
			}
		}

		if (constraint_row != this->num_parametric_constraints + this->num_defects)	{
			std::cout << "Error evaluating the Jacobian of constraints (PARAMETRIC)" << std::endl;
			std::cout << "contratint_row : " << constraint_row << std::endl;
			std::cout << "num_parametric_constraints : " << this->num_parametric_constraints << std::endl;
			exit(EXIT_FAILURE);
		}

    /* User Constraints */

		for (int k=0; k< this->_numberOfNodes; k++) {

			//val_vec.segment(G_index,this->num_nonzero_jacobian_user_per_node)  = this->_constraints[this->node_to_phase_map[k]]->evalConstraintsFctDerivatives(y_trajectory[k], u_trajectory[k]);
			val_vec.segment(G_index,this->num_nonzero_jacobian_user_per_node_in_phase[this->node_to_phase_map[k]])  = this->_constraints[this->node_to_phase_map[k]]->evalConstraintsFctDerivatives(this->_y_trajectory[k],this->_u_trajectory[k]);
			G_index += this->num_nonzero_jacobian_user_per_node_in_phase[this->node_to_phase_map[k]];

			constraint_row += this->num_user_constraints_per_node_in_phase[this->node_to_phase_map[k]];
		}

	// ToDo Guard Constraints
		if (this->_usingGuardFunction) {
			//we have this number already!
			//ToDo: Move this!
			int num_nnz_jac_guards = this->num_guards * this->_numStates;
			Eigen::VectorXi guard_derivatives;
			guard_derivatives.resize(this->_numStates);
			val_vec.segment(G_index,num_nnz_jac_guards).setZero();
			G_index +=num_nnz_jac_guards;
			constraint_row += this->num_guards;
		}

    if (constraint_row!=this->myNLP.num_F) {
    	std::cout << "Error evaluating the Jacobian of constraints (USER)" << std::endl;
    	std::cout << "constraint_row : " << constraint_row << std::endl;
    	std::cout << "num_F : " << this->myNLP.num_F << std::endl;
    	std::cout << "lenG : " << this->myNLP.lenG << std::endl;
		exit(EXIT_FAILURE);
    }
}

template<class DIMENSIONS>
void DTOMultipleShooting<DIMENSIONS>::evaluateDefects(
		Eigen::VectorXd & defect_vector) {

	Eigen::VectorXd x_local;
	this->SetDecisionVariablesFromStateControlIncrementsTrajectories(x_local,this->_y_trajectory,this->_u_trajectory,this->_h_trajectory);

	defect_vector = _shootingconstraint_->Evalfx(x_local);
}

} // namespace DirectTrajectoryOptimization

#endif /* INCLUDE_METHOD_INTERFACES_MULTIPLE_SHOOTING_HPP_ */
