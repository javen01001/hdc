/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file		shooting_constraint.hpp
 *	\brief		Multiple shooting defect function including numerical differentiation
 *	\author		depardo
 */

#ifndef SHOOTINGCONSTRAINT_HPP_
#define SHOOTINGCONSTRAINT_HPP_

#include <Eigen/Dense>
#include <method_interfaces/multiple_shooting/multiple_shooting.hpp>
#include <optimization/constraints/GenericConstraintsBase.hpp>
#include <boost/numeric/odeint.hpp>
#include <dynamical_systems/tools/eigenIntegration.h>

namespace DirectTrajectoryOptimization {
template <typename DIMENSIONS>
class DTOMultipleShooting;

/**
 * \brief	Class handling the defects of a multiple shooting method
 * \details	uses the mechanisms for numdiff provided by generic constraints base class
 */
template<class DIMENSIONS>
class ShootingConstraint : public BaseClass::GenericConstraintsBase {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	/**
	 * \brief	Constructor of ShootingConstraint
	 * \param[in]	n_func	The size of the vector of defects
	 * \param[in]	n_vars	The number of variables
	 * \param[in]	my_owner	Pointer to the DTOMultipleShooting using this object
	 */
	ShootingConstraint(const int & n_func, const int & n_vars, std::shared_ptr<DTOMultipleShooting<DIMENSIONS>>  my_owner):
		GenericConstraintsBase(n_func, n_vars),_dms_(my_owner)	{ }

	virtual Eigen::VectorXd Evalfx(const Eigen::VectorXd & in_vect) override;

	void forwardIntegrateForTimeRange(const state_vector_t & curr_state, const control_vector_t & curr_control,
			const control_vector_t & next_control, const double & total_t, state_vector_t & integrated_state);

	void evalDefectGradient(const Eigen::VectorXd & inputspace, Eigen::MatrixXd &jacobianMatrix);

	Eigen::VectorXd evalDefectVector(const state_vector_array_t & y_trajectory,
			const control_vector_array_t & u_trajectory, const Eigen::VectorXd & h_trajectory);

	~ShootingConstraint() {}

	void initialize() {
		this->initialize_num_diff();
		_ms_defect_vector.resize(_dms_->num_defects);
	}

	double ms_dt_ = 0.001;	/*!< The integration step */

	Eigen::VectorXd _ms_defect_vector;

	std::shared_ptr<DTOMultipleShooting<DIMENSIONS> > _dms_;
	std::shared_ptr<DynamicsBase<DIMENSIONS> > _local_dynamics_;

	//ToDo : Try with different steppers!
	boost::numeric::odeint::runge_kutta4<state_vector_t, double,
				state_vector_t, double,
				boost::numeric::odeint::vector_space_algebra> ms_stepper_;
};

template<class DIMENSIONS>
typename Eigen::VectorXd ShootingConstraint<DIMENSIONS>::Evalfx(const Eigen::VectorXd & in_vector) {

	state_vector_array_t y_trajectory;
	control_vector_array_t u_trajectory;
	Eigen::VectorXd  h_trajectory;

	_dms_->getStateControlIncrementsTrajectoriesFromDecisionVariables(in_vector,y_trajectory,u_trajectory,h_trajectory);

	Eigen::VectorXd defect = evalDefectVector(y_trajectory,u_trajectory,h_trajectory);

	return(defect);
}

template<class DIMENSIONS>
Eigen::VectorXd ShootingConstraint<DIMENSIONS>::evalDefectVector(const state_vector_array_t & y_trajectory,
		const control_vector_array_t & u_trajectory , const Eigen::VectorXd & h_trajectory) {

	state_vector_t integrated_state;
	_ms_defect_vector.setZero();

	int current_defect_index = 0;

	for (int k = 0; k < _dms_->n_inc ;  k++) {

		_local_dynamics_ = _dms_->_dynamics[_dms_->node_to_phase_map[k]];

		forwardIntegrateForTimeRange(y_trajectory[k], u_trajectory[k],
				u_trajectory[k+1], h_trajectory[k],integrated_state);

		state_vector_t defect = (y_trajectory[k+1] - integrated_state);

		_ms_defect_vector.segment<DIMENSIONS::kTotalStates>(current_defect_index) = defect;

		current_defect_index +=  DIMENSIONS::kTotalStates;
	}

	return(_ms_defect_vector);
}

template<class DIMENSIONS>
void ShootingConstraint<DIMENSIONS>::evalDefectGradient(const Eigen::VectorXd& input, Eigen::MatrixXd &defect_jacobian) {
	// User can still override the function EVALFXJACOBIAN in this class and write an analytical version

	defect_jacobian = this->EvalFxJacobian(input);
}

template<class DIMENSIONS>
void ShootingConstraint<DIMENSIONS>::forwardIntegrateForTimeRange(
		const state_vector_t & curr_state, const control_vector_t & curr_control,
		const control_vector_t & next_control, const double & total_t, state_vector_t & integrated_state) {

	// generate stepper and lambda for forward integration
	integrated_state = curr_state;

	auto f = [=](const state_vector_t &x, state_vector_t &dxdt, const double t) {

		double t_percentage = t/total_t;
		control_vector_t control =
				next_control * t_percentage +
				curr_control * (1-t_percentage);

		dxdt = _local_dynamics_->systemDynamics(x, control);
	};

	// run the integration
	boost::numeric::odeint::integrate_const(ms_stepper_, f, integrated_state,
			0.0,total_t,ms_dt_);
}

} // namespace DirectTrajectoryOptimization
#endif /* SHOOTINGCONSTRAINT_HPP_ */
