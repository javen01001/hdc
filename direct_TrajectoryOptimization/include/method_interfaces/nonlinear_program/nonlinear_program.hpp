/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*!	\file  nonlinearprogram.hpp
 *	\brief NLP constructed out of the DTO problem
 *	\authors depardo
 */

#ifndef NONLINEARPROGRAM_HPP_
#define NONLINEARPROGRAM_HPP_

#include <Eigen/Dense>

namespace DirectTrajectoryOptimization {

/**
 * @brief   This class contains all the information related to the NLP
 * @details this is independent of the TO problem
 * @note    no notes so far
 */

class nonlinearprogram {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	nonlinearprogram(bool v=false);
	~nonlinearprogram(){ };

	void SetVariablesSize(const int & n_vars);
	void SetConstraintsSize(const int & n_const);
	void SetVariablesBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub);
	void SetConstraintsBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub);

	void SetGSize(const int & g_size);
	void setGcostSize(const int & gcost_size);

	inline int GetNumVars() {
		return num_vars;
	}
	inline int GetNumF() {
		return num_F;
	}
	inline int GetDimG() {
		return lenG;
	}
	inline int GetDimGCost() {
		return lenG_cost;
	}
	double* GetXlb_p() {
		return x_lb.data();
	}
	double* GetXub_p() {
		return x_ub.data();
	}
	double* GetFlb_p() {
		return Flb.data();
	}
	double* GetFub_p() {
		return Fub.data();
	}
	int* GetiGfun_p() {
		return iGfun.data();
	}
	int* GetjGvar_p() {
		return jGvar.data();
	}

	// I think I can delete these
	Eigen::VectorXd GetFlb() {
		return Flb;
	}
	Eigen::VectorXd GetFub() {
		return Fub;
	}
	Eigen::VectorXi GetiGfun() {
		return iGfun;
	}
	Eigen::VectorXi GetjGvar() {
		return jGvar;
	}
	Eigen::VectorXi GetjGvarCost() {
		return jG_cost;
	}
	Eigen::VectorXd GetDecisionVariables() {
		return x_decision_variables;
	}

	/* size of problem */
	int num_vars = 0;
	int num_F = 0;
	int lenG = 0;
	int lenG_cost = 0;

	// constraints vector value/bounds
	Eigen::VectorXd F;
	Eigen::VectorXd Flb;
	Eigen::VectorXd Fub;

	// constraint Jacobian matrix and nnz element indices
	Eigen::VectorXd G;
	Eigen::VectorXi iGfun;
	Eigen::VectorXi jGvar;

	Eigen::VectorXd G_cost;
	Eigen::VectorXi jG_cost;

	// Optimization vector bounds
	Eigen::VectorXd x_lb;
	Eigen::VectorXd x_ub;

	// Decision variables
	Eigen::VectorXd x_decision_variables;

	bool nlp_verbosity;
};

nonlinearprogram::nonlinearprogram(bool verb /*= false */):nlp_verbosity(verb) {

	//during construction
	std::cout << "dummy" << std::endl;

}

void nonlinearprogram::SetVariablesSize(const int & n_vars) {

	num_vars = n_vars;
	x_lb.resize(n_vars);
	x_ub.resize(n_vars);

	if(nlp_verbosity) {
		std::cout << "[VERBOSE] num_vars : " << num_vars << std::endl;
	}

}

void nonlinearprogram::SetConstraintsSize(const int & n_const) {

	num_F = n_const;
	F.resize(n_const);
	Flb.resize(n_const);
	Fub.resize(n_const);

	if(nlp_verbosity) {
	std::cout << " [VERBOSE] num_F : " << num_F << std::endl;
	}
}

void nonlinearprogram::SetVariablesBounds(const Eigen::VectorXd & lb , const Eigen::VectorXd &ub) {

	x_lb = lb;
	x_ub = ub;

	if(nlp_verbosity) {
		std::cout << " [VERBOSE] xlb : " << x_lb.transpose() << std::endl;
		std::cout << " [VERBOSE] xup : " << x_ub.transpose() << std::endl;
	}
}

void nonlinearprogram::SetConstraintsBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub) {

	Flb = lb;
	Fub = ub;

	if(nlp_verbosity) {
		std::cout << " [VERBOSE] Fub : " << Fub.transpose() << std::endl;
		std::cout << " [VERBOSE] Flb : " << Flb.transpose() << std::endl;
	}
}

void nonlinearprogram::SetGSize(const int & g_size) {

	lenG = g_size;

	G.resize(g_size);
	iGfun.resize(g_size);
	jGvar.resize(lenG);

}

void nonlinearprogram::setGcostSize(const int & gcost_size) {

	lenG_cost = gcost_size;

	G_cost.resize(gcost_size);
	jG_cost.resize(gcost_size);
}


}
#endif /* NONLINEARPROGRAM_HPP_ */
