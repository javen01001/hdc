/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	base_solver_interface.hpp
 *	\brief 	Base class for NLP Solvers interface
 *	\authors singhh23, depardo
 */

#include <Eigen/Dense>
#include <unsupported/Eigen/NumericalDiff>
#include "method_interfaces/base_method_interface.hpp"

#ifndef INCLUDE_SOLVER_INTERFACES_BASE_SOLVER_INTERFACE_HPP_
#define INCLUDE_SOLVER_INTERFACES_BASE_SOLVER_INTERFACE_HPP_

namespace DirectTrajectoryOptimization {

/**
 * @brief This class serves as a base interface for inheriting classes.
 * @details Inheriting classes are responsible for implementing interfaces for NLP solvers
 * (eg. IPOPT/SNOPT).
 * @note An instance of this class should never be created.
 */
template <typename DIMENSIONS>
class DTOSolverInterface {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	virtual ~DTOSolverInterface() {};

	/**
	 * \brief DTO initializes the solver using this method
	 * \param[in] problemName The name of the problem provided by user
	 * \param[in] outputFile  The name of the outputfile provided by user
	 * \param[in] paramFile   The name of the parameters file (solver dependent)
	 * \param[in] computeJacobian The flag to use solver numerical differentiation
	 */
	virtual void InitializeSolver(
			const std::string & problemName,
			const std::string & outputFile,
			const std::string & paramFile /*= "" */,
			const bool &computeJacobian) = 0;

	/**
	 * \brief DTO request the solver to proceed with the NLP
	 * \param[in] warminit The flag indicating the type of optimization
	 */
	virtual void Solve(bool warminit = false) = 0;

	/**
	 * \brief Method to get the current values of the solution
	 * \details this point is not necessarily feasible or optimal
	 * \param[out] yTraj The state space trajectory
	 * \param[out] uTraj The control trajectory
	 * \param[out] hTraj The time increments between the discretization points
	 */
	virtual void GetDTOSolution(
			state_vector_array_t &yTraj,
			control_vector_array_t &uTraj,
			Eigen::VectorXd &hTraj) = 0;

	/**
	 * \brief Method to get the current values of the solution
	 * \details overloaded to also return the phase ID for each node
	 * \param[out] yTraj The state space trajectory
	 * \param[out] uTraj The control trajectory
	 * \param[out] hTraj The time increments between the discretization points
	 * \param[out] phaseId The vector of phases ID's of the corresponding nodes
	 */
	virtual void GetDTOSolution(
			state_vector_array_t & yTraj,
			control_vector_array_t & uTraj,
			Eigen::VectorXd & hTraj,
			std::vector<int> & phaseId) = 0;

  /**
   * \brief Method to get the current values of the solution
   * \details overloaded to also return the state derivatives
   * \param[out] yTraj The state space trajectory
   * \param[out] ydotTraj The state derivative trajectory
   * \param[out] uTraj The control trajectory
   * \param[out] hTraj The time increments between the discretization points
   * \param[out] phaseId The vector of phases ID's of the corresponding nodes
   */
	virtual void GetDTOSolution(
	    state_vector_array_t &yTraj,
	    state_vector_array_t &ydotTraj,
	    control_vector_array_t &uTraj,
	    Eigen::VectorXd &hTraj,
	    std::vector<int> &phaseId) { std::cout << "not here" << std::endl; getchar();};


	/**
	 * \brief Method to get final values of internal solver variables
	 * \details the meaning of the state variables is solver dependent
	 * \param[out] _x_state The vector of integers indicating the final state of all decision variables
	 * \param[out] _x_mul The vector of Lagrange multipliers corresponding to all decision variables
	 * \param[out] _f_state The vector of integers indicating the final state of the vector of NLP-constraints
	 * \param[out] _f_mul The vector of Lagrange multipliers corresponding to the vector of NLP-constraints
	 */
	virtual void GetSolverSolutionVariables(
			Eigen::VectorXi & _x_state,
			Eigen::VectorXd & _x_mul,
			Eigen::VectorXi & _f_state,
			Eigen::VectorXd & _f_mul) {}


	/*! \brief Initial solution point for the solver
	 *  \details Set the decision variables to the values given by a trajectory
	 *  \param[in] y_sol vector of states describing the trajectory
	 *  \param[in] u_sol vector of controls describing the trajectory
	 *  \param[in] h_sol vector of time increments between discretization points
	 */
	virtual void InitializeDecisionVariablesFromTrajectory(
			const state_vector_array_t & y_sol,
			const control_vector_array_t & u_sol,
			const Eigen::VectorXd & h_sol) = 0;


	/*! \brief Warm Initialization of NLP Solver
	 *  \details set solver variables given by previous solution
	 *  \param[in] x_state Index for SNOPT decision variables
	 *  \param[in] x_mul Lagrange multipliers of decision variables
	 *  \param[in] f_state Index for SNOPT vector of constraints
	 *  \param[in] f_mul Lagrange multipliers of the constraints
	 */
	virtual void WarmSolverInitialization(
			const Eigen::VectorXi & x_state,
			const Eigen::VectorXd & x_mul,
			const Eigen::VectorXi & f_state,
			const Eigen::VectorXd & f_mul) {};

	virtual void SetVerifyLevel(const int & level) { }
	virtual void UsePrintFile(const bool & flag) { }


	/*! \brief Setting up solver parameters
	 *  \details set solver specific parameters
	 */
	virtual void SetupSolverParameters() = 0 ;

	/**
	 * \brief Returns the vector of NLP-constraints
	 * \param[out] fVec The vector of NLP-constraints
	 */
	virtual void GetFVector(Eigen::VectorXd &fVec) = 0;

	/**
	 * \brief Set verbosity of the solver
	 * \param[in] verbose The flag determining the verbosity of the solver
	 */
	void SetVerbosity(bool verb) {  _verbose = verb; }

	void setPrintIterations(bool piter) { _print_iterations = piter;}

	/**
	 * \brief Set all the possible screen outputs off, the solver will not produce
	 * any message
	 * @param quiet
	 */
	void TotallyQuiet(bool quiet) {
	  _verbose = quiet;
	  _totally_quiet_ = quiet;
	  _print_iterations = quiet;
	}

  void SaveBasisFile(const bool & flag) {
    _save_this_solution_ = true;
    new_basis_file_param = 30;
  }

  void LoadBasisFile(const bool & flag , int name_number ) {
    _load_from_solution_ = true;
    old_basis_file_param = name_number;
  }


	//member variables
	bool _totally_quiet_ = false;
	bool _print_iterations = true;
	bool _verbose = true;
  bool _save_this_solution_ = false;
  bool _load_from_solution_ = false;
  int new_basis_file_param = 0;
  int old_basis_file_param = 0;

protected:

	/**
	 * @brief Construct an instance of the DTOSolverInterface class.
	 * @details This constructor is not public in order to prevent users from creating this class.
	 * This constructor is not private in order to allow derived classes to use the constructor.
	 */
	DTOSolverInterface() {};

};
} // namespace DirectTrajectoryOptimization
#endif /* INCLUDE_SOLVER_INTERFACES_BASE_SOLVER_INTERFACE_HPP_ */
