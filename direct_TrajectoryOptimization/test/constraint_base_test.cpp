/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 *  path_constraint_test.cpp
 *
 *  Created on: Nov 17, 2015
 *      Author: depardo
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <iostream>
#include <memory>
#include <Eigen/Dense>
#include <optimization/constraints/ConstraintsBase.hpp>
#include <optimization/constraints/CylindricalObstaclePathConstraint.hpp>
#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>



typedef acrobot::acrobotDimensions::state_vector_t state_vector_t;
typedef acrobot::acrobotDimensions::state_vector_array_t state_vector_array_t;
typedef acrobot::acrobotDimensions::control_vector_t control_vector_t;
typedef acrobot::acrobotDimensions::control_vector_array_t control_vector_array_t;

using namespace DirectTrajectoryOptimization;

int main(int argc, const char* argv[])
{

	CylindricalObstaclePathConstraint<acrobot::acrobotDimensions> cylindricalPathConstraint;

	int num_constraint = cylindricalPathConstraint.getNumConstraints();

	Eigen::VectorXd g_u_candidate(num_constraint);
	Eigen::VectorXd g_l_candidate(num_constraint);

	g_u_candidate[0] = 100;
	g_l_candidate[0] = -99;

	cylindricalPathConstraint.setConstraintsLowerBound(g_l_candidate);
	cylindricalPathConstraint.setConstraintsUpperBound(g_u_candidate);

	int trajectory_size = 100;

	state_vector_array_t x_trajectory;
	control_vector_array_t u_trajectory;
	Eigen::VectorXd h_trajectory;

	h_trajectory.resize(trajectory_size-1);
	x_trajectory.resize(trajectory_size);
	u_trajectory.resize(trajectory_size);

	for(int k = 0 ; k < trajectory_size ; k++)
	{

		control_vector_t u;

		u.setConstant(2);
		u_trajectory[k] = u;

		state_vector_t x = state_vector_t::LinSpaced(10,100);
		x_trajectory[k] = x;

		Eigen::VectorXd g_out = cylindricalPathConstraint.evalConstraintsFct(x,u);

		std::cout << "g_out:" << g_out << std::endl;

		Eigen:: VectorXd Jacobian  = cylindricalPathConstraint.evalConstraintsFctDerivatives(x,u);

		std::cout << "Jacbobian(k) : " << Jacobian.transpose() << std::endl;

	}

	std::cout << std::endl;

	h_trajectory.setConstant(1.0);

	Eigen::VectorXd g_lb = cylindricalPathConstraint.getConstraintsLowerBound();
	Eigen::VectorXd g_ub = cylindricalPathConstraint.getConstraintsUpperBound();

	Eigen::VectorXi index_i= cylindricalPathConstraint.getSparseRow();
	Eigen::VectorXi index_j= cylindricalPathConstraint.getSparseCol();

	int nn_size = cylindricalPathConstraint.getNnzJac();

		std::cout << "num_constraint: " << num_constraint << std::endl;
	std::cout << "g_lb: " << g_lb << std::endl;
	std::cout << "g_ub: " << g_ub << std::endl;

	std::cout << "index_i: " << index_i << std::endl;
	std::cout << "index_j: " << index_j << std::endl;



	std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > my_constraint_base(new CylindricalObstaclePathConstraint<acrobot::acrobotDimensions>);



	return 0;




}
#endif
