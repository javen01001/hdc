/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/** \example acrobot_snopt_example.cpp
 *  \brief example : acrobot trajectory optimization problem solved using dt/ms and SNOPT
 *  \author depardo,hsingh
 */


#include <memory>
#include <iostream>

// DS Package
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/DerivativesNumDiffBase.hpp>

// DTO Package
#include <optimization/constraints/ConstraintsBase.hpp>
#include <optimization/costs/CostFunctionBase.hpp>
#include <optimization/costs/ZeroCostBolza.hpp>
#include <direct_trajectory_optimization_problem.hpp>

// Acrobot headers
#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_dynamics.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_derivatives.hpp>

typedef acrobot::acrobotDimensions::state_vector_t state_vector_t;
typedef acrobot::acrobotDimensions::state_vector_array_t state_vector_array_t;
typedef acrobot::acrobotDimensions::control_vector_t control_vector_t;
typedef acrobot::acrobotDimensions::control_vector_array_t control_vector_array_t;

using namespace DirectTrajectoryOptimization;

int main(int argc, const char* argv[]) {

	std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > dynamics(new acrobotDynamics);
	std::shared_ptr<DerivativesNumDiffBase<acrobot::acrobotDimensions> > derivatives(new acrobotNumDiffDerivatives(dynamics));
	std::shared_ptr<BaseClass::CostFunctionBase<acrobot::acrobotDimensions> > costFunction(new Costs::ZeroCostBolza<acrobot::acrobotDimensions>);
	std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > constraints(new BaseClass::ConstraintsBase<acrobot::acrobotDimensions>);

	// reading options from command line  (method and number of nodes)
	if(argc < 2) {
		std::cout << "Add parameters to command line: Method(0/1)" << std::endl;
		std::cout << "Method: 0-Direct Transcription / 1-Multiple Shooting" << std::endl;
		exit(0);
	}

	Eigen::Vector2d duration;
	duration << 2,10;
	int number_of_nodes = 40;

	Methods::method_t chosen;
	std::string argv1 = argv[1];
	if (argv1 == "0") {
		chosen = Methods::DIRECT_TRANSCRIPTION;
	} else {
		chosen = Methods::MULTIPLE_SHOOTING;
	}

	bool method_verbose = false;
	bool solver_verbose = false;
	std::unique_ptr<DirectTrajectoryOptimizationProblem<acrobot::acrobotDimensions> > my_snoptdt =
			DirectTrajectoryOptimizationProblem<acrobot::acrobotDimensions>::Create(
					dynamics,
					derivatives,
					costFunction,
					constraints,
					duration,
					number_of_nodes,
					Solvers::SNOPT_SOLVER,
					chosen,
					solver_verbose,
					method_verbose);

	// set-up the TO problem
	state_vector_t initial_state = state_vector_t::Zero();
	state_vector_t final_state;
	final_state << 3.1415926, 0, 0, 0;

	my_snoptdt->SetInitialState(initial_state);
	my_snoptdt->SetFinalState(final_state);

	my_snoptdt->SetEvenTimeIncrements(false);

	my_snoptdt->SetControlBounds(control_vector_t::Constant(-10), control_vector_t::Constant(10));
	my_snoptdt->SetStateBounds(state_vector_t::Constant(-10), state_vector_t::Constant(10));

	// Use analytical Jacobians
	bool solver_numerical_differentiation = false;
	int integration_method = 1; // HermiteSimpson =1 , Trapezoidal = 0;
	int snopt_derivatives_verification = 3;
	bool use_solver_print_file = false;

	my_snoptdt->SetDircolMethod(integration_method);
	my_snoptdt->_solver->SetVerifyLevel(snopt_derivatives_verification);
	my_snoptdt->_solver->UsePrintFile(use_solver_print_file);
	my_snoptdt->InitializeSolver("acrobot", "filename", "",solver_numerical_differentiation);

	// solve the problem
	my_snoptdt->Solve();

	// get the solution
	state_vector_array_t 	y_trajectory;
	control_vector_array_t 	u_trajectory;
	Eigen::VectorXd  		h_trajectory;
	my_snoptdt->GetDTOSolution(y_trajectory, u_trajectory, h_trajectory);

	// print final state
	std::cout << std::endl;
	std::cout << "final state: ";
	std::cout << y_trajectory.back().transpose();
	std::cout << std::endl;

	std::cout << "final_control: ";
	std::cout << u_trajectory.back().transpose();
	std::cout << std::endl;

	std::cout << std::endl << "... end of Test."  << std::endl;

}
