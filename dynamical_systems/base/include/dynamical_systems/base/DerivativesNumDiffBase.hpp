/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file DerivativesNumDiffbase.hpp
 *	\brief Base class to provide automatic numdiff derivatives
 *  \authors depardo
 */

#ifndef DERIVATIVESNUMDIFFBASE_HPP_
#define DERIVATIVESNUMDIFFBASE_HPP_

#include <memory>
#include <limits>
#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/DerivativesBaseDS.hpp>
#include <unsupported/Eigen/NumericalDiff>

template <class DIMENSIONS>
class DerivativesNumDiffBase : public DerivativesBaseDS<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::control_gain_matrix_t control_gain_matrix_t;

	static const int kTotalStatesControls = DIMENSIONS::kTotalStatesControls;
	static const int kTotalControls = DIMENSIONS::kTotalControls;
	static const int kTotalStates = DIMENSIONS::kTotalStates;

	DerivativesNumDiffBase(std::shared_ptr<DynamicsBase<DIMENSIONS> > dynamics );

	virtual ~DerivativesNumDiffBase() {};

	virtual void setCurrentStateAndControl(const state_vector_t& x, const control_vector_t& u);

	template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
	struct Functor {

	public:
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		typedef _Scalar Scalar;
		enum {
			InputsAtCompileTime = NX,
			ValuesAtCompileTime = NY
		};
		typedef typename Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
		typedef typename Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
		typedef typename Eigen::Matrix<Scalar,ValuesAtCompileTime,InputsAtCompileTime> JacobianType;

		int m_inputs, m_values;

		Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
		Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

		int inputs() const { return m_inputs; }
		int values() const { return m_values; }
    virtual ~Functor(){}
	};

	struct dfunctor : Functor<double,kTotalStatesControls,kTotalStates> {

	public:
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		dfunctor(std::shared_ptr <DynamicsBase<DIMENSIONS> > dynamics):
		        Functor<double,kTotalStatesControls,kTotalStates>(),
						local_dynamics(dynamics) {}

		virtual ~dfunctor(){}
		std::shared_ptr<DynamicsBase<DIMENSIONS> > local_dynamics;

		using typename Functor<double,kTotalStatesControls,kTotalStates>::InputType;
		using typename Functor<double,kTotalStatesControls,kTotalStates>::ValueType;

		int operator()(const InputType &in_vect, ValueType  &fvec) const {
			fvec = local_dynamics->systemDynamics(in_vect.template segment<kTotalStates>(0),in_vect.template segment<kTotalControls>(kTotalStates));
			return 0;
		}

	};

	Eigen::Matrix<double,kTotalStatesControls,1> inputs;
	Eigen::Matrix<double,kTotalStates,kTotalStatesControls> Jacobian;

  dfunctor functor_;
  Eigen::NumericalDiff<dfunctor,Eigen::NumericalDiffMode::Forward> numDiff;

	state_matrix_t A_;
	control_gain_matrix_t B_;

	state_matrix_t getDerivativeState() { return A_; }
	control_gain_matrix_t getDerivativeControl() { return B_; }
};

template<class DIMENSIONS>
DerivativesNumDiffBase<DIMENSIONS>::DerivativesNumDiffBase(std::shared_ptr <DynamicsBase<DIMENSIONS> > system_dynamics):
													functor_(system_dynamics),
													numDiff(functor_,std::numeric_limits<double>::epsilon()) {
	inputs.setZero();
	Jacobian.setZero();
	A_.setZero();
	B_.setZero();
}

template <class DIMENSIONS>
void DerivativesNumDiffBase<DIMENSIONS>::setCurrentStateAndControl(const state_vector_t& x, const control_vector_t& u) {

    inputs << x, u;
		numDiff.df(inputs,Jacobian);
		A_ = Jacobian.template block<kTotalStates,kTotalStates>(0,0);
		B_ = Jacobian.template block<kTotalStates,kTotalControls>(0,kTotalStates);
}

template<class DIMENSIONS>
int const DerivativesNumDiffBase<DIMENSIONS>::kTotalStatesControls;

template<class DIMENSIONS>
int const DerivativesNumDiffBase<DIMENSIONS>::kTotalStates;

template<class DIMENSIONS>
int const DerivativesNumDiffBase<DIMENSIONS>::kTotalControls;


#endif /* DERIVATIVESNUMDIFFBASE_HPP_ */
