/*
 * Zmp.hpp
 *
 *  Created on: Jul 22, 2017
 *      Author: depardo
 */

#ifndef _ZMP_HPP_
#define _ZMP_HPP_

#include <Eigen/Dense>
#include <memory>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>

template <class DIMENSIONS>
class ZMP {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::EELambda_t EELambda_t;
  typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;
  typedef typename DIMENSIONS::ZMPVector_t ZMPVector_t;

  ZMP(std::unique_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd):lrd_(std::move(lrd)) {

    fc_ = lrd_->getContactConfiguration();
    is_valid_ = lrd_->grf_->isValid();
  }

  void changeContactConfiguration(const int & c_code){
    lrd_->setContactConfiguration(c_code);
    fc_ = lrd_->getContactConfiguration();
    is_valid_ = lrd_->grf_->isValid();
  }

  ZMPVector_t updateZMP(const GeneralizedCoordinates_t & q);
  ZMPVector_t updateZMP(const GeneralizedCoordinates_t & q,const EELambda_t & lambda);
  ZMPVector_t updateZMP(const state_vector_t & x , const control_vector_t & u);

  std::unique_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd_;
  ZMPVector_t zmp_vector_;
  ContactConfiguration_t fc_;
  bool is_valid_ = true;

};

template <class DIMENSIONS>
typename DIMENSIONS::ZMPVector_t ZMP<DIMENSIONS>::updateZMP(const GeneralizedCoordinates_t & q ,
               const EELambda_t & lambda) {

    lrd_->grf_->setContactForces(lambda);
    zmp_vector_ = updateZMP(q);

  return(zmp_vector_);
}

template <class DIMENSIONS>
typename DIMENSIONS::ZMPVector_t ZMP<DIMENSIONS>::updateZMP(const GeneralizedCoordinates_t & q) {

  //use the current value of the grf and compute a new ZMP

  EEPositionsDataMap_t EEPose_inertia;
  EEPositionsDataMap_t EEPose_base;

  lrd_->getEEPose(q, EEPose_inertia, EEPose_base);

  if(is_valid_){

    double acum_fiz = 0;
    Eigen::Vector2d acum_fp = Eigen::Vector2d::Zero();
    //for each leg in contact
      for(auto lic : lrd_->getContactPointIDVector()){
        double aux = lrd_->grf_->getContactForcesVector()(lic*3+2);
        acum_fiz += aux;
        acum_fp  += EEPose_inertia[lic].segment(0,2) * aux;
      }

    zmp_vector_ = acum_fp/acum_fiz;
  } else {
        zmp_vector_ << -1 , -1;
  }

  return zmp_vector_;
}

template <class DIMENSIONS>
typename DIMENSIONS::ZMPVector_t ZMP<DIMENSIONS>::updateZMP(const state_vector_t & x ,
                                                            const control_vector_t & u) {

  GeneralizedCoordinates_t q,qd;
  // use the dynamics to compute the contact forces
  // ToDo: Move this to GRF
  lrd_->updateContactForces(x,u);

  lrd_->getCoordinatesFromState(x,q,qd);

  zmp_vector_ = updateZMP(q);

  return zmp_vector_;

}
#endif /* _ZMP_HPP_ */
