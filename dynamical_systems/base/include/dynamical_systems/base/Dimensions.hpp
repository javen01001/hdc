/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * Dimensions.hpp
 *
 *  Created on: 26.03.2014 Author: neunertm
 *  Latest version : 04.08.2017 Author: depardo
 *
 */

#ifndef DIMENSIONS_HPP_
#define DIMENSIONS_HPP_

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <dynamical_systems/tools/EE_data_map.h>

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM, size_t DOF,  size_t EE>
class Dimensions {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	enum DimensionsSize {
	  STATE_SIZE = STATE_DIM,
	  CONTROL_SIZE = CONTROL_DIM,
	  JOINTS_SIZE = JOINTS_DIM,
	  SC_SIZE = STATE_DIM + CONTROL_DIM,
	  DOF_SIZE = DOF,
	  EE_SIZE = EE
	  };

	enum CoordinatesOrder {
	  qb0 = 0,
	  qb1,
	  qb2,
	  qb3,
	  qb4,
	  qb5,
	  qr0 = 6
	};

	typedef Eigen::Matrix<double, STATE_DIM, 1> state_vector_t;
	typedef std::vector<state_vector_t, Eigen::aligned_allocator<state_vector_t> > state_vector_array_t;

	typedef Eigen::Matrix<double, STATE_DIM, STATE_DIM> state_matrix_t;
	typedef std::vector<state_matrix_t, Eigen::aligned_allocator<state_matrix_t> > state_matrix_array_t;

	typedef Eigen::Matrix<double, STATE_DIM, CONTROL_DIM> control_gain_matrix_t;
	typedef std::vector<control_gain_matrix_t, Eigen::aligned_allocator<control_gain_matrix_t> > control_gain_matrix_array_t;

	typedef Eigen::Matrix<double, CONTROL_DIM, STATE_DIM> control_feedback_t;
	typedef std::vector<control_feedback_t, Eigen::aligned_allocator<control_feedback_t> > control_feedback_array_t;

	typedef Eigen::Matrix<double, CONTROL_DIM, 1> control_vector_t;
	typedef std::vector<control_vector_t, Eigen::aligned_allocator<control_vector_t> > control_vector_array_t;

	typedef Eigen::Matrix<double, CONTROL_DIM, CONTROL_DIM> control_matrix_t;
	typedef std::vector<control_matrix_t, Eigen::aligned_allocator<control_matrix_t> > control_matrix_array_t;

  typedef Eigen::Matrix<double, STATE_DIM*STATE_DIM , 1 > state_matrix_vectorized_t;

	typedef Eigen::Matrix<double, 1, 1> scalar_t;
	typedef std::vector<scalar_t> scalar_array_t;
  typedef std::vector<int> EEJoints;

  constexpr static double kGravity = 9.8;
  static const int kTotalJoints = static_cast<int>(JOINTS_DIM);
  static const int kBaseDof = 6;
  static const int kSingleEEContactForceSize = 3;
  static const int kEEJoints = 3;
  static const int kTotalEE = static_cast<int>(EE);
  static const int kTotalDof = static_cast<int>(DOF);
  static const int kTotalControls = static_cast<int>(CONTROL_DIM);
  static const int kTotalStates = static_cast<int>(STATE_DIM);
  static const int kTotalStatesControls = kTotalStates + kTotalControls;
  static const int kContactForcesSize = kSingleEEContactForceSize*kTotalEE;
  static int kTotalConfigurations;

  enum BaseCoordinatesOrder {
    roll = 0,
    pitch,
    yaw,
    LX,
    LY,
    LZ
  };

	typedef Eigen::Matrix<double,kTotalDof,kTotalDof> InertiaMatrix_t;
	typedef control_matrix_t JointsInertiaMatrix_t;

	typedef Eigen::Matrix<double,kTotalDof,1>   GeneralizedCoordinates_t;
	typedef Eigen::Matrix<double,kBaseDof,1>    BaseCoordinates_t;
	typedef Eigen::Matrix<double,JOINTS_DIM,1>  JointCoordinates_t;
	typedef Eigen::Matrix<double,CONTROL_DIM,1> TorqueCommand_t;

  typedef Eigen::Matrix<double, 3, 1> EEPosition_t;
  typedef Eigen::Matrix<double, 3, 1> EEVelocity_t;
  typedef Eigen::Matrix<double, 3, 1> EEForce_t;
  typedef Eigen::Matrix<double, 2, 1> ZMPVector_t;

  typedef Eigen::Matrix<double, 6, 1> Vector6D_t;
  typedef Eigen::Matrix<double, 6, 6> Matrix66D_t;

  typedef EEDataMap<EEPosition_t,EE> EEPositionsDataMap_t;
  typedef EEDataMap<EEVelocity_t,EE> EEVelocityDataMap_t;
  typedef EEDataMap<EEForce_t,EE> EEForceDataMap_t;
  typedef EEDataMap<bool,DimensionsSize::EE_SIZE> ContactConfiguration_t;

  typedef Eigen::Matrix<double,kContactForcesSize,1> EELambda_t;
  typedef Eigen::Matrix<double,EE,1> EELambda_single_component_t;
  typedef Eigen::Matrix<double,EE,1> EEFriction_cone_coefficient_t;

  typedef Eigen::Matrix<double,kSingleEEContactForceSize,kEEJoints> SingleEEJointJacobian_t;
  typedef Eigen::Matrix<double,kSingleEEContactForceSize,kBaseDof> SingleEEBaseJacobian_t;
  typedef Eigen::Matrix<double,kSingleEEContactForceSize,kEEJoints+kBaseDof> SingleEEJacobian_t;
  typedef Eigen::Matrix<double,kContactForcesSize,kTotalDof> EEJacobian_t;
  typedef Eigen::Matrix<double,kTotalDof,kContactForcesSize> EEJacobianTranspose_t;
  typedef Eigen::Matrix<double,kTotalJoints,kTotalJoints> JointMatrix_t;
  typedef Eigen::Matrix<double,kBaseDof,kTotalJoints> BaseJointMatrix_t;

  typedef EEDataMap<SingleEEJacobian_t,EE> EESingleJacobianDataMap_t;

  typedef std::vector<GeneralizedCoordinates_t,
      Eigen::aligned_allocator<GeneralizedCoordinates_t> > GeneralizedCoordinates_array_t;
  typedef std::vector<BaseCoordinates_t,
      Eigen::aligned_allocator<BaseCoordinates_t> > BaseCoordinates_array_t;
  typedef std::vector<JointCoordinates_t,
      Eigen::aligned_allocator<JointCoordinates_t> > JointCoordinates_array_t;
  typedef std::vector<EELambda_t,
      Eigen::aligned_allocator<EELambda_t> > EELambda_array_t;
  typedef std::vector<EEPosition_t,
      Eigen::aligned_allocator<EEPosition_t> > EEPosition_array_t;
  typedef std::vector<EEVelocity_t,
      Eigen::aligned_allocator<EEVelocity_t> > EEVelocity_array_t;
  typedef std::vector<ZMPVector_t,
      Eigen::aligned_allocator<ZMPVector_t> > ZMPVector_array_t;
  typedef std::vector<ContactConfiguration_t,
      Eigen::aligned_allocator<ContactConfiguration_t> > ContactConfiguration_array_t;

  // for control
  typedef Eigen::Matrix<double, kTotalDof, kTotalDof > pos_feedback_gain_t;
  typedef Eigen::Matrix<double, kTotalDof, kTotalDof > vel_feedback_gain_t;

  static ContactConfiguration_array_t get_all_contact_configurations(){
    set_all_contact_configurations();
    return robotEEConfiguration;
  }

  static std::vector<std::string> robot_joint_names;

  static std::vector<std::string> joint_names(){
    return(robot_joint_names);
  }
private:
  //all possible combinations of contact configuration!
  static ContactConfiguration_array_t robotEEConfiguration;
  static bool compute_flag;
  static void set_all_contact_configurations();

};

//template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>


template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalDof;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kBaseDof;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kSingleEEContactForceSize;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kEEJoints;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM, size_t DOF, size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalStates;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM, size_t DOF, size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalControls;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM, size_t DOF, size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalStatesControls;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM, size_t DOF, size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalJoints;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalEE;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int const Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kContactForcesSize;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
bool Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::compute_flag = false;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
int Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::kTotalConfigurations;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
typename Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::ContactConfiguration_array_t Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::robotEEConfiguration;

template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
void Dimensions<STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::set_all_contact_configurations() {
  if(compute_flag) return;
  else {
    compute_flag = true;
    kTotalConfigurations = std::pow(2,kTotalEE);
    for(auto c = 0 ; c < kTotalConfigurations ; ++c) {
        ContactConfiguration_t fc(false);
        double rem = (double)(c);
        for(int p = kTotalEE - 1 ; p >= 0 ; --p) {
            double table = std::pow(2,p);
            if(rem >= table) {
               fc[p] = true;
               rem = rem - table;
            } else fc[p] = false;
        }
        robotEEConfiguration.push_back(fc);
    }
  }
}

#endif /* DIMENSIONS_DS_HPP_ */
