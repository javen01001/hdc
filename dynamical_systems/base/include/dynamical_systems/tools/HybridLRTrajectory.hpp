/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * HybridLRTrajectory.hpp
 *
 *  Created on: May 3, 2017
 *      Author: depardo
 */

#ifndef _HYBRIDLRTRAJECTORY_HPP_
#define _HYBRIDLRTRAJECTORY_HPP_

#include <memory>
#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <dynamical_systems/tools/SystemTrajectory.hpp>
#include <dynamical_systems/tools/LRPhase.hpp>
#include <dynamical_systems/tools/LeggedRobotTrajectory.hpp>


/**
 * @brief: A Collection of LRphases
 */

template <class DIMENSIONS>
class HybridLRTrajectory {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

  HybridLRTrajectory(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd):lrd_(lrd){};

  std::vector<std::shared_ptr<LRPhase<DIMENSIONS> > > lrp_vector_;
  std::vector<double> phase_duration_;
  std::vector<double> phase_size_;

  std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd_;
  std::shared_ptr<LRPhase<DIMENSIONS>> lrp;

  bool setHybridTrajectory(const state_vector_array_t & x, const control_vector_array_t & u , const Eigen::VectorXd & t,
                      const std::vector<int> & p_code, const std::vector<int> & c_code);
  bool setHybridTrajectory(LeggedRobotTrajectory<DIMENSIONS> lrt);

  int number_of_phases_ = 0;

  //ToDo: Validate new trajectories q_k = q_k+1 & t_end = t_start
  void addPhase(std::shared_ptr<LRPhase<DIMENSIONS>> lrp);

};

template <class DIMENSIONS>
bool HybridLRTrajectory<DIMENSIONS>::setHybridTrajectory(LeggedRobotTrajectory<DIMENSIONS> lrt) {

  return(setHybridTrajectory(lrt.x_t,lrt.u_t,lrt.t_,lrt.p_code_,lrt.c_code_));
}

template <class DIMENSIONS>
void HybridLRTrajectory<DIMENSIONS>::addPhase(std::shared_ptr<LRPhase<DIMENSIONS>> lrp) {

  //TODO: ADD VALIDATIONS TO LRP

  lrp_vector_.push_back(lrp);
  number_of_phases_++;

}

template <class DIMENSIONS>
bool HybridLRTrajectory<DIMENSIONS>::setHybridTrajectory(const state_vector_array_t & x, const control_vector_array_t & u , const Eigen::VectorXd & t,
                                                         const std::vector<int> & p_code, const std::vector<int> & c_code) {

  bool flag_ok = false;

  int l_size = x.size();
  if(u.size() != l_size || p_code.size() != l_size || c_code.size() != l_size || t.size() != l_size) {
      return false;
  }

  int l_nphase = 0;
  //int l_pcode = 1;

  int phase_size = 0;
  int index_s = 0;

  state_vector_array_t x_phase;
  control_vector_array_t u_phase;
  std::vector<int> p_phase,c_phase;


  for(int k = 0; k < l_size - 1; k++ ) {

      // the single phase dynamics right dynamics
      lrp = std::shared_ptr<LRPhase<DIMENSIONS>>(new LRPhase<DIMENSIONS>(lrd_,c_code[k]));

      // every point in the phase
      phase_size++;
      x_phase.push_back(x[k]);
      u_phase.push_back(u[k]);
      p_phase.push_back(p_code[k]);
      c_phase.push_back(c_code[k]);

      //only if k is a switching point
      if(p_code[k+1] != p_code[k]) {

          // counting phases
          l_nphase++;

          // the chunk of time vector
          Eigen::VectorXd t_phase = t.segment(index_s,phase_size);

          // add the trajectory to the phase
          flag_ok = lrp->updateLRTrajectory(x_phase,u_phase,t_phase,p_phase,c_phase);
          if(!flag_ok) {
              std::cout <<"Something is wrong with the splinning!" << std::endl;
              return false;
          }

          addPhase(lrp);

          x_phase.clear();
          u_phase.clear();
          p_phase.clear();
          c_phase.clear();

          index_s = index_s + phase_size;

          double phase_duration = t_phase(phase_size-1) - t_phase(0);

          phase_size_.push_back(phase_size);
          phase_duration_.push_back(phase_duration);

          phase_size = 0;

      }
      // otherwise if k is the previous to last point (it will never be a switching point)
      else if(k == l_size-2) {
          phase_size++;
          x_phase.push_back(x[k+1]);
          u_phase.push_back(u[k+1]);
          p_phase.push_back(p_code[k+1]);
          c_phase.push_back(c_code[k+1]);

          Eigen::VectorXd t_phase = t.segment(index_s,phase_size);

          flag_ok = lrp->updateLRTrajectory(x_phase,u_phase,t_phase,p_phase,c_phase);
          if(!flag_ok){
              return false;
          }

          phase_size_.push_back(phase_size);
          double phase_duration = t_phase(phase_size-1) - t_phase(0);
          phase_duration_.push_back(phase_duration);

          addPhase(lrp);
      }
    }


  return flag_ok;
}


#endif /* _HYBRIDLRTRAJECTORY_HPP_ */
