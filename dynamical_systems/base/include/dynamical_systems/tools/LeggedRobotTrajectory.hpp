/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * LeggedRobotTrajectory.hpp
 *
 *  Created on: May 3, 2017
 *      Author: depardo
 */

#ifndef _LEGGEDROBOTTRAJECTORY_HPP_
#define _LEGGEDROBOTTRAJECTORY_HPP_

#include <Eigen/Dense>
#include <memory>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <dynamical_systems/tools/SystemTrajectory.hpp>


template <class DIMENSIONS>
class LeggedRobotTrajectory : public SystemTrajectory<DIMENSIONS> {


public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
  typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;

  typedef typename DIMENSIONS::EEPosition_t EEPosition_t;
  typedef typename DIMENSIONS::EEVelocity_t EEVelocity_t;
  typedef typename DIMENSIONS::EELambda_t EELambda_t;
  typedef typename DIMENSIONS::EELambda_array_t EELambda_array_t;
  typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;

  typedef typename DIMENSIONS::BaseCoordinates_t BaseCoordinates_t;
  typedef typename DIMENSIONS::JointCoordinates_t JointCoordinates_t;

  typedef typename DIMENSIONS::BaseCoordinates_array_t BaseCoordinates_array_t;
  typedef typename DIMENSIONS::JointCoordinates_array_t JointCoordinates_array_t;

  static const int base_dof  = static_cast<int>(DIMENSIONS::kBaseDof);
  static const int joint_dof = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE);
  static const int ee_size   = static_cast<int>(DIMENSIONS::DimensionsSize::EE_SIZE);

  LeggedRobotTrajectory(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd,
                        const state_vector_array_t & x, const control_vector_array_t & u,
                        const Eigen::VectorXd & time, const std::vector<int> & p_code,
                        const std::vector<int> & c_code):
                        SystemTrajectory<DIMENSIONS>(x,u,time), lrd_(lrd),p_code_(p_code), c_code_(c_code){

    refreshLRTrajectory();
  }

  LeggedRobotTrajectory(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd):lrd_(lrd){
    this->trajectory_is_ok = false;
  }

  virtual ~LeggedRobotTrajectory(){}

  std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd_;

  std::vector<int> c_code_;
  std::vector<int> p_code_;

  BaseCoordinates_array_t q_base_t;
  BaseCoordinates_array_t qd_base_t;
  BaseCoordinates_array_t qdd_base_t;

  JointCoordinates_array_t q_joints_t;
  JointCoordinates_array_t qd_joints_t;
  JointCoordinates_array_t qdd_joints_t;

  EELambda_array_t EEPosition_world_t;
  EELambda_array_t EEPosition_base_t;

  // Handling dynamic variables
  bool dynamic_variables_ready = false;
  EELambda_array_t lambda_t;

  /**
   * @brief : update Legged Robot Trajectory
   */
  bool updateLRTrajectory(const state_vector_array_t & x , const control_vector_array_t & u , const Eigen::VectorXd t ,
                         const std::vector<int> & p_code , const std::vector<int> & c_code);

  /**
   * @brief : compute contact forces and state derivatives
   */
  void updateDynamicVariables();

  /**
   * @brief : refresh all the variables of the trajectory using the current x,u,t,p,c
   */
  bool refreshLRTrajectory();

  /**
   * @brief : clear trajectory plus LR variables
   */
  void clearLRTrajectory();

private:
  void updateLRVariables();

};

template <class DIMENSIONS>
bool LeggedRobotTrajectory<DIMENSIONS>::updateLRTrajectory(const state_vector_array_t & x , const control_vector_array_t & u , const Eigen::VectorXd t ,
                         const std::vector<int> & p_code , const std::vector<int> & c_code) {

    clearLRTrajectory();
    this->trajectory_is_ok = this->updateTrajectory(x,u,t);
    if(p_code.size()!=this->trajectory_size || c_code.size() != this->trajectory_size) {
        this->trajectory_is_ok = false;
    } else {
     p_code_ = p_code;
     c_code_ = c_code;
     this->trajectory_is_ok = refreshLRTrajectory();
    }

    return this->trajectory_is_ok;
}

template <class DIMENSIONS>
bool LeggedRobotTrajectory<DIMENSIONS>::refreshLRTrajectory() {

  //all values are refreshed using current x,u,t,p,c, therefore I can assume SystemTrajectory is ok

  if(p_code_.size()!=this->trajectory_size || c_code_.size() != this->trajectory_size) {
      clearLRTrajectory();
      this->trajectory_is_ok = false;
  } else {
      updateDynamicVariables();
      updateLRVariables();
      this->trajectory_is_ok = true;
  }

  return this->trajectory_is_ok;

}

template <class DIMENSIONS>
void LeggedRobotTrajectory<DIMENSIONS>::updateLRVariables() {

  q_base_t.clear()   ; q_joints_t.clear();
  qd_base_t.clear()  ; qd_joints_t.clear();

  EEPositionsDataMap_t EEPosition_w;
  EEPositionsDataMap_t EEPosition_b;

  EELambda_t ee_pos_world;
  EELambda_t ee_pos_base;

  for(int k = 0 ; k < this->trajectory_size ; ++k) {

      q_base_t.push_back(this->q_t[k].template segment<base_dof>(0));
      q_joints_t.push_back(this->q_t[k].template segment<joint_dof>(base_dof));

      qd_base_t.push_back(this->qd_t[k].template segment<base_dof>(0));
      qd_joints_t.push_back(this->qd_t[k].template segment<joint_dof>(base_dof));

      lrd_->setContactConfiguration(c_code_[k]);

      //please fix this afterwards!
      lrd_->getEEPose(this->q_t[k],EEPosition_w,EEPosition_b);

      for(int l = 0 ; l < ee_size ; l++ ){
          ee_pos_world.template segment<3>(l*3) = EEPosition_w[l];
          ee_pos_base.template segment<3>(l*3) = EEPosition_b[l];
      }
      EEPosition_world_t.push_back(ee_pos_world);
      EEPosition_base_t.push_back(ee_pos_base);
  }

}


template <class DIMENSIONS>
void LeggedRobotTrajectory<DIMENSIONS>::updateDynamicVariables() {

  lambda_t.clear();
  qdd_base_t.clear() ; qdd_joints_t.clear();

  EELambda_t lambda;
  state_vector_array_t xd;

  for(int k = 0 ; k < this->trajectory_size ; ++k) {

      lrd_->setContactConfiguration(c_code_[k]);
      lrd_->updateState(this->x_t[k]);
      lrd_->updateContactForces(this->u_t[k]);
      lrd_->grf_->getContactForcesVector(lambda);

      lambda_t.push_back(lambda);
      xd.push_back(lrd_->systemDynamics(this->x_t[k],this->u_t[k]));
  }

  this->setStateDerivatives(xd);


  for(int k = 0 ; k < this->trajectory_size ; ++k) {

      qdd_base_t.push_back(this->qdd_t[k].template segment<base_dof>(0));
      qdd_joints_t.push_back(this->qdd_t[k].template segment<joint_dof>(base_dof));
  }

  // Other dynamic variables can be computed here, for example:
  // dynamic constraints
  // Support Polygon?
  // CoP
}

template <class DIMENSIONS>
void LeggedRobotTrajectory<DIMENSIONS>::clearLRTrajectory() {

  this->clearTrajectory();

  p_code_.clear();
  c_code_.clear();

  q_base_t.clear();
  qd_base_t.clear();
  qdd_base_t.clear();

  q_joints_t.clear();
  qd_joints_t.clear();
  qdd_joints_t.clear();

  lambda_t.clear();

  EEPosition_world_t.clear();
  EEPosition_base_t.clear();

}

template <class DIMENSIONS>
const int LeggedRobotTrajectory<DIMENSIONS>::base_dof;

template <class DIMENSIONS>
const int LeggedRobotTrajectory<DIMENSIONS>::joint_dof;

template <class DIMENSIONS>
const int LeggedRobotTrajectory<DIMENSIONS>::ee_size;

#endif /* _LEGGEDROBOTTRAJECTORY_HPP_ */
