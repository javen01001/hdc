/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * LRConversions.hpp
 *
 *  Created on: May 11, 2017
 *      Author: depardo
 */

#ifndef _LRCONVERSIONS_HPP_
#define _LRCONVERSIONS_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>

template <class DIMENSIONS>
class LRConversions {

  public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::BaseCoordinates_t BaseCoordinates_t;
  typedef typename DIMENSIONS::JointCoordinates_t JointCoordinates_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
  typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;
  typedef typename DIMENSIONS::Vector6D_t Vector6D_t;
  typedef typename DIMENSIONS::Matrix66D_t Matrix66D_t;
  typedef typename DIMENSIONS::InertiaMatrix_t InertiaMatrix_t;
  typedef typename DIMENSIONS::EEJacobianTranspose_t EEJacobianTranspose_t;

  static const int kTotalEE     = static_cast<int>(DIMENSIONS::kTotalEE);
  static const int kEEJoints    = static_cast<int>(DIMENSIONS::kEEJoints);
  static const int kTotalJoints = static_cast<int>(DIMENSIONS::kTotalJoints);
  static const int kTotalDof = static_cast<int>(DIMENSIONS::kTotalDof);
  static const int kBaseDof  = static_cast<int>(DIMENSIONS::kBaseDof);
  static const int kSingleEEContactForceSize = static_cast<int>(DIMENSIONS::kSingleEEContactForceSize);
  static const int kTotalControls = static_cast<int>(DIMENSIONS::kTotalControls);
  static const int kTotalStates = static_cast<int>(DIMENSIONS::kTotalStates);
  static const int kContactForcesSize = kSingleEEContactForceSize*kTotalEE;
  static constexpr double kGravity = DIMENSIONS::kGravity;

	LRConversions() {};

	virtual ~LRConversions() { };

	/**
	 * @brief Builds the vector of Generalized Coordinates from its components (base & joints)
	 * @param base_q Base components
	 * @param joints_q_r joints components
	 * @param result Complete vector of Generalized Coordinates
	 */
	void GeneralizedCoordinatesFromComponents(const BaseCoordinates_t & base_q , const JointCoordinates_t & joints_q_r,
	                                          GeneralizedCoordinates_t & result);

	/**
	 * @brief Get the joint coordinates from the vector of GeneralizedCoordinates
	 * @param big_q GeneralizedCoordinates
	 * @param joints_q_r joint coordinates
	 */
	void GetActuatedCoordinates(const GeneralizedCoordinates_t & big_q , JointCoordinates_t & joints_q_r);

	/**
	 * @brief Get the base coordinates from the vector of GeneralizedCoordinates
	 * @param big_q the generalized coordinates
	 * @param base_q the base coordinates
	 */
	void GetFloatingBaseCoordinates(const GeneralizedCoordinates_t & big_q , BaseCoordinates_t & base_q);

	/**
	 * @brief Get base position and velocity from GeneralizedCoordinates
	 * @param q the generalized coordinates
	 * @param qd the body velocities
	 * @param trunk_position base position
	 * @param trunk_velocity base velocity
	 */
	void BodyStateFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q,const GeneralizedCoordinates_t & qd,
	                                         BaseCoordinates_t & trunk_position, BaseCoordinates_t & trunk_velocity);

	/**
	 * @brief Get joint posistion and velocity from GeneralizedCoordinates
	 * @param q the generalized coordinates
	 * @param qd the body velocities
	 * @param jointposition the base position
	 * @param jointvelocity the base velocity
	 */
	void JointStateFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q , const GeneralizedCoordinates_t & qd ,
	                                          JointCoordinates_t & jointposition, JointCoordinates_t & jointvelocity);

	/**
	 * @brief Get Generalized Coordinates position and velocity from state
	 * @param x the robot state
	 * @param q generalized coordinates position
	 * @param qd generalized coordinates velocities
	 */
	void GeneralizedCoordinatesFromStateVector(const state_vector_t & x , GeneralizedCoordinates_t& q,
	                                           GeneralizedCoordinates_t& qd);

	/**
	 * @brief Get robot state from GeneralizedCoordinates components
	 * @param q the generalized coordinates position
	 * @param qd the generalized coordinates velocities
	 * @param x the robot state
	 */
	void StateVectorFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q, const GeneralizedCoordinates_t & qd ,
	                                           state_vector_t & x);

	/**
	 * brief Get all components from state vector
	 * @param x the robot state
	 * @param q the generalized coordinates position
	 * @param qd the generalized coordinates velocities
	 * @param q_b the base position
	 * @param qd_b the base velocities
	 * @param q_r the joint positions
	 * @param qd_r the joint velocities
	 */
	void ComponentsFromStateVector(const state_vector_t & x ,
	                               GeneralizedCoordinates_t & q , GeneralizedCoordinates_t & qd,
	                               BaseCoordinates_t  & q_b , BaseCoordinates_t  & qd_b,
	                               JointCoordinates_t & q_r , JointCoordinates_t & qd_r);

	/**
	 * @brief Get all components from Generalized Coordinates
	 * @param q the generalized coordinates position / velocity
	 * @param q_b the base position / velocity
	 * @param q_r the joint position / velocity
	 */
	void ComponentsFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q ,
	                                          BaseCoordinates_t & q_b , JointCoordinates_t & q_r);

  /**
   * @brief computes the Affine transformation W_X_B from the base pose
   * @param big_q robot configuration
   * @param A_wb W_X_B affine transformation
   */
	void GetWBTransform(const GeneralizedCoordinates_t & big_q , Eigen::Affine3d & A_wb);

	/**
	 * @brief computes the Affine transformation W_X_B from the base pose
	 * @param base_pose position and orientation of the base of the robot (Euler angles)
	 * @param A_wb W_X_B affine transformation (Extrinsic rotation ZYX)
	 */
	void GetWBTransform(const BaseCoordinates_t & base_pose, Eigen::Affine3d & A_wb);

	/**
	 * @brief computes the position of the EE in Inertia frame
	 * @param base_transformation the W_X_B Affine transformation (Extrinsic rotation)
	 * @param p_base the DataMap of Vector3d representing the position of all EE in base reference frame
	 * @param p_inertia the DataMap of Vector3d representing the position of all EE in Inertia reference frame
	 */
  void GetFeetPoseInertia(const Eigen::Affine3d & base_transformation, const EEPositionsDataMap_t & p_base, EEPositionsDataMap_t & p_inertia);

  /**
   * @brief computes the Selection Matrix for the columns of Q^T corresponding to the unconstrained space
   * @param contact_feet Data Map with booleand description of the EE in contact
   * @param Su The Selection Matrix
   */
  template<typename Derived>
  void GetUnconstrainedSpaceMatrixS(const ContactConfiguration_t & contact_feet, Eigen::MatrixBase<Derived> & Su);

  /**
   * @brief Computes the skew symmetric matrix [ax] correspondent to a given vector a x b = [ax].b
   * @param input_vector a Vector3d (a)
   * @param skew_matrix a Matrix33d with the skew symmetric matrix [ax]
   */
  void GetSkewMatrix(const Eigen::Vector3d & input_vector , Eigen::Matrix3d & skew_matrix);

  /**
   * @brief Computes the ZYX euler angles from a rotation matrix
   * @param tr the Matrix3d rotation matrix
   * @param rpy the Vector3d euler angles ZYX
   */
  void GetRPYangles(const Eigen::Matrix3d & tr, Eigen::Vector3d& rpy);

  /**
   * @brief QR Decomposition of a JacobianTranspose matrix, such that PAP'=QR
   * @param A The (n+6) x k JacobianTranspose Matrix to decompose
   * @param Q The (n+6) x (n+6) unitary matrix
   * @param R The upper triangular (n+6) x k matrix
   * @param P The permutation matrix
   * @param rank The rank fo the JacobianTranspose Matrix
   */
  template<typename Derived1,typename Derived2>
  void QRDecomposition(const EEJacobianTranspose_t & A, InertiaMatrix_t & Q,
                       Eigen::MatrixBase<Derived1> & R, Eigen::MatrixBase<Derived2> &P,
                       int & rank);

  /**
   * @brief computes the inverse of a matrix
   * @param A the matrix to invert
   * @param tolerance the smallest value considerd as zero
   * @return the inverse of A
   */
  template<typename Derived>
  Eigen::Matrix<double, Derived::ColsAtCompileTime,Derived::RowsAtCompileTime> psdInv(const Eigen::MatrixBase<Derived> & A, const double & tolerance);

  template<typename Derived>
  Eigen::Matrix<double, Derived::ColsAtCompileTime,Derived::RowsAtCompileTime> psdInv(const Eigen::MatrixBase<Derived> & in, const double & tolerance,
                                                                                      Eigen::Matrix<double,Derived::RowsAtCompileTime,Derived::RowsAtCompileTime> & U,
                                                                                      Eigen::MatrixBase<Derived> & S,
                                                                                      Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::ColsAtCompileTime> & V);

  /**
   * @brief computes the Singular Value Decomposition of an mxn matrix
   * @param A the mxn matrix
   * @param U the mxm unitary matrix (columns are the left singular vectors of A)
   * @param S the mxn diagonal matrix with the singular values of A
   * @param V the nxn unitary matrix (columns are the right singular vectors of A)
   */
  template <typename Derived>
  void svDecomp(const Eigen::MatrixBase<Derived> & A,
                Eigen::Matrix<double,Derived::RowsAtCompileTime,Derived::RowsAtCompileTime> & U,
                Eigen::MatrixBase<Derived> & S,
                Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::ColsAtCompileTime> & V);


  // Not ready for use consider removing
  void GetWXB_TransformationMatrix(const Eigen::Affine3d & Awb, Matrix66D_t & motion_transformation, bool reverse /*=false*/ );
  void WorldXbase_velocityTransformation(const GeneralizedCoordinates_t & big_q , const Vector6D_t & in_vector , Vector6D_t & out_vector, bool reverse=false);
  void WorldXbase_motionTransformation(const Eigen::Affine3d & Afb , const Vector6D_t & in_vector, Vector6D_t & out_vector, bool reverse=false);

  private:

    Eigen::Vector3d uX = Eigen::Vector3d::UnitX();
    Eigen::Vector3d uY = Eigen::Vector3d::UnitY();
    Eigen::Vector3d uZ = Eigen::Vector3d::UnitZ();
};

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kBaseDof;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kTotalEE;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kTotalJoints;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kTotalDof;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kEEJoints;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kSingleEEContactForceSize;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kTotalControls;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kTotalStates;

template <class DIMENSIONS>
int const LRConversions<DIMENSIONS>::kContactForcesSize;

template <class DIMENSIONS>
double constexpr LRConversions<DIMENSIONS>::kGravity;

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetFeetPoseInertia(const Eigen::Affine3d & base_transformation, const EEPositionsDataMap_t & p_base,
                                       EEPositionsDataMap_t & p_inertia) {

  for(int k = 0 ; k < kTotalEE ; ++k)
    p_inertia[k]  = base_transformation * p_base[k];
}

template <class DIMENSIONS>
template <typename Derived>
void LRConversions<DIMENSIONS>::GetUnconstrainedSpaceMatrixS(const ContactConfiguration_t & contact_feet,
                                                             Eigen::MatrixBase<Derived> & S) {

  /*
   * S = [ S_zero S_Identity ]  : Selects the columns of Q^T corresponding to the unconstrained space
   */
  int number_of_feet_contact = 0;

  for(int leg = 0 ; leg < 4 ; leg ++) {
    if(contact_feet[leg])
      number_of_feet_contact++;
  }

  int wrench_components = 3;
  int contact_forces =  number_of_feet_contact * wrench_components;
  int selection_matrix_rows   = kTotalDof - contact_forces;  // Selection matrix is dynamic
  int selection_matrix_cols   = kTotalDof;

  S.resize(selection_matrix_rows,selection_matrix_cols);
  S.setZero();
  S.block(0,contact_forces,selection_matrix_rows,selection_matrix_rows).setIdentity(); //These are always unconstrained
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::WorldXbase_velocityTransformation(const GeneralizedCoordinates_t & big_q , const Vector6D_t & in_vector,
    Vector6D_t & out_vector, bool reverse /*=false*/) {

  BaseCoordinates_t base_pose_worldFrame;
  Eigen::Affine3d Awb;

  GetFloatingBaseCoordinates(big_q,base_pose_worldFrame);
  GetWBTransform(base_pose_worldFrame,Awb);
  WorldXbase_motionTransformation(Awb , in_vector, out_vector , reverse);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::WorldXbase_motionTransformation(const Eigen::Affine3d & Awb, const Vector6D_t & in_vector,
    Vector6D_t & out_vector, bool reverse /*=false*/) {

  Matrix66D_t motion_transformation;
  GetWXB_TransformationMatrix(Awb,motion_transformation,reverse);
  out_vector = motion_transformation * in_vector;
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetWXB_TransformationMatrix(const Eigen::Affine3d & Awb,
                                                Matrix66D_t & motion_transformation, bool reverse /*=false*/ ) {

  // w_R_b : Rotation Matrix that transforms 3D vector from Base to world
  Eigen::Matrix3d w_R_b = Awb.linear();
  Eigen::Matrix3d b_R_w = w_R_b.transpose();

  // rwb : Locates the origin of the Base in World Coordinates
  Eigen::Vector3d rwb = Awb.translation();

  // rwb_base
  Eigen::Vector3d rwb_base = -b_R_w*rwb;

  // corresponding skew symmetric matrix
  Eigen::Matrix3d skew_matrix;
  GetSkewMatrix(rwb_base,skew_matrix);

  motion_transformation.setZero();

  if(!reverse) {
      motion_transformation.template block<3,3>(0,0) = w_R_b;
      motion_transformation.template block<3,3>(3,3) = w_R_b;
      motion_transformation.template block<3,3>(3,0) = -w_R_b*skew_matrix;
  } else {

    motion_transformation.template block<3,3>(0,0) = b_R_w;
    motion_transformation.template block<3,3>(3,3) = b_R_w;
    motion_transformation.template block<3,3>(3,0) = skew_matrix*b_R_w;
  }
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetSkewMatrix(const Eigen::Vector3d & input_vector , Eigen::Matrix3d & skew_matrix) {

  skew_matrix.setZero();

  skew_matrix(0,1) = - input_vector(2); //-z
  skew_matrix(0,2) =   input_vector(1); // y
  skew_matrix(1,0) =   input_vector(2); // z
  skew_matrix(1,2) = - input_vector(0); //-x
  skew_matrix(2,0) = - input_vector(1); //-y
  skew_matrix(2,1) =   input_vector(0); // x
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetRPYangles(const Eigen::Matrix3d & tr, Eigen::Vector3d& rpy) {

  // this function works for pitch = [ -pi/2 , pi/2]
  // and degenerates at cos(pitch) = 0

  rpy(2) =  std::atan2(  tr(1,0) , tr(0,0));  // Yawing in Z
  rpy(1) =  std::atan2( -tr(2,0)  , std::sqrt(std::pow(tr(2,1),2) + std::pow(tr(2,2),2))); // Pitching in Y
  rpy(0) =  std::atan2(  tr(2,1) , tr(2,2));  // Rolling in X
}

template <class DIMENSIONS>
template <typename Derived1 , typename Derived2>
void LRConversions<DIMENSIONS>::QRDecomposition(const EEJacobianTranspose_t & A, InertiaMatrix_t & Q,
                                                Eigen::MatrixBase<Derived1> & R, Eigen::MatrixBase<Derived2> &P,
                                                int & rank) {

  Eigen::ColPivHouseholderQR<EEJacobianTranspose_t> QRD(A);
  Q = QRD.matrixQ();
  Eigen::MatrixXd PD = QRD.matrixQR();
  R = PD.triangularView<Eigen::Upper>();
  P = QRD.colsPermutation();
  rank = QRD.rank();

}

template <class DIMENSIONS>
template <typename Derived>
void LRConversions<DIMENSIONS>::svDecomp(const Eigen::MatrixBase<Derived> & A,
                                         Eigen::Matrix<double,Derived::RowsAtCompileTime,Derived::RowsAtCompileTime> & U,
                                         Eigen::MatrixBase<Derived> & S,
                                         Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::ColsAtCompileTime> & V){
  Eigen::JacobiSVD<Derived> svd;
  svd.compute(A,Eigen::ComputeFullU | Eigen::ComputeFullV);
  S=A;S.setZero();
  S.diagonal() = svd.singularValues();
  U = svd.matrixU();
  V = svd.matrixV();
}

template <class DIMENSIONS>
template <typename Derived>
Eigen::Matrix<double, Derived::ColsAtCompileTime,Derived::RowsAtCompileTime> LRConversions<DIMENSIONS>::psdInv(const Eigen::MatrixBase<Derived> & in, const double & tolerance) {

  Derived S;
  Eigen::Matrix<double,Derived::RowsAtCompileTime,Derived::RowsAtCompileTime> U;
  Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::ColsAtCompileTime> V;
  return (psdInv(in,tolerance,U,S,V));
}

template <class DIMENSIONS>
template <typename Derived>
Eigen::Matrix<double, Derived::ColsAtCompileTime,Derived::RowsAtCompileTime> LRConversions<DIMENSIONS>::psdInv(const Eigen::MatrixBase<Derived> & in, const double & tolerance,
                                                                                                               Eigen::Matrix<double,Derived::RowsAtCompileTime,Derived::RowsAtCompileTime> & U,
                                                                                                               Eigen::MatrixBase<Derived> & S,
                                                                                                               Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::ColsAtCompileTime> & V) {

  Eigen::Matrix<double,Derived::ColsAtCompileTime,Derived::RowsAtCompileTime> out,S_;

  svDecomp(in,U,S,V);
  S_=S.transpose();S_.setZero();
  S_.diagonal() = (S.diagonal().array()>tolerance).select(S.diagonal().array().inverse(), 0);
  out =  V *  S_ * U.transpose();
  return (out);
}


template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GeneralizedCoordinatesFromComponents(const BaseCoordinates_t & base_q ,
                      const JointCoordinates_t & joints_q_r , GeneralizedCoordinates_t & result) {

  result.template segment<kBaseDof>(DIMENSIONS::CoordinatesOrder::qb0) = base_q;
  result.template segment<kTotalJoints>(DIMENSIONS::CoordinatesOrder::qr0) = joints_q_r;
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetActuatedCoordinates(const GeneralizedCoordinates_t & big_q,
                                      JointCoordinates_t & joint_coordinates) {

  joint_coordinates = big_q.template segment<kTotalJoints>(DIMENSIONS::CoordinatesOrder::qr0);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetFloatingBaseCoordinates(const GeneralizedCoordinates_t & big_q ,
                                      BaseCoordinates_t & base_q) {

  base_q = big_q.template segment<kBaseDof>(DIMENSIONS::CoordinatesOrder::qb0);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::BodyStateFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q,
                                     const GeneralizedCoordinates_t & qd, BaseCoordinates_t & trunk_position,
                                     BaseCoordinates_t & trunk_velocity) {

  trunk_position = q.template segment<kBaseDof>(DIMENSIONS::CoordinatesOrder::qb0);
  trunk_velocity = qd.template segment<kBaseDof>(DIMENSIONS::CoordinatesOrder::qb0);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::JointStateFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q ,
                                     const GeneralizedCoordinates_t & qd , JointCoordinates_t & jointposition,
                                     JointCoordinates_t & jointvelocity) {

  jointposition = q.template segment<kTotalJoints>(DIMENSIONS::CoordinatesOrder::qr0);
  jointvelocity = qd.template segment<kTotalJoints>(DIMENSIONS::CoordinatesOrder::qr0);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GeneralizedCoordinatesFromStateVector(const state_vector_t &x ,
                                     GeneralizedCoordinates_t& q, GeneralizedCoordinates_t& qd) {

  q   = x.template segment<kTotalDof>(0);
  qd  = x.template segment<kTotalDof>(kTotalDof);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::StateVectorFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q,
                                         const GeneralizedCoordinates_t & qd ,
                                         state_vector_t & x) {

  x.template segment<kTotalDof>(0)  = q;
  x.template segment<kTotalDof>(kTotalDof) = qd;

}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::ComponentsFromStateVector(const state_vector_t & x ,
                               GeneralizedCoordinates_t & q , GeneralizedCoordinates_t & qd,
                               BaseCoordinates_t  & q_b , BaseCoordinates_t  & qd_b,
                               JointCoordinates_t & q_r , JointCoordinates_t & qd_r) {

  GeneralizedCoordinatesFromStateVector(x,q,qd);

  ComponentsFromGeneralizedCoordinates(q,q_b,q_r);
  ComponentsFromGeneralizedCoordinates(qd,qd_b,qd_r);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::ComponentsFromGeneralizedCoordinates(const GeneralizedCoordinates_t & q ,
                                          BaseCoordinates_t & q_b , JointCoordinates_t & q_r) {
  GetFloatingBaseCoordinates(q,q_b);
  GetActuatedCoordinates(q,q_r);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetWBTransform(const GeneralizedCoordinates_t & big_q , Eigen::Affine3d & A_wb) {

  BaseCoordinates_t qb;
  GetFloatingBaseCoordinates(big_q,qb);
  GetWBTransform(qb, A_wb);
}

template <class DIMENSIONS>
void LRConversions<DIMENSIONS>::GetWBTransform(const BaseCoordinates_t & base_pose,
                                               Eigen::Affine3d & A_wb) {

    // Rotation transformation from base to Inertial frame (Extrinsic ZYX)
    Eigen::Matrix3d rot_I_X_B;

    // Euler angels, RPY rotation
    double roll_angle  = base_pose(DIMENSIONS::CoordinatesOrder::qb0); // roll
    double pitch_angle = base_pose(DIMENSIONS::CoordinatesOrder::qb1); // pitch
    double yaw_angle   = base_pose(DIMENSIONS::CoordinatesOrder::qb2); // yaw

    Eigen::AngleAxisd roll_rotation (roll_angle, uX);
    Eigen::AngleAxisd pitch_rotation(pitch_angle, uY);
    Eigen::AngleAxisd yaw_rotation  (yaw_angle, uZ);

    //Rotation matrix : premultiplication of the rotation
    rot_I_X_B   =   yaw_rotation * pitch_rotation * roll_rotation;

    A_wb.linear() = rot_I_X_B;
    Eigen::Vector3d base_location = base_pose.template segment<3>(DIMENSIONS::CoordinatesOrder::qb3);

    A_wb.translation() = base_location;
}

#endif /* _LRCONVERSIONS_HPP_ */
