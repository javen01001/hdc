/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * LRPhase.hpp
 *
 *  Created on: May 7, 2017
 *      Author: depardo
 */

#ifndef _LRPHASE_HPP_
#define _LRPHASE_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/tools/LeggedRobotTrajectory.hpp>

/**
 * @brief : A LeggedRobotTrajectory with a single c_code
 */
template <class DIMENSIONS>
class LRPhase : public LeggedRobotTrajectory<DIMENSIONS> {


public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
  typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;


  LRPhase(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd,
          const  state_vector_array_t & x, const control_vector_array_t & u , const Eigen::VectorXd & t,
          int c_code):LeggedRobotTrajectory<DIMENSIONS>(lrd,x,u,t,std::vector<int>(x.size(),1),std::vector<int>(x.size(),c_code)),phase_c_code(c_code) { }

  LRPhase(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd,int c_code):LeggedRobotTrajectory<DIMENSIONS>(lrd),phase_c_code(c_code){};

  int phase_c_code = 0;


};
#endif /* _LRPHASE_HPP_ */
