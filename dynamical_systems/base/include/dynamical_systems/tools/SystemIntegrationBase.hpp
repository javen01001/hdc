/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * SystemIntegrationBase.hpp
 *
 *  Created on: Feb 4, 2016
 *      Author: depardo
 */

#ifndef SYSTEMINTEGRATIONBASE_HPP_
#define SYSTEMINTEGRATIONBASE_HPP_

#include <Eigen/Dense>

#include <memory>

#include <boost/numeric/odeint.hpp>
#include <boost/function.hpp>

#include <dynamical_systems/tools/eigenIntegration.h>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/DynamicsBase.hpp>


template<class DIMENSIONS>
class SystemIntegrationBase {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
	typedef typename DIMENSIONS::control_feedback_t control_feedback_t;
	typedef typename DIMENSIONS::control_feedback_array_t control_feedback_array_t;

	typedef std::vector<double> time_vector_array_t;

	typedef boost::numeric::odeint::runge_kutta4< state_vector_t , double, state_vector_t , double,
														boost::numeric::odeint::vector_space_algebra > runge_kutta4_state_stepper_type_t;
public:

	SystemIntegrationBase(std::shared_ptr<DynamicsBase<DIMENSIONS> > dynamics):dynamics_(dynamics)	{	}
	virtual ~SystemIntegrationBase(){};

	void Integrate(state_vector_t & x_initial, double frequency , double total_time , double start_time = 0.0);
	void ClearTrajectories();
	virtual void ClearCustomTrajectories(){};
	virtual void CustomObserver(const state_vector_t &x_observer  , const double t){};

	void ControlLoopWrap(const state_vector_t &x  , state_vector_t & xdot , const double t);
	virtual void GetControlInput(const state_vector_t &x , const double t , control_vector_t & u_total);
	void ObserverWrap(const state_vector_t &x_observer  , const double t);

	void GetSolution(state_vector_array_t & x_solution , control_vector_array_t & u_solution , time_vector_array_t & t_solution);

	std::shared_ptr<DynamicsBase<DIMENSIONS> > dynamics_;

private:

	bool solution_available = false;

	state_vector_array_t solution_state_trajectory;
	control_vector_array_t solution_control_trajectory;
	time_vector_array_t time_trajectory;
	runge_kutta4_state_stepper_type_t state_rk4_stepper;


};

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::Integrate(state_vector_t & x_initial,
                                                  double frequency ,
                                                  double total_time,
                                                  double start_time /* = 0 */) {

	ClearTrajectories();

	double delta_t = 1/frequency;
	double time_range = total_time - start_time;
	int integration_n_steps = static_cast<int>(time_range/delta_t);

	 /*pointers to the system dynamics and the observer*/
		auto system_ptr_ = [this]( const state_vector_t & x,  state_vector_t & xdot, const double t )
			{
				this->ControlLoopWrap( x , xdot , t );
			};

		auto observer_ptr_ = [this](const state_vector_t & x , const double t )
			{
				this->ObserverWrap(x,t);
			};


    /*integrating System Dynamics*/
	boost::numeric::odeint::integrate_n_steps(state_rk4_stepper,
												system_ptr_,
												x_initial,
												start_time,
												delta_t,
												integration_n_steps,
												observer_ptr_);

	solution_available = true;

}

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::ClearTrajectories() {
	solution_state_trajectory.clear();
	solution_control_trajectory.clear();
	time_trajectory.clear();

	ClearCustomTrajectories();

}

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::ControlLoopWrap(const state_vector_t & x,
                                                        state_vector_t & xdot , const double t) {

	control_vector_t u_total = control_vector_t::Zero();

	this->GetControlInput(x,t,u_total);

	xdot = this->dynamics_->systemDynamics(x,u_total);

}

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::GetControlInput(const state_vector_t & x,
                                                        const double t , control_vector_t & u_total) {

	// Default integration is natural dynamics
	u_total.setZero();

}

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::ObserverWrap(const state_vector_t &x_observer,
                                                     const double t) {
	solution_state_trajectory.push_back(x_observer);
	time_trajectory.push_back(t);

	CustomObserver(x_observer, t);
}

template<class DIMENSIONS>
void SystemIntegrationBase<DIMENSIONS>::GetSolution(state_vector_array_t & x_solution,
                                                    control_vector_array_t & u_solution,
                                                    time_vector_array_t & t_solution) {
	if(!solution_available){
		std::cout << "No solution available!" << std::endl;
		return;
	}
	x_solution = solution_state_trajectory;
	t_solution = time_trajectory;

	// compute again all the controls for the observable points (really?!)
	for(int i = 0 ; i < time_trajectory.size() ; i++){
		control_vector_t u;
		this->GetControlInput(solution_state_trajectory[i],time_trajectory[i],u);
		u_solution.push_back(u);
	}

	return;
}

#endif /* SYSTEMINTEGRATIONBASE_HPP_ */
