/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * test_ds_base.cpp
 *
 *  Created on: Oct 27, 2017
 *      Author: depardo
 */
#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/tools/LRConversions.hpp>

namespace {
class test_ds_base : public ::testing::Test {

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:

  test_ds_base() {

    base_.setRandom();
    joints_.setRandom();

    rotation_.col(0) << cos(gamma)*cos(beta), sin(gamma)*cos(beta),-sin(beta);
    rotation_.col(1) << cos(gamma)*sin(beta)*sin(alpha) - sin(gamma)*cos(alpha), sin(gamma)*sin(beta)*sin(alpha) + cos(gamma)*cos(alpha), cos(beta)*sin(alpha);
    rotation_.col(2) << cos(gamma)*sin(beta)*cos(alpha) + sin(gamma)*sin(alpha), sin(gamma)*sin(beta)*cos(alpha) - cos(gamma)*sin(alpha), cos(beta)*cos(alpha);
   }

   virtual ~test_ds_base() {
     // You can do clean-up work that doesn't throw exceptions here.
   }

   // If the constructor and destructor are not enough for setting up
   // and cleaning up each test, you can define the following methods:
   virtual void SetUp() {
       std::srand((unsigned int) time(0));
     }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test cases
   static const size_t STATE_DIM = 24;
   static const size_t CONTROL_DIM = 10;
   static const size_t JOINTS_DIM = 6;
   static const size_t DOF_DIM = 12;
   static const size_t EE = 5;

   // rotation angles (x=alpha, y=beta, z=gamma)
   double alpha = 0.7854;
   double beta  = 1.0472;
   double gamma = 0.5236;

   typedef Dimensions<STATE_DIM,10,6,12,5> testDimensions;
   LRConversions<testDimensions> testConversions;
   testDimensions::BaseCoordinates_t base_;
   testDimensions::JointCoordinates_t joints_;
   testDimensions::GeneralizedCoordinates_t general_;
   testDimensions::state_vector_t x_;

   Eigen::Matrix3d rotation_;

};

size_t const test_ds_base::STATE_DIM;
size_t const test_ds_base::CONTROL_DIM;
size_t const test_ds_base::JOINTS_DIM;
size_t const test_ds_base::DOF_DIM;
size_t const test_ds_base::EE;

//// Tests that the Foo::Bar() method does Abc.

TEST_F(test_ds_base, dimensions) {

  // 1. What component aspect are you testing?: Dimensions of all types

  EXPECT_EQ(STATE_DIM,testDimensions::DimensionsSize::STATE_SIZE);
  EXPECT_EQ(CONTROL_DIM,testDimensions::DimensionsSize::CONTROL_SIZE);
  EXPECT_EQ(JOINTS_DIM,testDimensions::DimensionsSize::JOINTS_SIZE);
  EXPECT_EQ(STATE_DIM+CONTROL_DIM,testDimensions::DimensionsSize::SC_SIZE);
  EXPECT_EQ(DOF_DIM,testDimensions::DimensionsSize::DOF_SIZE);
  EXPECT_EQ(EE,testDimensions::DimensionsSize::EE_SIZE);

  // Index of Coordinates
  EXPECT_EQ(0,testDimensions::CoordinatesOrder::qb0);
  EXPECT_EQ(6,testDimensions::CoordinatesOrder::qr0);

  // Eigen::Vectors type size
  EXPECT_EQ(STATE_DIM,testDimensions::state_vector_t::SizeAtCompileTime);
  EXPECT_EQ(CONTROL_DIM,testDimensions::control_vector_t::SizeAtCompileTime);
  EXPECT_EQ(DOF_DIM,testDimensions::InertiaMatrix_t::RowsAtCompileTime);
  EXPECT_EQ(DOF_DIM,testDimensions::InertiaMatrix_t::ColsAtCompileTime);
  EXPECT_EQ(DOF_DIM,testDimensions::GeneralizedCoordinates_t::SizeAtCompileTime);
  EXPECT_EQ(6,testDimensions::BaseCoordinates_t::SizeAtCompileTime);
  EXPECT_EQ(JOINTS_DIM,testDimensions::JointCoordinates_t::SizeAtCompileTime);
  EXPECT_EQ(CONTROL_DIM,testDimensions::TorqueCommand_t::SizeAtCompileTime);
  EXPECT_EQ(3,testDimensions::EEPosition_t::SizeAtCompileTime);
  EXPECT_EQ(3,testDimensions::EEVelocity_t::SizeAtCompileTime);
  EXPECT_EQ(3,testDimensions::EEForce_t::SizeAtCompileTime);
  EXPECT_EQ(2,testDimensions::ZMPVector_t::SizeAtCompileTime);
  EXPECT_EQ(6,testDimensions::Vector6D_t::SizeAtCompileTime);
  EXPECT_EQ(3*EE,testDimensions::EELambda_t::SizeAtCompileTime);
  EXPECT_EQ(EE,testDimensions::EELambda_single_component_t::SizeAtCompileTime);
  EXPECT_EQ(EE,testDimensions::EEFriction_cone_coefficient_t::SizeAtCompileTime);
  EXPECT_EQ(3*6,testDimensions::SingleEEBaseJacobian_t::SizeAtCompileTime);
  EXPECT_EQ(3*EE,testDimensions::EEJacobian_t::RowsAtCompileTime);
  EXPECT_EQ(DOF_DIM,testDimensions::EEJacobian_t::ColsAtCompileTime);
  EXPECT_EQ(DOF_DIM,testDimensions::EEJacobianTranspose_t::RowsAtCompileTime);
  EXPECT_EQ(3*EE,testDimensions::EEJacobianTranspose_t::ColsAtCompileTime);
  EXPECT_EQ(JOINTS_DIM*JOINTS_DIM,testDimensions::JointMatrix_t::SizeAtCompileTime);
  EXPECT_EQ(6,testDimensions::BaseJointMatrix_t::RowsAtCompileTime);
  EXPECT_EQ(JOINTS_DIM,testDimensions::BaseJointMatrix_t::ColsAtCompileTime);

  EXPECT_EQ(std::pow(2,EE),testDimensions::get_all_contact_configurations().size());
  EXPECT_EQ(std::pow(2,EE),testDimensions::kTotalConfigurations);

  EXPECT_EQ(JOINTS_DIM,testDimensions::kTotalJoints);
  EXPECT_EQ(6,testDimensions::kBaseDof);
  EXPECT_EQ(3,testDimensions::kSingleEEContactForceSize);

  EXPECT_EQ(EE,testDimensions::kTotalEE);
  EXPECT_EQ(DOF_DIM,testDimensions::kTotalDof);
  EXPECT_EQ(CONTROL_DIM,testDimensions::kTotalControls);
  EXPECT_EQ(STATE_DIM,testDimensions::kTotalStates);
  EXPECT_EQ(3*EE,testDimensions::kContactForcesSize);
  EXPECT_DOUBLE_EQ(9.8,testDimensions::kGravity);

  //ToDo: How to check EEDataMaps?
  //ToDo: kEEJoints should not be defined here
  //ToDo: SingleEEJointJacobian_t inherits the same problem from kEEJoints
  //ToDo: SingleEEJacobian_t inherits the same problem of kEEJoints
  //ToDo: kGravity
}

TEST_F(test_ds_base, conversions) {

  testDimensions::state_vector_t local_x;
  testDimensions::GeneralizedCoordinates_t local_general;
  testDimensions::JointCoordinates_t local_joints;
  testDimensions::BaseCoordinates_t local_base;
  testDimensions::GeneralizedCoordinates_t generalv;
  testDimensions::BaseCoordinates_t basev;
  testDimensions::JointCoordinates_t jointsv;
  Eigen::Affine3d A_wb;
  testDimensions::EEPositionsDataMap_t p_base,p_inertia;
  testDimensions::EEJacobianTranspose_t A;
  testDimensions::EEJacobian_t B;
  testDimensions::InertiaMatrix_t Q;

  //Testing static constants
  {
    EXPECT_EQ(6,testConversions.kBaseDof);
    EXPECT_EQ(DOF_DIM,testConversions.kTotalDof);
    EXPECT_EQ(EE,testConversions.kTotalEE);
    EXPECT_EQ(JOINTS_DIM,testConversions.kTotalJoints);
    EXPECT_EQ(CONTROL_DIM,testConversions.kTotalControls);
    EXPECT_EQ(STATE_DIM,testConversions.kTotalStates);
    EXPECT_EQ(EE*3,testConversions.kContactForcesSize);
    EXPECT_DOUBLE_EQ(9.8,testConversions.kGravity);
    EXPECT_EQ(3,testConversions.kSingleEEContactForceSize);
  }

  //Testing: GeneralizedCoordinatesFromComponents
  testConversions.GeneralizedCoordinatesFromComponents(base_,joints_,general_);
  EXPECT_TRUE(general_.segment<6>(0).isApprox(base_));
  EXPECT_TRUE(general_.segment<JOINTS_DIM>(6).isApprox(joints_));

  //Testing: GetActuatedCoordinates
  testConversions.GetActuatedCoordinates(general_,local_joints);
  EXPECT_TRUE(local_joints.isApprox(joints_));

  //Testing: GetFloatingBaseCoordinates
  testConversions.GetFloatingBaseCoordinates(general_,local_base);
  EXPECT_TRUE(local_base.isApprox(base_));

  //Testing: BodyStateFromGeneralizedCoordinates
  generalv = general_*2;
  testConversions.BodyStateFromGeneralizedCoordinates(general_,generalv,local_base,basev);
  EXPECT_TRUE(general_.segment<6>(0).isApprox(local_base));
  EXPECT_TRUE(generalv.segment<6>(0).isApprox(basev));

  //Testing: JointStateFromGeneralizedCoordinates
  testConversions.JointStateFromGeneralizedCoordinates(general_,generalv,local_joints,jointsv);
  EXPECT_TRUE(general_.segment<JOINTS_DIM>(6).isApprox(local_joints));
  EXPECT_TRUE(generalv.segment<JOINTS_DIM>(6).isApprox(jointsv));

  //Testing: StateVectorFromGeneralizedCoordinates
  testConversions.StateVectorFromGeneralizedCoordinates(general_,generalv,x_);
  Eigen::VectorXd t(STATE_DIM); t << general_,generalv;
  EXPECT_TRUE(x_.isApprox(t)) << x_;

  //Testing: GeneralizedCoordinatesFromStateVector
  generalv.setZero();local_general.setZero();
  testConversions.GeneralizedCoordinatesFromStateVector(x_,local_general,generalv);
  EXPECT_TRUE(local_general.isApprox(general_));
  EXPECT_TRUE(generalv.isApprox(2*general_));

  //Testing: ComponentsFromStateVector
  generalv.setZero();local_general.setZero();local_base.setZero();
  local_joints.setZero();basev.setZero();jointsv.setZero();
  testConversions.ComponentsFromStateVector(x_,local_general,generalv,local_base,basev,local_joints,jointsv);
  EXPECT_TRUE(local_general.isApprox(general_));
  EXPECT_TRUE(generalv.isApprox(2*general_));
  EXPECT_TRUE(local_base.isApprox(base_));
  EXPECT_TRUE(basev.isApprox(2*base_));
  EXPECT_TRUE(local_joints.isApprox(joints_));
  EXPECT_TRUE(jointsv.isApprox(2*joints_));

  //Testing: ComponentsFromGeneralizedCoordinates
  local_joints.setZero();local_base.setZero();
  testConversions.ComponentsFromGeneralizedCoordinates(general_,local_base,local_joints);
  EXPECT_TRUE(local_base.isApprox(base_));
  EXPECT_TRUE(local_joints.isApprox(joints_));

  //Testing: GetWBTransform
  double x = 1.68, y = 7.53, z = 1.59;
  local_base << alpha,beta,gamma,x,y,z;
  Eigen::Vector3d p0,p1,p2,p3,p4,p5; p0 << x,y,z;
  testConversions.GeneralizedCoordinatesFromComponents(local_base,local_joints,local_general);
  testConversions.GetWBTransform(local_general,A_wb);
  EXPECT_TRUE(A_wb.rotation().isApprox(rotation_));
  EXPECT_TRUE(A_wb.translation().isApprox(p0));

  //Testing: GetFeetPoseInertia
  p1 << 1,0,0; p_base[0] = p1;
  p2 << 0,1,0; p_base[1] = p2;
  p3 << 0,0,1; p_base[2] = p3;
  p4 << 1,0,1; p_base[3] = p4;
  p5 << 1,1,0; p_base[4] = p5;
  Eigen::Vector4d p1_,p2_,p3_,p4_,p5_;
  testConversions.GetFeetPoseInertia(A_wb,p_base,p_inertia);
  EXPECT_TRUE(p_inertia[0].isApprox(rotation_*p1 + p0));
  EXPECT_TRUE(p_inertia[1].isApprox(rotation_*p2 + p0));
  EXPECT_TRUE(p_inertia[2].isApprox(rotation_*p3 + p0));
  EXPECT_TRUE(p_inertia[3].isApprox(rotation_*p4 + p0));
  EXPECT_TRUE(p_inertia[4].isApprox(rotation_*p5 + p0));

  //Testing: GetSkewMatrix
  {
    Eigen::Matrix3d skm; skm <<  0,-z, y,
                                 z, 0,-x,
                                -y, x, 0;
    Eigen::Matrix3d skew_matrix;
    testConversions.GetSkewMatrix(p0,skew_matrix);
    EXPECT_TRUE(skm.isApprox(skew_matrix));
  }

  //Testing: GetRPYanges (Rotation Matrix)
  {
    p1.setZero(); p2 << alpha,beta,gamma;
    testConversions.GetRPYangles(rotation_,p1);
    EXPECT_TRUE(p2.isApprox(p1));
  }

  //Testing: QRDecomposition
  {
    A.setRandom();Eigen::MatrixXd R,P;int rank;
    P.resize(A.cols(),A.cols());
    testConversions.QRDecomposition(A,Q,R,P,rank);
    EXPECT_TRUE(Q.isUnitary());
    Eigen::MatrixXd T1 = A*P;
    Eigen::MatrixXd T2 = Q*R;
    EXPECT_TRUE((T1).isApprox(T2));
  }

  //Testing: svDecomp (m < n)
  {
    A.setRandom();
    testDimensions::EEJacobianTranspose_t S;
    Eigen::Matrix<double,testDimensions::kTotalDof,testDimensions::kTotalDof> U;
    Eigen::Matrix<double,testDimensions::kContactForcesSize,testDimensions::kContactForcesSize> V;
    testConversions.svDecomp(A,U,S,V);
    Eigen::MatrixXd SVD_TEST= U*S*V.transpose();
    EXPECT_TRUE(A.isApprox(SVD_TEST)) << "A: " << std::endl << A << std::endl << "U:" << std::endl << U << std::endl << "V: " << std::endl << V << std::endl << "S:" << std::endl << S <<std::endl;
    EXPECT_TRUE(U.isUnitary());
    EXPECT_TRUE(V.isUnitary());
  }

  //Testing: svDecomp (n < m)
  {
    B.setRandom();
    testDimensions::EEJacobian_t S1;
    Eigen::Matrix<double,testDimensions::kTotalDof,testDimensions::kTotalDof> V1;
    Eigen::Matrix<double,testDimensions::kContactForcesSize,testDimensions::kContactForcesSize> U1;
    testConversions.svDecomp(B,U1,S1,V1);
    Eigen::MatrixXd SVD_TEST1= U1*S1*V1.transpose();
    EXPECT_TRUE(B.isApprox(SVD_TEST1)) << "B: " << std::endl << B << std::endl << "U1:" << std::endl << U1 << std::endl
                                       << "V1: " << std::endl << V1 << std::endl << "S1:" << std::endl << S1 <<std::endl;
    EXPECT_TRUE(U1.isUnitary());
    EXPECT_TRUE(V1.isUnitary());
  }

  //Testing: svDecomp (using dynamic matrices)
  {
    Eigen::MatrixXd B_(19,5),U_,S_,V_;
    B_.setRandom();
    testConversions.svDecomp(B_,U_,S_,V_);
    EXPECT_TRUE(B_.isApprox(U_*S_*V_.transpose())) << "B_: " << std::endl << B_ << std::endl << "U_:" << std::endl << U_ << std::endl
        << "V_: " << std::endl << V_ << std::endl << "S_:" << std::endl << S_ <<std::endl;
    EXPECT_TRUE(U_.isUnitary());
    EXPECT_TRUE(V_.isUnitary());
  }

  //Testing: psdInv (m>n) LEFT PSEUDOINVERSE ONLY
  {
    B.setRandom();
    Eigen::MatrixXd out = testConversions.psdInv(B,1e-9);
    Eigen::MatrixXd B_L = out*B;
    Eigen::MatrixXd B_R = B*out;
    EXPECT_FALSE(B_R.isIdentity(1e-4)) << "right: " << B*out << std::endl;
    EXPECT_TRUE(B_L.isIdentity(1e-4)) << "left: " << out*B << std::endl;
  }

  //Testing: psdInv (m<n) RIGHT PSEUDOINVERSE ONLY
  {
    A.setRandom();
    Eigen::MatrixXd out = testConversions.psdInv(A,1e-9);
    Eigen::MatrixXd A_L = out*A;
    Eigen::MatrixXd A_R = A*out;
    EXPECT_TRUE(A_R.isIdentity(1e-4)) << "right: " << A*out << std::endl;
    EXPECT_FALSE(A_L.isIdentity(1e-5)) << "left: " << out*A << std::endl;
  }

  //Testing: psdInv (m<n) and getting the SUV
  {
    Eigen::MatrixXd A_(12,18),U_,S_,V_;
    Eigen::MatrixXd out = testConversions.psdInv(A_,1e-9,U_,S_,V_);
    Eigen::MatrixXd A_R = A_*out;
    Eigen::MatrixXd A_L = out*A_;
    EXPECT_TRUE(A_R.isIdentity(1e-4)) << "right: " << A_*out << std::endl;
    EXPECT_FALSE(A_L.isIdentity(1e-4)) << "left: " << out*A_ << std::endl;
    EXPECT_TRUE(A_.isApprox(U_*S_*V_.transpose()));
    EXPECT_TRUE(U_.isUnitary());
    EXPECT_TRUE(V_.isUnitary());
  }
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
