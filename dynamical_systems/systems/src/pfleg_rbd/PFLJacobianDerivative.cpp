/*
 * PFLJacobianDerivative.cpp
 *
 *  Created on: August 7, 2017
 *      Author: depardo
 */

#include<dynamical_systems/systems/pointFootLeg/PFLJacobianDerivative.hpp>

PFLJacobianDerivative::PFLJacobianDerivative():PFLKinematics(),
    fc_(true), //assuming ALL EE are in contact
		numDiff(functor,std::sqrt(std::numeric_limits<double>::epsilon())) {

	functor.changefc(fc_);
	dJacobian.setZero();
	Jdot.setZero();
}

void PFLJacobianDerivative::getDerivative(const GeneralizedCoordinates_t & big_q,
                                          const GeneralizedCoordinates_t & qd,
                                          EEJacobian_t & dJdq) {
	numDiff.df(big_q,dJacobian);
	Jdot = dJacobian * qd;

	// This is only to change the shape of the result [3*9 x 1] -> [3 x 9]
	dJdq = Eigen::Map<EEJacobian_t>(Jdot.data(), this->kContactForcesSize, this->kTotalDof);
}
