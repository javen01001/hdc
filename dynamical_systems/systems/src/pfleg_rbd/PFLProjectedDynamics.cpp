/*! \file PFLProjectedDynamics.cpp
 *  \authors   depardo
 *  \brief Implementaion of the PFLProjectedDynamics class
 */

#include <dynamical_systems/systems/pointFootLeg/PFLProjectedDynamics.hpp>
#include <dynamical_systems/base/extras/GRFBase.hpp>


PFLProjectedDynamics::PFLProjectedDynamics(const ContactConfiguration_t & feet_contact):
                      LeggedRobotDynamics(feet_contact,
                                          std::shared_ptr<GRFBase<robotDimensions>>(new GRFBase<robotDimensions>(feet_contact))),
                      pfl_JacobianDerivative_(new PFLJacobianDerivative()),
                      fplu(),
                      pfl_state(feet_contact),
                      inertia_properties(),
                      base_forceTransforms(),
                      base_motionTransforms(),
											inertia_matrix(inertia_properties, base_forceTransforms),
											inverse_dynamics(inertia_properties, base_motionTransforms),
                      pfl_kin() {

	//Torque selection matrix
	torque_slection_matrix_S.block<kTotalJoints,kBaseDof>(0,0).setZero();
	torque_slection_matrix_S.block<kTotalJoints,kTotalJoints>(0,kBaseDof) = JointMatrix_t::Identity();
}

void PFLProjectedDynamics::setFeetConfiguration(const ContactConfiguration_t & fc ) {

  pfl_JacobianDerivative_->changecontactconfiguration(fc);
  pfl_state.changecontactconfiguration(fc);
}

robotDimensions::GeneralizedCoordinates_t PFLProjectedDynamics::systemCDynamics(const control_vector_t & tau,
		GeneralizedCoordinates_t & tau_minus_h_minus_Mqdd) {

  M_ = inertia_matrix.update(pfl_state.joints_pos);

  inverse_dynamics.setJointStatus(pfl_state.joints_pos);
	GetCandGfromDynamics(h_);

	getOrthogonalProjection(P_,Jinv_);

	pfl_JacobianDerivative_->getDerivative(pfl_state.big_q,pfl_state.q_derivative,_Jdot);

	PM_.noalias() = P_ * M_;
	PMT_ = PM_.transpose();

	Mtilda_ = M_ + PM_ - PMT_;

	tau_minus_h_ = torque_slection_matrix_S.transpose()*tau - h_;
	b_ = -M_ * (Jinv_*_Jdot) * pfl_state.big_qdot + (P_ * tau_minus_h_);

	qdd_ = Mtilda_.fullPivLu().solve(b_);

	//remove the variable from this method
	tau_minus_h_minus_Mqdd = -tau_minus_h_ + (M_*qdd_);
	_tau_minus_h_minus_Mqdd = tau_minus_h_minus_Mqdd;

	return(qdd_);
}

// Change the name of this method
void PFLProjectedDynamics::GetContactForces(const control_vector_t & u ) {

  GeneralizedCoordinates_t tau_minus_h_minus_Mqdd;

	if(!grf_->isValid()) {
	    grf_->setContactForces(EELambda_t::Zero());
	    return;
	}
  systemCDynamics(u,tau_minus_h_minus_Mqdd);
	UpdateGRF(tau_minus_h_minus_Mqdd);
}

void PFLProjectedDynamics::UpdateGRF(const GeneralizedCoordinates_t & tau_minus_h_minus_Mqdd) {

  EELambda_t lambda_w, lambda_b;
  lambda_w.setZero();
  lambda_b.setZero();

	if(grf_->isValid()){

      //Dangerously using a 'global' variable
      lambda_b = Jinv_.transpose()*tau_minus_h_minus_Mqdd;

		int current_index = 0;
		for(int i= 0 ; i<kTotalEE ; i++) {
			if(pfl_state.getFeetConfiguration()[i]) {
				lambda_w.segment<3>(3*i) = pfl_state.W_X_B.linear()*lambda_b.segment<3>(current_index*3);
				current_index++;
			} else {
				lambda_w.segment<3>(3*i) = Eigen::Vector3d::Zero();
				current_index++;
			}
		}
	} else {
		lambda_w.setZero();
	}

	grf_->setContactForces(lambda_w);
}

void PFLProjectedDynamics::GeneralizedContactForces(const PFLState &h_s, GeneralizedCoordinates_t & F ) {
	//ToDo
	F.setZero();
}

void PFLProjectedDynamics::positionStateDerivative(const state_vector_t &x, GeneralizedCoordinates_t & q_derivative) {

	GeneralizedCoordinates_t q, qd;
	BaseCoordinates_t base_velocities_baseFrame;
	BaseCoordinates_t base_velocities_worldFrame;
	JointCoordinates_t qrd;

	pfl_kin.GeneralizedCoordinatesFromStateVector(x,q,qd);
	pfl_kin.GetFloatingBaseCoordinates(qd,base_velocities_baseFrame);
	pfl_kin.GetActuatedCoordinates(qd,qrd);

	Eigen::Affine3d Awb;
	pfl_kin.GetWBTransform(q,Awb);

	// good point!
	pfl_kin.WorldXbase_motionTransformation(Awb,base_velocities_baseFrame,base_velocities_worldFrame);

	q_derivative.segment<kBaseDof>(0) = base_velocities_worldFrame;
	q_derivative.segment<kTotalJoints>(kBaseDof) = qrd;
}

void PFLProjectedDynamics::getOrthogonalProjection(InertiaMatrix_t &P, EEJacobianTranspose_t &Jcinv) {

  Eigen::Matrix<double,kTotalDof,kTotalDof> V;
  Eigen::Matrix<double,3,3> U;
  EEJacobianTranspose_t S;
  S.setZero();

  int k = pfl_state.GetSize();

  //ToDo: Check what happens with the JacobianContainer when
  if(k==0){
      P.setIdentity();
      Jcinv.setIdentity();
  } else {
    pfl_state.GetInverse(Jcinv,1e-1,U,V);

    // The size of V2 depends on the contact configuration
    // Is the SVD already ordered?
    Eigen::MatrixXd V2 =  V.block(0,k,kTotalDof,kTotalDof-k);
    P = V2*V2.transpose();
  }
}

bool PFLProjectedDynamics::VerifyProjection(const Eigen::MatrixXd & P) {

	/* fake function please fix or delete */

	Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(P);
	std::cout << "Never entering here The rank of P is :  " << lu_decomp.rank() << std::endl;
	return(true);
}

robotDimensions::state_vector_t PFLProjectedDynamics::systemDynamics(const state_vector_t & x , const control_vector_t & u) {

	pfl_state.updateState(x);

	state_vector_t xd;
	GeneralizedCoordinates_t qdd;

	qdd = systemCDynamics(u,_tau_minus_h_minus_Mqdd);

	/* set the derivatives of the state*/
	pfl_kin.StateVectorFromGeneralizedCoordinates(this->pfl_state.q_derivative,qdd,xd);

	return(xd);
}

void PFLProjectedDynamics::GetConstrainedUnconstrainedQ(const Eigen::MatrixXd & Q, Eigen::MatrixXd & Qc,
		Eigen::MatrixXd & Qu, const int & rank) {

	Qc = Q.block(0,0,kTotalDof,rank);
	Qu = Q.block(0,rank,kTotalDof,kTotalDof-rank);
}

void PFLProjectedDynamics::getMfromDynamics(iit::pfLeg::dyn::JSIM::MatrixType & bigM) {

  bigM = inertia_matrix.update(pfl_state.joints_pos);
  return;
}

void PFLProjectedDynamics::getInertiaMatrix(const GeneralizedCoordinates_t & big_q, InertiaMatrix_t &M) {

	M = inertia_matrix.update(big_q.segment<kTotalJoints>(kBaseDof));
	return;
}

// ToDo: Fix this, the other methods are using the current state of the robot
void PFLProjectedDynamics::GetFHfromDynamics(const GeneralizedCoordinates_t & big_q ,
                                             BaseJointMatrix_t & F,
                                             JointMatrix_t & H) {

	iit::pfLeg::JointState qr;
	pfl_kin.GetActuatedCoordinates(big_q, qr);

	inertia_matrix.update(qr);

	// Using Featherstone notation
	F = inertia_matrix.getF();
	H = inertia_matrix.getFixedBaseBlock();
}

//ToDo : Fix -> please update the inverse_dynamics before using this method
void PFLProjectedDynamics::GetCandGfromDynamics(GeneralizedCoordinates_t & h_vector) {

	iit::rbd::ForceVector base_wrench_c;
	iit::pfLeg::JointState joint_torques_c;

	iit::rbd::ForceVector base_wrench_g;
	iit::pfLeg::JointState joint_torques_g;

	iit::rbd::VelocityVector acceleration_g_baseFrame(iit::rbd::VelocityVector::Zero());

	inverse_dynamics.C_terms_fully_actuated(base_wrench_c , joint_torques_c , pfl_state.base_velocity_base, pfl_state.joints_vel);

	acceleration_g_baseFrame.segment<3>(iit::rbd::LX) = pfl_state.gravity_base_;

	inverse_dynamics.G_terms_fully_actuated(base_wrench_g , joint_torques_g , acceleration_g_baseFrame);

	h_vector.segment<kBaseDof>(qb0) = base_wrench_c + base_wrench_g;
	h_vector.segment<kTotalJoints>(qr0) = joint_torques_c + joint_torques_g;
}

//ToDo : Fix -> use only assuming the state has been updated
void PFLProjectedDynamics::InverseDynamicsControl(const GeneralizedCoordinates_t & qdd_desired,
                                                  TorqueCommand_t & inverse_dynamics_u) {

  InertiaMatrix_t M,P;
  EEJacobianTranspose_t H;
  GeneralizedCoordinates_t h_vector;

	M = inertia_matrix.update(pfl_state.joints_pos);
	getOrthogonalProjection(P,H);

	inverse_dynamics.setJointStatus(pfl_state.joints_pos);
	GetCandGfromDynamics(h_vector);

  Eigen::MatrixXd PSt = P*torque_slection_matrix_S.transpose();
  Eigen::MatrixXd Pstinv = pfl_kin.psdInv(PSt,1e-9);
	Eigen::MatrixXd PStP = Pstinv*P;

	GeneralizedCoordinates_t Mqdd = M*qdd_desired;

	inverse_dynamics_u = PStP * (Mqdd + h_vector);
}
