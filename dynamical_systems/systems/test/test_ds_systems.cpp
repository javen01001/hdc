/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * test_ds_systems.cpp
 *
 *  Created on: Oct 31, 2017
 *      Author: depardo
 */
#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLJacobianDerivative.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLExtendedJacobians.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLConstraintJacobian.hpp>
namespace {
class test_ds_systems : public ::testing::Test {

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using Kinematics = PFLKinematics;
  using ExtendedJacobians  = PFLExtendedJacobians;
  using JacobianDerivative = PFLJacobianDerivative;
  using ConstraintJacobian = PFLConstraintJacobian;

protected:

   test_ds_systems() {
     fc_ = true;
     testJderiv = std::shared_ptr<JacobianDerivative>(new JacobianDerivative());
   }

   virtual ~test_ds_systems() {}

   virtual void SetUp() {
     //changing the seed of the random generator
     std::srand((unsigned int) time(0));
   }

   virtual void TearDown() {
   }

   // Robot Kinematics
   Kinematics testK;
   std::shared_ptr<JacobianDerivative> testJderiv;
   ExtendedJacobians testExtendedJacobians;

   robotDimensions::ContactConfiguration_t fc_;
   robotDimensions::BaseCoordinates_t base_;
   robotDimensions::JointCoordinates_t joints_;
   robotDimensions::GeneralizedCoordinates_t general_;
   robotDimensions::state_vector_t x_;
   robotDimensions::EEPositionsDataMap_t ee_position_datamap_;

};

//// Tests that the Foo::Bar() method does Abc.
TEST_F(test_ds_systems, kinematics) {

  //Testing: constants and sizes
  {
    EXPECT_EQ(6,testK.kBaseDof);
    EXPECT_EQ(9,testK.kTotalDof);
    EXPECT_EQ(1,testK.kTotalEE);
    EXPECT_EQ(3,testK.kEEJoints);
    EXPECT_EQ(3,testK.kTotalJoints);
  }

  //Testing: GetCanonicalPoseState
  {
    testK.GetCanonicalPoseState(x_);
    general_.setZero();
    EXPECT_TRUE(x_.segment<testK.kTotalDof>(testK.kTotalDof).isApprox(general_));
  }

  //Testing: GetFeetPoseBaseFromJoints
  {
    EXPECT_EQ(testK.kTotalEE,ee_position_datamap_.size());
  }
}

TEST_F(test_ds_systems, JacboianDerivative) {

  //Testing: constants and sizes
  {
    EXPECT_EQ(6,testJderiv->kBaseDof);
    EXPECT_EQ(9,testJderiv->kTotalDof);
    EXPECT_EQ(1,testJderiv->kTotalEE);
    EXPECT_EQ(3,testJderiv->kEEJoints);
    EXPECT_EQ(3,testJderiv->kTotalJoints);
    EXPECT_EQ(3,testJderiv->kContactForcesSize);
    EXPECT_EQ(3*9,testJderiv->Jdot.size());
    EXPECT_EQ(27*9,testJderiv->dJacobian.size());
    EXPECT_EQ(9,testJderiv->functor.m_inputs);
    EXPECT_EQ(9*3,testJderiv->functor.m_values);
  }

  //Testing: change feet configuration
  {
    EXPECT_TRUE(testJderiv->fc_[0]);
    fc_ = false;
    testJderiv->changecontactconfiguration(fc_);
    EXPECT_FALSE(testJderiv->fc_[0]);
  }

  //ToDo: Think how to test the actual value of the derivative of the Jacobian
}

TEST_F(test_ds_systems,ExtendedJacobian) {

  //Testing: constants and sizes
  {
    EXPECT_EQ(6,testK.kBaseDof);
    EXPECT_EQ(9,testK.kTotalDof);
    EXPECT_EQ(1,testK.kTotalEE);
    EXPECT_EQ(3,testK.kEEJoints);
    EXPECT_EQ(3,testK.kTotalJoints);
    EXPECT_EQ(3,testExtendedJacobians.Jb_LF.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jb_RF.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jb_LH.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jb_RH.rows());
    EXPECT_EQ(6,testExtendedJacobians.Jb_LF.cols());
    //EXPECT_EQ(6,testExtendedJacobians.Jb_RF.cols());
    //EXPECT_EQ(6,testExtendedJacobians.Jb_LH.cols());
    //EXPECT_EQ(6,testExtendedJacobians.Jb_RH.cols());
    EXPECT_EQ(3,testExtendedJacobians.Jr_LF.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_RF.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_LH.rows());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_RH.rows());
    EXPECT_EQ(3,testExtendedJacobians.Jr_LF.cols());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_RF.cols());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_LH.cols());
    //EXPECT_EQ(3,testExtendedJacobians.Jr_RH.cols());
  }

  //Testing: second block of (ALL) Jb_XX must be identity
  {
    Eigen::MatrixXd t = testExtendedJacobians.Jb_LF.block<3,3>(0,3);
    EXPECT_TRUE(t.isIdentity(1e-5));
  }

  //Testing: interfaces for extended jacobinans
  {
    general_.setRandom();
    testExtendedJacobians.GetActuatedCoordinates(general_,joints_);
    testK.GetFeetPoseBase(general_,ee_position_datamap_);
    Eigen::MatrixXd Jb_t, Jr_t;

    //First using GeneralizedCoordinates_t
    testExtendedJacobians.updateJacobians(general_);
    Jb_t=testExtendedJacobians.Jb_LF;
    Jr_t=testExtendedJacobians.Jr_LF;

    //Then using ee_position and joints (They must correspond!)
    testExtendedJacobians.updateJacobiansQuick(joints_,ee_position_datamap_);
    EXPECT_TRUE(testExtendedJacobians.Jb_LF.isApprox(Jb_t));
    EXPECT_TRUE(testExtendedJacobians.Jr_LF.isApprox(Jr_t));

    //Now using only joints
    general_.setRandom();
    testExtendedJacobians.GetActuatedCoordinates(general_,joints_);

    //Now using only the joints
    testExtendedJacobians.updateJacobians(joints_);
    EXPECT_FALSE(testExtendedJacobians.Jb_LF.isApprox(Jb_t));
    EXPECT_FALSE(testExtendedJacobians.Jr_LF.isApprox(Jr_t));
    Jb_t=testExtendedJacobians.Jb_LF;
    Jr_t=testExtendedJacobians.Jr_LF;
    EXPECT_TRUE(testExtendedJacobians.Jb_LF.isApprox(Jb_t));
    EXPECT_TRUE(testExtendedJacobians.Jr_LF.isApprox(Jr_t));
  }
}

TEST_F(test_ds_systems,ConstraintJacobian) {

  //Testing: the constructor
  {
    robotDimensions::ContactConfiguration_t ee1(true);
    ConstraintJacobian testConstraintJacobian1(ee1);
    EXPECT_EQ(3,testConstraintJacobian1.GetSize());

    robotDimensions::ContactConfiguration_t ee2(false);
    ConstraintJacobian testConstraintJacobian2(ee2);
    EXPECT_EQ(0,testConstraintJacobian2.GetSize());
  }

  //Testing: the inverse
  {
    ConstraintJacobian testConstraintJacobian(true);
    general_.setRandom();
    testConstraintJacobian.update(general_);
    robotDimensions::EEJacobianTranspose_t Jinv;
    testConstraintJacobian.GetInverse(Jinv,1e-1);
    Eigen::MatrixXd JT = testConstraintJacobian.GetJc()*Jinv;
    Eigen::MatrixXd JTF = Jinv*testConstraintJacobian.GetJc();
    EXPECT_TRUE(JT.isIdentity(1e-5));
    EXPECT_FALSE(JTF.isIdentity(1e-5));
  }

  //Testing: the projector
  {

  }

}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
