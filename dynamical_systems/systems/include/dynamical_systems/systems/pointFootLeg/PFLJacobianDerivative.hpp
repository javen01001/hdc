/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * PFLJacobianDerivative.hpp
 *
 *  Created on: 07.08.2017
 *      Author: depardo
 */

#ifndef PFLJACOBIANDERIVATIVES_HPP_
#define PFLJACOBIANDERIVATIVES_HPP_

#include <memory>
#include <Eigen/Dense>
#include <unsupported/Eigen/NumericalDiff>
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLConstraintJacobian.hpp>

/**
 * @brief Class to compute the time derivative of the EE Jacobian. It uses the
 * following chain rule:
 * \dot{J} = dJ/dq x \dot{q}
 * Note that the user provides the \dot{q} to compute \dot{J} and there is no
 * verification of the consistency of this vector
 */
class PFLJacobianDerivative : public PFLKinematics {

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef robotDimensions::ContactConfiguration_t ContactConfiguration_t;
  typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef robotDimensions::EEJacobian_t EEJacobian_t;

  /**
   * The constructor. It is assumed that all the EE of the robot are in contact
   * user should use 'changecontactconfiguration' after construction for set
   * the correc contact configuration
   */
	PFLJacobianDerivative();
	virtual ~PFLJacobianDerivative(){}

	/**
	 * Computes the Jacobian time derivative
	 * @param q The configuration of the robot
	 * @param qd The velocities of the robot
	 * @param djdq The resultant Jacobian derivative
	 */
	void getDerivative(const GeneralizedCoordinates_t & q,
	                   const GeneralizedCoordinates_t & qd,
	                   EEJacobian_t & djdq);

	/**
	 * Changes the ee contact configuration
	 * @param fc
	 */
	void changecontactconfiguration(const ContactConfiguration_t & fc) {
	  fc_ = fc;
	  this->functor.changefc(fc);
	}

	template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
	struct BaseFunctorNumDiff {

	  public :
	    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	    typedef _Scalar Scalar;
			enum {
				InputsAtCompileTime = NX,
				ValuesAtCompileTime = NY
			};
			typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
			typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
			typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,InputsAtCompileTime> JacobianType;

			int m_inputs, m_values;

			BaseFunctorNumDiff() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
			BaseFunctorNumDiff(int inputs, int values) : m_inputs(inputs), m_values(values) {}

			int inputs() const { return m_inputs; }
			int values() const { return m_values; }

			virtual ~BaseFunctorNumDiff(){}
	};

  struct jacobianFunctor: BaseFunctorNumDiff<double,kTotalDof,kTotalDof*kContactForcesSize> {

    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      jacobianFunctor():BaseFunctorNumDiff<double,kTotalDof,kTotalDof*kContactForcesSize>(),
          my_constraintJacobian(new PFLConstraintJacobian(true))
      {}
      virtual ~jacobianFunctor(){}

      int operator()(const InputType &in_vect, ValueType &fvec) const {
        my_constraintJacobian->update(in_vect);
        fvec = Eigen::Map<Eigen::Matrix<double,kTotalDof*kContactForcesSize,1>>(my_constraintJacobian->GetJc().data(),kTotalDof*kContactForcesSize);
        return 0;
      }

      void changefc(const ContactConfiguration_t & fc) {
        my_constraintJacobian->ChangeFeetConfiguration(fc);
      }

      //cannot be an unique_ptr as this struct is copyed into the NumDiff class
      std::shared_ptr<PFLConstraintJacobian> my_constraintJacobian;
  };

  Eigen::Matrix<double, kTotalDof*kContactForcesSize,1> Jdot;
  Eigen::Matrix<double, kTotalDof*kContactForcesSize,kTotalDof> dJacobian;

  ContactConfiguration_t fc_;
  jacobianFunctor functor;
  Eigen::NumericalDiff<jacobianFunctor> numDiff;
};
#endif /* PFLJACOBIANDERIVATIVE_HPP_ */
