/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * PFLConstraintJacobian.hpp
 *
 * by : Diego Pardo
 * date : 04/08/2017
 *
 */

#ifndef PFLConstraintJacobian_HPP_
#define PFLConstraintJacobian_HPP_

#include <Eigen/Dense>

#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLExtendedJacobians.hpp>

/**
 * class representing the EE contact Jacobian
 */
class PFLConstraintJacobian : public PFLExtendedJacobians {

public :

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef robotDimensions::ContactConfiguration_t ContactConfiguration_t;
	typedef robotDimensions::EEPositionsDataMap_t EEPositionsDataMap_t;
	typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef robotDimensions::EEJacobian_t EEJacobian_t;
	typedef robotDimensions::EEJacobianTranspose_t EEJacobianTranspose_t;
	typedef robotDimensions::SingleEEJacobian_t SingleEEJacobian_t;
	typedef robotDimensions::EESingleJacobianDataMap_t EESingleJacobianDataMap_t;
	typedef robotDimensions::JointCoordinates_t JointCoordinates_t;

	/**
	 * @brief Contact Jacobian constructor depends on the EE configuration
	 * @param fc the DataMap describing the EE configuration
	 */
	PFLConstraintJacobian(const ContactConfiguration_t & fc);

	/**
	 * @brief convenient contact Jacobian constructor for the case of all EE in the same configuration
	 * @param fc boolean describing the state of the EE
	 */
	PFLConstraintJacobian(const bool &fc);

	virtual ~PFLConstraintJacobian(){ }

	/**
	 * @return The size of the current EE Jacobian
	 */
  int GetSize(){return j_size_;};

	/**
	 * @brief Get the current Jacobian for specific single EE
	 * @param leg a size_t representing the id of the EE
	 * @param requested_jacobian the EE Jacobian
	 */
  void GetCurrentFootJacobian(const size_t & leg , SingleEEJacobian_t & requested_jacobian);

  /**
   * @brief Get all the current EE Jacobians
   * @param feet_jacobians the DataMap with separate Jacobians for all the EE
   */
  void GetCurrentFeetJacobian(EESingleJacobianDataMap_t & feet_jacobians);

	/**
	 * @brief update the current EE Jacobian (Jc), setting to zero the blocks of those EE not in contact
	 * @param big_q the GeneralizedCoordinates vector with the configuration of the robot
	 */
	void update(const GeneralizedCoordinates_t & big_q);

	/**
	 * @brief update the current EE Jacobian (Jc), setting to zero the blocks of those EE not in contact
	 * @param joints_q_r the JointCoordinates vector with the configuration of the joints
	 * @param feet_pose_base the position of the EE in base frame
	 * (CAREFOUL: This function doesn't check that feet_pose and joints_q_r correspond to each other)
	 */
	void updateQuick(const JointCoordinates_t & joints_q_r, const EEPositionsDataMap_t & feet_pose_base);

	/**
	 * @brief updates the current EE Jacobian and gets it back
	 * @param big_q the GeneralizedCoordinates with the configuration of the robot
	 * @param Jc the current EE Jacobian
	 */
	void GetContactConfigurationJacobian(const GeneralizedCoordinates_t & big_q, EEJacobian_t & Jc);

	/**
	 * @brief gets the current EE Jacobian
	 * @param Jc_user the current EE Jacobian
	 */
	void GetJc(EEJacobian_t & Jc_user) { Jc_user = Jc_ ; }
	EEJacobian_t GetJc(){return Jc_;}

	/**
	 * @brief computes and gets the inverse of the current EE Jacobian
	 * @param JcInv The inverse of Jc
	 */
	void GetInverse(EEJacobianTranspose_t & JcInv,
	                const double & tolerance = 1e-9);

	/**
	 * @brief computes and gets the inverse of the current EE Jacobian AND returns the components of the SVD factorization
	 * @param JcInv EJacobianTranspose_t matrix with the inverse of Jc
	 * @param tolerance double indicating the tolerance to be used for the inversion
	 * @param U Square unitary matrix (# rows of J)
	 * @param V Square unitary martix (# cols of J)
	 */
	void GetInverse(EEJacobianTranspose_t & JcInv,
	                const double & tolerance,
	                Eigen::Matrix<double,kContactForcesSize,kContactForcesSize> & U,
	                Eigen::Matrix<double, kTotalDof, kTotalDof> & V);

	void ChangeFeetConfiguration(const ContactConfiguration_t & fc);

	ContactConfiguration_t getFeetConfiguration() const { return feet_configuration_; }

private:
	void setJSize(const ContactConfiguration_t & fc);
  void SetCurrentJc();
	EEJacobian_t Jc_; /*!< The Jacobian for all the EE */
	size_t j_size_; /*! The size of the current Jacobian */
	ContactConfiguration_t feet_configuration_; /*! The current EE configuration */
};

#endif
