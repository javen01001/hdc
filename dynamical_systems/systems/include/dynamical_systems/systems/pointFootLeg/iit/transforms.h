#ifndef PFLEG_TRANSFORMS_H_
#define PFLEG_TRANSFORMS_H_

#include <Eigen/Dense>
#include <iit/rbd/TransformsBase.h>
#include "declarations.h"
#include <iit/rbd/traits/DoubleTrait.h>
#include "kinematics_parameters.h"

namespace iit {
namespace pfLeg {

template<typename SCALAR, class M>
class TransformMotion : public iit::rbd::SpatialTransformBase<tpl::JointState<SCALAR>, M> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

template<typename SCALAR, class M>
class TransformForce : public iit::rbd::SpatialTransformBase<tpl::JointState<SCALAR>, M> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

template<typename SCALAR, class M>
class TransformHomogeneous : public iit::rbd::HomogeneousTransformBase<tpl::JointState<SCALAR>, M> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

namespace tpl {


/**
 * The class for the 6-by-6 coordinates transformation matrices for
 * spatial motion vectors.
 */
template <typename TRAIT>
class MotionTransforms {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef typename TRAIT::Scalar Scalar;

    typedef JointState<Scalar> JState;
    class Dummy {};
    typedef typename TransformMotion<Scalar, Dummy>::MatrixType MatrixType;
public:
    class Type_fr_trunk_X_fr_LF_hipassembly : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_upperleg : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_upperleg();
        const Type_fr_trunk_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_lowerleg : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_lowerleg();
        const Type_fr_trunk_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_trunk : public TransformMotion<Scalar, Type_fr_LF_hipassembly_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_trunk : public TransformMotion<Scalar, Type_fr_LF_upperleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_trunk();
        const Type_fr_LF_upperleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_trunk : public TransformMotion<Scalar, Type_fr_LF_lowerleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_trunk();
        const Type_fr_LF_lowerleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_hipassemblyCOM : public TransformMotion<Scalar, Type_fr_trunk_X_LF_hipassemblyCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_hipassemblyCOM();
        const Type_fr_trunk_X_LF_hipassemblyCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_upperlegCOM : public TransformMotion<Scalar, Type_fr_trunk_X_LF_upperlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_upperlegCOM();
        const Type_fr_trunk_X_LF_upperlegCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_lowerlegCOM : public TransformMotion<Scalar, Type_fr_trunk_X_LF_lowerlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_lowerlegCOM();
        const Type_fr_trunk_X_LF_lowerlegCOM& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_LF_lowerleg : public TransformMotion<Scalar, Type_LF_foot_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_LF_lowerleg();
        const Type_LF_foot_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_foot : public TransformMotion<Scalar, Type_fr_trunk_X_LF_foot>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_trunk : public TransformMotion<Scalar, Type_LF_foot_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_trunk();
        const Type_LF_foot_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HAA : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_HAA>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HFE : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_HFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_KFE : public TransformMotion<Scalar, Type_fr_trunk_X_fr_LF_KFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformMotion<Scalar, Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformMotion<Scalar, Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformMotion<Scalar, Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformMotion<Scalar, Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
public:
    MotionTransforms();
    void updateParameters();
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_trunk_X_fr_LF_upperleg fr_trunk_X_fr_LF_upperleg;
    Type_fr_trunk_X_fr_LF_lowerleg fr_trunk_X_fr_LF_lowerleg;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_LF_upperleg_X_fr_trunk fr_LF_upperleg_X_fr_trunk;
    Type_fr_LF_lowerleg_X_fr_trunk fr_LF_lowerleg_X_fr_trunk;
    Type_fr_trunk_X_LF_hipassemblyCOM fr_trunk_X_LF_hipassemblyCOM;
    Type_fr_trunk_X_LF_upperlegCOM fr_trunk_X_LF_upperlegCOM;
    Type_fr_trunk_X_LF_lowerlegCOM fr_trunk_X_LF_lowerlegCOM;
    Type_LF_foot_X_fr_LF_lowerleg LF_foot_X_fr_LF_lowerleg;
    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_LF_foot_X_fr_trunk LF_foot_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;

protected:

}; //class 'MotionTransforms'

/**
 * The class for the 6-by-6 coordinates transformation matrices for
 * spatial force vectors.
 */
template <typename TRAIT>
class ForceTransforms {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef typename TRAIT::Scalar Scalar;

    typedef JointState<Scalar> JState;
    class Dummy {};
    typedef typename TransformForce<Scalar, Dummy>::MatrixType MatrixType;
public:
    class Type_fr_trunk_X_fr_LF_hipassembly : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_upperleg : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_upperleg();
        const Type_fr_trunk_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_lowerleg : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_lowerleg();
        const Type_fr_trunk_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_trunk : public TransformForce<Scalar, Type_fr_LF_hipassembly_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_trunk : public TransformForce<Scalar, Type_fr_LF_upperleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_trunk();
        const Type_fr_LF_upperleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_trunk : public TransformForce<Scalar, Type_fr_LF_lowerleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_trunk();
        const Type_fr_LF_lowerleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_hipassemblyCOM : public TransformForce<Scalar, Type_fr_trunk_X_LF_hipassemblyCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_hipassemblyCOM();
        const Type_fr_trunk_X_LF_hipassemblyCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_upperlegCOM : public TransformForce<Scalar, Type_fr_trunk_X_LF_upperlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_upperlegCOM();
        const Type_fr_trunk_X_LF_upperlegCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_lowerlegCOM : public TransformForce<Scalar, Type_fr_trunk_X_LF_lowerlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_lowerlegCOM();
        const Type_fr_trunk_X_LF_lowerlegCOM& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_LF_lowerleg : public TransformForce<Scalar, Type_LF_foot_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_LF_lowerleg();
        const Type_LF_foot_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_foot : public TransformForce<Scalar, Type_fr_trunk_X_LF_foot>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_trunk : public TransformForce<Scalar, Type_LF_foot_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_trunk();
        const Type_LF_foot_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HAA : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_HAA>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HFE : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_HFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_KFE : public TransformForce<Scalar, Type_fr_trunk_X_fr_LF_KFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformForce<Scalar, Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformForce<Scalar, Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformForce<Scalar, Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformForce<Scalar, Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
public:
    ForceTransforms();
    void updateParameters();
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_trunk_X_fr_LF_upperleg fr_trunk_X_fr_LF_upperleg;
    Type_fr_trunk_X_fr_LF_lowerleg fr_trunk_X_fr_LF_lowerleg;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_LF_upperleg_X_fr_trunk fr_LF_upperleg_X_fr_trunk;
    Type_fr_LF_lowerleg_X_fr_trunk fr_LF_lowerleg_X_fr_trunk;
    Type_fr_trunk_X_LF_hipassemblyCOM fr_trunk_X_LF_hipassemblyCOM;
    Type_fr_trunk_X_LF_upperlegCOM fr_trunk_X_LF_upperlegCOM;
    Type_fr_trunk_X_LF_lowerlegCOM fr_trunk_X_LF_lowerlegCOM;
    Type_LF_foot_X_fr_LF_lowerleg LF_foot_X_fr_LF_lowerleg;
    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_LF_foot_X_fr_trunk LF_foot_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;

protected:

}; //class 'ForceTransforms'

/**
 * The class with the homogeneous (4x4) coordinates transformation
 * matrices.
 */
template <typename TRAIT>
class HomogeneousTransforms {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef typename TRAIT::Scalar Scalar;

    typedef JointState<Scalar> JState;
    class Dummy {};
    typedef typename TransformHomogeneous<Scalar, Dummy>::MatrixType MatrixType;
public:
    class Type_fr_trunk_X_fr_LF_hipassembly : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_upperleg : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_upperleg();
        const Type_fr_trunk_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_lowerleg : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_lowerleg();
        const Type_fr_trunk_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_trunk : public TransformHomogeneous<Scalar, Type_fr_LF_hipassembly_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_trunk : public TransformHomogeneous<Scalar, Type_fr_LF_upperleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_trunk();
        const Type_fr_LF_upperleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_trunk : public TransformHomogeneous<Scalar, Type_fr_LF_lowerleg_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_trunk();
        const Type_fr_LF_lowerleg_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_hipassemblyCOM : public TransformHomogeneous<Scalar, Type_fr_trunk_X_LF_hipassemblyCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_hipassemblyCOM();
        const Type_fr_trunk_X_LF_hipassemblyCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_upperlegCOM : public TransformHomogeneous<Scalar, Type_fr_trunk_X_LF_upperlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_upperlegCOM();
        const Type_fr_trunk_X_LF_upperlegCOM& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_lowerlegCOM : public TransformHomogeneous<Scalar, Type_fr_trunk_X_LF_lowerlegCOM>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_lowerlegCOM();
        const Type_fr_trunk_X_LF_lowerlegCOM& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_LF_lowerleg : public TransformHomogeneous<Scalar, Type_LF_foot_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_LF_lowerleg();
        const Type_LF_foot_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_LF_foot : public TransformHomogeneous<Scalar, Type_fr_trunk_X_LF_foot>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const JState&);
    protected:
    };
    
    class Type_LF_foot_X_fr_trunk : public TransformHomogeneous<Scalar, Type_LF_foot_X_fr_trunk>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_LF_foot_X_fr_trunk();
        const Type_LF_foot_X_fr_trunk& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HAA : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_HAA>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_HFE : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_HFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const JState&);
    protected:
    };
    
    class Type_fr_trunk_X_fr_LF_KFE : public TransformHomogeneous<Scalar, Type_fr_trunk_X_fr_LF_KFE>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformHomogeneous<Scalar, Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformHomogeneous<Scalar, Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformHomogeneous<Scalar, Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const JState&);
    protected:
    };
    
    class Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformHomogeneous<Scalar, Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const JState&);
    protected:
    };
    
public:
    HomogeneousTransforms();
    void updateParameters();
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_trunk_X_fr_LF_upperleg fr_trunk_X_fr_LF_upperleg;
    Type_fr_trunk_X_fr_LF_lowerleg fr_trunk_X_fr_LF_lowerleg;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_LF_upperleg_X_fr_trunk fr_LF_upperleg_X_fr_trunk;
    Type_fr_LF_lowerleg_X_fr_trunk fr_LF_lowerleg_X_fr_trunk;
    Type_fr_trunk_X_LF_hipassemblyCOM fr_trunk_X_LF_hipassemblyCOM;
    Type_fr_trunk_X_LF_upperlegCOM fr_trunk_X_LF_upperlegCOM;
    Type_fr_trunk_X_LF_lowerlegCOM fr_trunk_X_LF_lowerlegCOM;
    Type_LF_foot_X_fr_LF_lowerleg LF_foot_X_fr_LF_lowerleg;
    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_LF_foot_X_fr_trunk LF_foot_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;

protected:

}; //class 'HomogeneousTransforms'

}

using MotionTransforms = tpl::MotionTransforms<rbd::DoubleTrait>;
using ForceTransforms = tpl::ForceTransforms<rbd::DoubleTrait>;
using HomogeneousTransforms = tpl::HomogeneousTransforms<rbd::DoubleTrait>;

}
}

#include "transforms.impl.h"

#endif
