#ifndef IIT_PFLEG_LINK_DATA_MAP_H_
#define IIT_PFLEG_LINK_DATA_MAP_H_

#include "declarations.h"

namespace iit {
namespace pfLeg {

/**
 * A very simple container to associate a generic data item to each link
 */
template<typename T> class LinkDataMap {
private:
    T data[linksCount];
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    LinkDataMap() {};
    LinkDataMap(const T& defaultValue);
    LinkDataMap(const LinkDataMap& rhs);
    LinkDataMap& operator=(const LinkDataMap& rhs);
    LinkDataMap& operator=(const T& rhs);
          T& operator[](LinkIdentifiers which);
    const T& operator[](LinkIdentifiers which) const;
private:
    void copydata(const LinkDataMap& rhs);
    void assigndata(const T& commonValue);
};

template<typename T> inline
LinkDataMap<T>::LinkDataMap(const T& value) {
    assigndata(value);
}

template<typename T> inline
LinkDataMap<T>::LinkDataMap(const LinkDataMap& rhs)
{
    copydata(rhs);
}

template<typename T> inline
LinkDataMap<T>& LinkDataMap<T>::operator=(const LinkDataMap& rhs)
{
    if(&rhs != this) {
        copydata(rhs);
    }
    return *this;
}

template<typename T> inline
LinkDataMap<T>& LinkDataMap<T>::operator=(const T& value)
{
    assigndata(value);
    return *this;
}

template<typename T> inline
T& LinkDataMap<T>::operator[](LinkIdentifiers l) {
    return data[l];
}

template<typename T> inline
const T& LinkDataMap<T>::operator[](LinkIdentifiers l) const {
    return data[l];
}

template<typename T> inline
void LinkDataMap<T>::copydata(const LinkDataMap& rhs) {
    data[TRUNK] = rhs[TRUNK];
    data[LF_HIPASSEMBLY] = rhs[LF_HIPASSEMBLY];
    data[LF_UPPERLEG] = rhs[LF_UPPERLEG];
    data[LF_LOWERLEG] = rhs[LF_LOWERLEG];
}

template<typename T> inline
void LinkDataMap<T>::assigndata(const T& value) {
    data[TRUNK] = value;
    data[LF_HIPASSEMBLY] = value;
    data[LF_UPPERLEG] = value;
    data[LF_LOWERLEG] = value;
}

template<typename T> inline
std::ostream& operator<<(std::ostream& out, const LinkDataMap<T>& map) {
    out
    << "   trunk = "
    << map[TRUNK]
    << "   LF_hipassembly = "
    << map[LF_HIPASSEMBLY]
    << "   LF_upperleg = "
    << map[LF_UPPERLEG]
    << "   LF_lowerleg = "
    << map[LF_LOWERLEG]
    ;
    return out;
}

}
}
#endif
