#ifndef IIT_ROBOT_PFLEG_DECLARATIONS_H_
#define IIT_ROBOT_PFLEG_DECLARATIONS_H_

#include <iit/rbd/rbd.h>

namespace iit {
namespace pfLeg {

static const int JointSpaceDimension = 3;
static const int jointsCount = 3;
/** The total number of rigid bodies of this robot, including the base */
static const int linksCount  = 4;

namespace tpl {
template <typename SCALAR>
using Column3d = iit::rbd::PlainMatrix<SCALAR, 3, 1>;

template <typename SCALAR>
using JointState = Column3d<SCALAR>;
}

using Column3d = tpl::Column3d<double>;
typedef Column3d JointState;

enum JointIdentifiers {
    LF_HAA = 0
    , LF_HFE
    , LF_KFE
};

enum LinkIdentifiers {
    TRUNK = 0
    , LF_HIPASSEMBLY
    , LF_UPPERLEG
    , LF_LOWERLEG
};

static const JointIdentifiers orderedJointIDs[jointsCount] =
    {LF_HAA,LF_HFE,LF_KFE};

static const LinkIdentifiers orderedLinkIDs[linksCount] =
    {TRUNK,LF_HIPASSEMBLY,LF_UPPERLEG,LF_LOWERLEG};

}
}
#endif
