
// Initialization of static-const data
template <typename TRAIT>
const typename iit::pfLeg::dyn::tpl::ForwardDynamics<TRAIT>::ExtForces
    iit::pfLeg::dyn::tpl::ForwardDynamics<TRAIT>::zeroExtForces(Force::Zero());

template <typename TRAIT>
iit::pfLeg::dyn::tpl::ForwardDynamics<TRAIT>::ForwardDynamics(iit::pfLeg::dyn::tpl::InertiaProperties<TRAIT>& inertia, MTransforms& transforms) :
    inertiaProps( & inertia ),
    motionTransforms( & transforms )
{
    LF_hipassembly_v.setZero();
    LF_hipassembly_c.setZero();
    LF_upperleg_v.setZero();
    LF_upperleg_c.setZero();
    LF_lowerleg_v.setZero();
    LF_lowerleg_c.setZero();

    vcross.setZero();
    Ia_r.setZero();

}

template <typename TRAIT>
void iit::pfLeg::dyn::tpl::ForwardDynamics<TRAIT>::fd(
    JointState& qdd,
    Acceleration& trunk_a,
    const Velocity& trunk_v,
    const Acceleration& g,
    const JointState& qd,
    const JointState& tau,
    const ExtForces& fext/* = zeroExtForces */)
{
    
    trunk_AI = inertiaProps->getTensor_trunk();
    trunk_p = - fext[TRUNK];
    LF_hipassembly_AI = inertiaProps->getTensor_LF_hipassembly();
    LF_hipassembly_p = - fext[LF_HIPASSEMBLY];
    LF_upperleg_AI = inertiaProps->getTensor_LF_upperleg();
    LF_upperleg_p = - fext[LF_UPPERLEG];
    LF_lowerleg_AI = inertiaProps->getTensor_LF_lowerleg();
    LF_lowerleg_p = - fext[LF_LOWERLEG];
    // ---------------------- FIRST PASS ---------------------- //
    // Note that, during the first pass, the articulated inertias are really
    //  just the spatial inertia of the links (see assignments above).
    //  Afterwards things change, and articulated inertias shall not be used
    //  in functions which work specifically with spatial inertias.
    
    // + Link LF_hipassembly
    //  - The spatial velocity:
    LF_hipassembly_v = (motionTransforms-> fr_LF_hipassembly_X_fr_trunk) * trunk_v;
    LF_hipassembly_v(iit::rbd::AZ) += qd(LF_HAA);
    
    //  - The velocity-product acceleration term:
    iit::rbd::motionCrossProductMx<Scalar>(LF_hipassembly_v, vcross);
    LF_hipassembly_c = vcross.col(iit::rbd::AZ) * qd(LF_HAA);
    
    //  - The bias force term:
    LF_hipassembly_p += iit::rbd::vxIv(LF_hipassembly_v, LF_hipassembly_AI);
    
    // + Link LF_upperleg
    //  - The spatial velocity:
    LF_upperleg_v = (motionTransforms-> fr_LF_upperleg_X_fr_LF_hipassembly) * LF_hipassembly_v;
    LF_upperleg_v(iit::rbd::AZ) += qd(LF_HFE);
    
    //  - The velocity-product acceleration term:
    iit::rbd::motionCrossProductMx<Scalar>(LF_upperleg_v, vcross);
    LF_upperleg_c = vcross.col(iit::rbd::AZ) * qd(LF_HFE);
    
    //  - The bias force term:
    LF_upperleg_p += iit::rbd::vxIv(LF_upperleg_v, LF_upperleg_AI);
    
    // + Link LF_lowerleg
    //  - The spatial velocity:
    LF_lowerleg_v = (motionTransforms-> fr_LF_lowerleg_X_fr_LF_upperleg) * LF_upperleg_v;
    LF_lowerleg_v(iit::rbd::AZ) += qd(LF_KFE);
    
    //  - The velocity-product acceleration term:
    iit::rbd::motionCrossProductMx<Scalar>(LF_lowerleg_v, vcross);
    LF_lowerleg_c = vcross.col(iit::rbd::AZ) * qd(LF_KFE);
    
    //  - The bias force term:
    LF_lowerleg_p += iit::rbd::vxIv(LF_lowerleg_v, LF_lowerleg_AI);
    
    // + The floating base body
    trunk_p += iit::rbd::vxIv(trunk_v, trunk_AI);
    
    // ---------------------- SECOND PASS ---------------------- //
    Matrix66S IaB;
    Force pa;
    
    // + Link LF_lowerleg
    LF_lowerleg_u = tau(LF_KFE) - LF_lowerleg_p(iit::rbd::AZ);
    LF_lowerleg_U = LF_lowerleg_AI.col(iit::rbd::AZ);
    LF_lowerleg_D = LF_lowerleg_U(iit::rbd::AZ);
    
    iit::rbd::compute_Ia_revolute(LF_lowerleg_AI, LF_lowerleg_U, LF_lowerleg_D, Ia_r);  // same as: Ia_r = LF_lowerleg_AI - LF_lowerleg_U/LF_lowerleg_D * LF_lowerleg_U.transpose();
    pa = LF_lowerleg_p + Ia_r * LF_lowerleg_c + LF_lowerleg_U * LF_lowerleg_u/LF_lowerleg_D;
    ctransform_Ia_revolute(Ia_r, motionTransforms-> fr_LF_lowerleg_X_fr_LF_upperleg, IaB);
    LF_upperleg_AI += IaB;
    LF_upperleg_p += (motionTransforms-> fr_LF_lowerleg_X_fr_LF_upperleg).transpose() * pa;
    
    // + Link LF_upperleg
    LF_upperleg_u = tau(LF_HFE) - LF_upperleg_p(iit::rbd::AZ);
    LF_upperleg_U = LF_upperleg_AI.col(iit::rbd::AZ);
    LF_upperleg_D = LF_upperleg_U(iit::rbd::AZ);
    
    iit::rbd::compute_Ia_revolute(LF_upperleg_AI, LF_upperleg_U, LF_upperleg_D, Ia_r);  // same as: Ia_r = LF_upperleg_AI - LF_upperleg_U/LF_upperleg_D * LF_upperleg_U.transpose();
    pa = LF_upperleg_p + Ia_r * LF_upperleg_c + LF_upperleg_U * LF_upperleg_u/LF_upperleg_D;
    ctransform_Ia_revolute(Ia_r, motionTransforms-> fr_LF_upperleg_X_fr_LF_hipassembly, IaB);
    LF_hipassembly_AI += IaB;
    LF_hipassembly_p += (motionTransforms-> fr_LF_upperleg_X_fr_LF_hipassembly).transpose() * pa;
    
    // + Link LF_hipassembly
    LF_hipassembly_u = tau(LF_HAA) - LF_hipassembly_p(iit::rbd::AZ);
    LF_hipassembly_U = LF_hipassembly_AI.col(iit::rbd::AZ);
    LF_hipassembly_D = LF_hipassembly_U(iit::rbd::AZ);
    
    iit::rbd::compute_Ia_revolute(LF_hipassembly_AI, LF_hipassembly_U, LF_hipassembly_D, Ia_r);  // same as: Ia_r = LF_hipassembly_AI - LF_hipassembly_U/LF_hipassembly_D * LF_hipassembly_U.transpose();
    pa = LF_hipassembly_p + Ia_r * LF_hipassembly_c + LF_hipassembly_U * LF_hipassembly_u/LF_hipassembly_D;
    ctransform_Ia_revolute(Ia_r, motionTransforms-> fr_LF_hipassembly_X_fr_trunk, IaB);
    trunk_AI += IaB;
    trunk_p += (motionTransforms-> fr_LF_hipassembly_X_fr_trunk).transpose() * pa;
    
    // + The acceleration of the floating base trunk, without gravity
    trunk_a = - TRAIT::solve(trunk_AI, trunk_p);  // trunk_a = - IA^-1 * trunk_p
    
    // ---------------------- THIRD PASS ---------------------- //
    LF_hipassembly_a = (motionTransforms-> fr_LF_hipassembly_X_fr_trunk) * trunk_a + LF_hipassembly_c;
    qdd(LF_HAA) = (LF_hipassembly_u - LF_hipassembly_U.dot(LF_hipassembly_a)) / LF_hipassembly_D;
    LF_hipassembly_a(iit::rbd::AZ) += qdd(LF_HAA);
    
    LF_upperleg_a = (motionTransforms-> fr_LF_upperleg_X_fr_LF_hipassembly) * LF_hipassembly_a + LF_upperleg_c;
    qdd(LF_HFE) = (LF_upperleg_u - LF_upperleg_U.dot(LF_upperleg_a)) / LF_upperleg_D;
    LF_upperleg_a(iit::rbd::AZ) += qdd(LF_HFE);
    
    LF_lowerleg_a = (motionTransforms-> fr_LF_lowerleg_X_fr_LF_upperleg) * LF_upperleg_a + LF_lowerleg_c;
    qdd(LF_KFE) = (LF_lowerleg_u - LF_lowerleg_U.dot(LF_lowerleg_a)) / LF_lowerleg_D;
    LF_lowerleg_a(iit::rbd::AZ) += qdd(LF_KFE);
    
    
    // + Add gravity to the acceleration of the floating base
    trunk_a += g;
}
