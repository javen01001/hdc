/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * quadrotor_dimensions.hpp
 *
 *  Created on: Jan 15, 2015
 *      Author: depardo
 */

#ifndef QUADROTORDIMENSIONS_HPP_
#define QUADROTORDIMENSIONS_HPP_

#include<dynamical_systems/base/Dimensions.hpp>


template <size_t STATE_DIM, size_t CONTROL_DIM, size_t JOINTS_DIM , size_t DOF , size_t EE>
std::vector<std::string> Dimensions <STATE_DIM,CONTROL_DIM,JOINTS_DIM,DOF,EE>::robot_joint_names;

namespace quadrotor {
enum DIMENSIONS { STATE_DIM = 12, CONTROL_DIM = 4, JOINTS_DIM = 0, DOF_DIM = 6 , EE_DIM = 0};
typedef Dimensions<STATE_DIM, CONTROL_DIM, JOINTS_DIM, DOF_DIM, EE_DIM> quadrotorDimensions;
}



#endif /* QUADROTORDIMENSIONS_HPP_ */
