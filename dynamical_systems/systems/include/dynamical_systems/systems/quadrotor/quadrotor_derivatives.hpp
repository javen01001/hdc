/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * quadrotor_derivatives.hpp
 *
 *  Created on: Jan 15, 2016
 *      Author: depardo
 */

#ifndef QUADROTOR_DERIVATIVES_HPP_
#define QUADROTOR_DERIVATIVES_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/DerivativesBaseDS.hpp>

class QuadrotorDerivatives : public DerivativesBaseDS<quadrotor::quadrotorDimensions> {
public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	QuadrotorDerivatives() { }

	quadrotor::quadrotorDimensions::state_matrix_t getDerivativeState() {
		quadrotor::quadrotorDimensions::state_matrix_t A;
		A = this->A_quadrotor(this->x_, this->u_);
		return A;
	}

	quadrotor::quadrotorDimensions::control_gain_matrix_t getDerivativeControl() {
		quadrotor::quadrotorDimensions::control_gain_matrix_t B;
		B = this->B_quadrotor(this->x_, this->u_);
		return B;
	}

private:
	quadrotor::quadrotorDimensions::state_matrix_t A_quadrotor(const quadrotor::quadrotorDimensions::state_vector_t &x, const quadrotor::quadrotorDimensions::control_vector_t &u);
	quadrotor::quadrotorDimensions::control_gain_matrix_t B_quadrotor(const quadrotor::quadrotorDimensions::state_vector_t &x, const quadrotor::quadrotorDimensions::control_vector_t &u);
};

quadrotor::quadrotorDimensions::state_matrix_t QuadrotorDerivatives::A_quadrotor(const quadrotor::quadrotorDimensions::state_vector_t &x, const quadrotor::quadrotorDimensions::control_vector_t &u) {

	using namespace quadrotor;

	double qxQ = x(0);
	double qyQ = x(1);
	double qzQ = x(2);
	double qph = x(3);
	double qth = x(4);
	double qps = x(5);
	double dqxQ = x(6);
	double dqyQ = x(7);
	double dqzQ = x(8);
	double dqph = x(9);
	double dqth = x(10);
	double dqps = x(11);

	double Fz = u(0);
	double Mx = u(1);
	double My = u(2);
	//double Mz = u(3);

	double t2, t3, t4,	t5,	t6,	t7,	t8,	t9,	t10,
	t11, t12, t13, t14, t15, t16, t17, t18, t19,
	t20, t26, t21, t22, t27, t23, t24, t25;

	t2 = 1.0/mQ;
	t3 = cos(qth);
	t4 = sin(qph);
	t5 = cos(qph);
	t6 = sin(qth);
	t7 = 1.0/Thxxyy;
	t8 = cos(qps);
	t9 = sin(qps);
	t10 = 1.0/t3;
	t11 = Thxxyy*2.0;
	t12 = Thzz-t11;
	t13 = qth*2.0;
	t14 = cos(t13);
	t15 = My*t9;
	t16 = sin(t13);
	t17 = 1.0/(t3*t3);
	t18 = qth*3.0;
	t19 = sin(t18);
	t20 = My*t8;
	t21 = Mx*t9;
	t22 = t20+t21;
	t23 = t3*t3;
	t24 = t6*t6;
	t25 = Thzz*dqps*t6;

	quadrotor::quadrotorDimensions::state_matrix_t A;
	A.setZero();

	A(0,6) = 1.0;
	A(1,7) = 1.0;
	A(2,8) = 1.0;
	A(3,9) = 1.0;
	A(4,10) = 1.0;
	A(5,11) = 1.0;
	A(6,4) = Fz*t2*t3;
	A(7,3) = -Fz*t2*t3*t5;
	A(7,4) = Fz*t2*t4*t6;
	A(8,3) = -Fz*t2*t3*t4;
	A(8,4) = -Fz*t2*t5*t6;
	A(9,4) = -t6*t7*t17*(t15-Mx*t8+Thzz*dqps*dqth-Thxxyy*dqph*dqth*t6*2.0+Thzz*dqph*dqth*t6)-dqph*dqth*t7*t12;
	A(9,5) = -t7*t10*t22;
	A(9,9) = -dqth*t6*t7*t10*t12;
	A(9,10) = -t7*t10*(Thzz*dqps-Thxxyy*dqph*t6*2.0+Thzz*dqph*t6);
	A(9,11) = -Thzz*dqth*t7*t10;
	A(10,4) = -dqph*t7*(t25+Thxxyy*dqph*t14-Thzz*dqph*t14);
	A(10,5) = -t7*(t15-Mx*t8);
	A(10,9) = t7*(-Thxxyy*dqph*t16+Thzz*dqps*t3+Thzz*dqph*t16);
	A(10,11) = Thzz*dqph*t3*t7;
	A(11,4) = t7*t17*(Mx*t8*-4.0+My*t9*4.0+Thzz*dqps*dqth*4.0-Thxxyy*dqph*dqth*t6*9.0-Thxxyy*dqph*dqth*t19+Thzz*dqph*dqth*t6*5.0+Thzz*dqph*dqth*t19)*(1.0/4.0);
	A(11,5) = t6*t7*t10*t22;
	A(11,9) = dqth*t7*t10*(Thzz-t11+Thxxyy*t23-Thzz*t23);
	A(11,10) = t7*t10*(t25-Thxxyy*dqph-Thxxyy*dqph*t24+Thzz*dqph*t24);
	A(11,11) = Thzz*dqth*t6*t7*t10;

	return A;

}

quadrotor::quadrotorDimensions::control_gain_matrix_t QuadrotorDerivatives::B_quadrotor(const quadrotor::quadrotorDimensions::state_vector_t &x, const quadrotor::quadrotorDimensions::control_vector_t &u)
{

	using namespace quadrotor;

	double qxQ = x(0);
	double qyQ = x(1);
	double qzQ = x(2);
	double qph = x(3);
	double qth = x(4);
	double qps = x(5);
	double dqxQ = x(6);
	double dqyQ = x(7);
	double dqzQ = x(8);
	double dqph = x(9);
	double dqth = x(10);
	double dqps = x(11);

	//	double u1 = u(0);
	//	double u2 = u(1);
	//	double u3 = u(2);
	//	double u4 = u(3);
	//
	//	double kF1 = kFs(0);
	//	double kF2 = kFs(1);
	//	double kF3 = kFs(2);
	//	double kF4 = kFs(3);
	//
	//	double kM1 = kMs(0);
	//	double kM2 = kMs(1);
	//	double kM3 = kMs(2);
	//	double kM4 = kMs(3);

	double t2, t3, t4, t5, t6, t7, t8, t9, t10, t11;

	t2 = 1.0/mQ;
	t3 = cos(qth);
	t4 = 1.0/Thxxyy;
	t5 = 1.0/t3;
	t6 = sin(qps);
	t7 = cos(qps);
	t8 = sin(qth);

	quadrotor::quadrotorDimensions::control_gain_matrix_t B;
	B.setZero();

	B(6,0) = t2*t8;
	B(7,0) = -t2*t3*sin(qph);
	B(8,0) = t2*t3*cos(qph);
	B(9,1) = t4*t5*t7;
	B(9,2) = -t4*t5*t6;
	B(10,1) = t4*t6;
	B(10,2) = t4*t7;
	B(11,1) = -t4*t5*t7*t8;
	B(11,2) = t4*t5*t6*t8;
	B(11,3) = 1.0/Thzz;

	return B;
}

#endif /* QUADROTOR_DERIVATIVES_HPP_ */
